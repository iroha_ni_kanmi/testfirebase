﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class SettingList : ScriptableObject
{
	public List<SettingEntity> Setting; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < Setting.Count ;i++){
			listIds.Add(Setting[i].id);
		}
		return listIds;
	}

	public Dictionary<string, SettingInfo> getSettingInfo_id(){
		//各idに紐づいた情報を返す
		Dictionary<string, SettingInfo> dicSettingInfo = new Dictionary<string, SettingInfo>();

		for(int i = 0; i < Setting.Count ;i++){
			SettingInfo SettingInfo = new SettingInfo();

			//クエストに紐づいた情報を格納していく
	        SettingInfo.id = Setting[i].id;
	        SettingInfo.settingName = Setting[i].settingName;

			dicSettingInfo[Setting[i].id] = SettingInfo;
			//Debug.Log("Setting"+dicSettingInfo[Setting[i].id].itemName);
		}
		return dicSettingInfo;

	}

	public class SettingInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string settingName = "";

        public void ResetData() {
			id = "";
	        settingName = "";
        }
	}
}
