﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

[ExcelAsset]
public class meetCharacterList : ScriptableObject
{
	public List<meetCharacterEntity> meetCharacter; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//questのIdをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < meetCharacter.Count ;i++){
			listIds.Add(meetCharacter[i].id);
		}
		return listIds;
	}

	public Dictionary<string, QuestInfo> getMeetCharacterInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, QuestInfo> dicMeetCharacterInfo = new Dictionary<string, QuestInfo>();

		for(int i = 0; i < meetCharacter.Count ;i++){
			QuestInfo questInfo = new QuestInfo();

			//クエストに紐づいた情報を格納していく
        	questInfo.id = meetCharacter[i].id;
        	questInfo.needCharacter = meetCharacter[i].needCharacter;

			dicMeetCharacterInfo[meetCharacter[i].id] = questInfo;
			//Debug.Log("main"+dicQuestInfo[MainQuest[i].id].questName);
		}
		return dicMeetCharacterInfo;

	}

	public class QuestInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string needCharacter = "";

        public void ResetData() {
        	id = "";
        	needCharacter = "";
        }
	}
}
