using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class naoshiDialogList : ScriptableObject
{
	public List<naoshiDialogEntity> naoshiDialog; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < naoshiDialog.Count ;i++){
			listIds.Add(naoshiDialog[i].id);
		}
		return listIds;
	}

	public Dictionary<string, naoshiDialogInfo> getNaoshiDialogInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, naoshiDialogInfo> dicnaoshiDialogInfo = new Dictionary<string, naoshiDialogInfo>();

		for(int i = 0; i < naoshiDialog.Count ;i++){
			naoshiDialogInfo naoshiDialogInfo = new naoshiDialogInfo();

			//クエストに紐づいた情報を格納していく
	        naoshiDialogInfo.clientIcon = naoshiDialog[i].clientIcon;
	        naoshiDialogInfo.dialogStart = naoshiDialog[i].dialogStart;
	        naoshiDialogInfo.dialogPhase01 = naoshiDialog[i].dialogPhase01;
	        naoshiDialogInfo.dialogPhase02 = naoshiDialog[i].dialogPhase02;
	        naoshiDialogInfo.dialogPhase03 = naoshiDialog[i].dialogPhase03;
	        naoshiDialogInfo.dialogEnd = naoshiDialog[i].dialogEnd;
	        naoshiDialogInfo.dialogWrong = naoshiDialog[i].dialogWrong;
	        naoshiDialogInfo.dialogRight = naoshiDialog[i].dialogRight;

			dicnaoshiDialogInfo[naoshiDialog[i].id] = naoshiDialogInfo;
			//Debug.Log("naoshi"+dicnaoshiDialogInfo[naoshiDialog[i].id].dollName);
		}
		return dicnaoshiDialogInfo;

	}

	public class naoshiDialogInfo{

        [SerializeField]
        public string clientIcon = "";

        [SerializeField]
        public string dialogStart = "";

        [SerializeField]
        public string dialogPhase01 = "";

        [SerializeField]
        public string dialogPhase02 = "";

        [SerializeField]
        public string dialogPhase03 = "";

        [SerializeField]
        public string dialogEnd = "";

        [SerializeField]
        public string dialogWrong = "";

        [SerializeField]
        public string dialogRight = "";

        public void ResetData() {
			clientIcon = "";
			dialogStart = "";
			dialogPhase01 = "";
			dialogPhase02 = "";
			dialogPhase03 = "";
			dialogEnd = "";
			dialogWrong = "";
			dialogRight = "";
        }
	}
}
