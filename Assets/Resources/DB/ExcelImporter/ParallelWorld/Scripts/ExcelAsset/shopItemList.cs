﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class shopItemList : ScriptableObject
{
	public List<shopItemEntity> shopItem; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < shopItem.Count ;i++){
			listIds.Add(shopItem[i].id);
		}
		return listIds;
	}

	public Dictionary<string, shopItemInfo> getShopItemInfo_id(){
		//各idに紐づいた情報を返す
		Dictionary<string, shopItemInfo> dicShopItemInfo = new Dictionary<string, shopItemInfo>();

		for(int i = 0; i < shopItem.Count ;i++){
			shopItemInfo shopItemInfo = new shopItemInfo();

			//クエストに紐づいた情報を格納していく
	        shopItemInfo.id = shopItem[i].id;
	        shopItemInfo.itemName = shopItem[i].itemName;
	        shopItemInfo.fileName = shopItem[i].fileName;
	        shopItemInfo.filePath = shopItem[i].filePath;
	        shopItemInfo.genre = shopItem[i].genre;
	        shopItemInfo.description = shopItem[i].description;
	        shopItemInfo.tradeItem = shopItem[i].tradeItem;
	        shopItemInfo.minStoryNo = shopItem[i].minStoryNo;
	        shopItemInfo.sortNumber = shopItem[i].sortNumber;

			dicShopItemInfo[shopItem[i].id] = shopItemInfo;
			//Debug.Log("shopItem"+dicshopItemInfo[shopItem[i].id].itemName);
		}
		return dicShopItemInfo;

	}

	public Dictionary<string, shopItemInfo> getShopItemInfo_itemName(){
		//各idに紐づいた情報を返す
		Dictionary<string, shopItemInfo> dicShopItemInfo = new Dictionary<string, shopItemInfo>();

		for(int i = 0; i < shopItem.Count ;i++){
			shopItemInfo shopItemInfo = new shopItemInfo();

			//クエストに紐づいた情報を格納していく
	        shopItemInfo.id = shopItem[i].id;
	        shopItemInfo.itemName = shopItem[i].itemName;
	        shopItemInfo.fileName = shopItem[i].fileName;
	        shopItemInfo.filePath = shopItem[i].filePath;
	        shopItemInfo.genre = shopItem[i].genre;
	        shopItemInfo.description = shopItem[i].description;
	        shopItemInfo.tradeItem = shopItem[i].tradeItem;
	        shopItemInfo.minStoryNo = shopItem[i].minStoryNo;
	        shopItemInfo.sortNumber = shopItem[i].sortNumber;

			dicShopItemInfo[shopItem[i].itemName] = shopItemInfo;
			//Debug.Log("shopItem"+dicShopItemInfo[shopItem[i].id].itemName);
		}
		return dicShopItemInfo;

	}

	public Dictionary<string, shopItemInfo> getShopItemInfo_fileName(){
		//各idに紐づいた情報を返す
		Dictionary<string, shopItemInfo> dicShopItemInfo = new Dictionary<string, shopItemInfo>();

		for(int i = 0; i < shopItem.Count ;i++){
			shopItemInfo shopItemInfo = new shopItemInfo();

			//クエストに紐づいた情報を格納していく
	        shopItemInfo.id = shopItem[i].id;
	        shopItemInfo.itemName = shopItem[i].itemName;
	        shopItemInfo.fileName = shopItem[i].fileName;
	        shopItemInfo.filePath = shopItem[i].filePath;
	        shopItemInfo.genre = shopItem[i].genre;
	        shopItemInfo.description = shopItem[i].description;
	        shopItemInfo.tradeItem = shopItem[i].tradeItem;
	        shopItemInfo.minStoryNo = shopItem[i].minStoryNo;
	        shopItemInfo.sortNumber = shopItem[i].sortNumber;

			dicShopItemInfo[shopItem[i].fileName] = shopItemInfo;
			//Debug.Log("shopItem"+dicshopItemInfo[shopItem[i].id].itemName);
		}
		return dicShopItemInfo;

	}

	public Dictionary<string, shopItemInfo> getShopItemInfo_genre(string genre){
		//各idに紐づいた情報を返す
		Dictionary<string, shopItemInfo> dicShopItemInfo = new Dictionary<string, shopItemInfo>();

		for(int i = 0; i < shopItem.Count ;i++){
			if(genre == shopItem[i].genre){
				shopItemInfo shopItemInfo = new shopItemInfo();

				//クエストに紐づいた情報を格納していく
		        shopItemInfo.id = shopItem[i].id;
	    	    shopItemInfo.itemName = shopItem[i].itemName;
	        	shopItemInfo.fileName = shopItem[i].fileName;
		        shopItemInfo.filePath = shopItem[i].filePath;
		        shopItemInfo.genre = shopItem[i].genre;
	    	    shopItemInfo.description = shopItem[i].description;
	        	shopItemInfo.tradeItem = shopItem[i].tradeItem;
		        shopItemInfo.minStoryNo = shopItem[i].minStoryNo;
		        shopItemInfo.sortNumber = shopItem[i].sortNumber;

				dicShopItemInfo[shopItem[i].id] = shopItemInfo;
				//Debug.Log("shopItem"+dicshopItemInfo[shopItem[i].id].itemName);
			}
		}
		return dicShopItemInfo;

	}

	public class shopItemInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string itemName = "";

        [SerializeField]
        public string fileName = "";

        [SerializeField]
        public string filePath = "";

        [SerializeField]
        public string genre = "";

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string tradeItem = "";

        [SerializeField]
        public int minStoryNo = 0;

        [SerializeField]
        public int sortNumber = 0;

        public void ResetData() {
			id = "";
	        itemName = "";
			fileName = "";
			filePath = "";
	        genre = "";
	        description = "";
			tradeItem = "";
			minStoryNo = 0;
			sortNumber = 0;
        }
	}
}
