using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class storyList : ScriptableObject
{
	public List<storyEntity> story; // Replace 'EntityType' to an actual type that is serializable.

	public List<int> getStoryNoes(){
		//Idをリストで返す
		List<int> listStoryNoes = new List<int>();
		for(int i = 0; i < story.Count ;i++){
			listStoryNoes.Add(story[i].storyNo);
		}
		return listStoryNoes;
	}

	public Dictionary<int, storyInfo> getDicStoryInfo(){
		//各idに紐づいた情報を返す
		Dictionary<int, storyInfo> dicStoryInfo = new Dictionary<int, storyInfo>();

		for(int i = 0; i < story.Count ;i++){
			storyInfo storyInfo = new storyInfo();

			//クエストに紐づいた情報を格納していく
	        storyInfo.storyNo = story[i].storyNo;
	        storyInfo.storyName = story[i].storyName;
			storyInfo.storyHeadingName = story[i].storyHeadingName;
	        storyInfo.storyDescription = story[i].storyDescription;
	        storyInfo.storyImageFilePath = story[i].storyImageFilePath;
	        storyInfo.keyIds = story[i].keyIds;
	        storyInfo.transitionFilePath = story[i].transitionFilePath;

			dicStoryInfo[story[i].storyNo] = storyInfo;
		}
		return dicStoryInfo;

	}

	public class storyInfo{

        [SerializeField]
        public int storyNo = 0;

        [SerializeField]
        public string storyName = "";

        [SerializeField]
        public string storyHeadingName = "";

        [SerializeField]
        public string storyDescription = "";

        [SerializeField]
        public string storyImageFilePath = "";

        [SerializeField]
        public string keyIds = "";

        [SerializeField]
        public string transitionFilePath = "";

        public void ResetData() {
	        storyNo = 0;
        	storyName = "";
			storyHeadingName = "";
        	storyDescription = "";
        	storyImageFilePath = "";
        	keyIds = "";
			transitionFilePath = "";
        }
	}
}
