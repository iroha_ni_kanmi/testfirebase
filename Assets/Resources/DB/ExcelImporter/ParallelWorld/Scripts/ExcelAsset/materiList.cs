using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class materiList : ScriptableObject
{
	public List<materiEntity> materi; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < materi.Count ;i++){
			listIds.Add(materi[i].id);
		}
		return listIds;
	}

	public Dictionary<string, materiInfo> getMateriInfo_id(){
		//各idに紐づいた情報を返す
		Dictionary<string, materiInfo> dicMateriInfo = new Dictionary<string, materiInfo>();

		for(int i = 0; i < materi.Count ;i++){
			materiInfo materiInfo = new materiInfo();

			//クエストに紐づいた情報を格納していく
	        materiInfo.id = materi[i].id;
	        materiInfo.itemName = materi[i].itemName;
	        materiInfo.fileName = materi[i].fileName;
	        materiInfo.filePath = materi[i].filePath;
	        materiInfo.genre = materi[i].genre;
	        materiInfo.description = materi[i].description;
	        materiInfo.howToGet = materi[i].howToGet;
	        materiInfo.land = materi[i].land;
	        materiInfo.itemPoint = materi[i].itemPoint;
	        materiInfo.sortNumber = materi[i].sortNumber;

			dicMateriInfo[materi[i].id] = materiInfo;
			//Debug.Log("materi"+dicMateriInfo[materi[i].id].itemName);
		}
		return dicMateriInfo;

	}

	public Dictionary<string, materiInfo> getMateriInfo_itemName(){
		//各idに紐づいた情報を返す
		Dictionary<string, materiInfo> dicMateriInfo = new Dictionary<string, materiInfo>();

		for(int i = 0; i < materi.Count ;i++){
			materiInfo materiInfo = new materiInfo();

			//クエストに紐づいた情報を格納していく
	        materiInfo.id = materi[i].id;
	        materiInfo.itemName = materi[i].itemName;
	        materiInfo.fileName = materi[i].fileName;
	        materiInfo.filePath = materi[i].filePath;
	        materiInfo.genre = materi[i].genre;
	        materiInfo.description = materi[i].description;
	        materiInfo.howToGet = materi[i].howToGet;
	        materiInfo.land = materi[i].land;
	        materiInfo.itemPoint = materi[i].itemPoint;
	        materiInfo.sortNumber = materi[i].sortNumber;

			dicMateriInfo[materi[i].itemName] = materiInfo;
			//Debug.Log("materi"+dicMateriInfo[materi[i].id].itemName);
		}
		return dicMateriInfo;

	}

	public Dictionary<string, materiInfo> getMateriInfo_fileName(){
		//各idに紐づいた情報を返す
		Dictionary<string, materiInfo> dicMateriInfo = new Dictionary<string, materiInfo>();

		for(int i = 0; i < materi.Count ;i++){
			materiInfo materiInfo = new materiInfo();

			//クエストに紐づいた情報を格納していく
	        materiInfo.id = materi[i].id;
	        materiInfo.itemName = materi[i].itemName;
	        materiInfo.fileName = materi[i].fileName;
	        materiInfo.filePath = materi[i].filePath;
	        materiInfo.genre = materi[i].genre;
	        materiInfo.description = materi[i].description;
	        materiInfo.howToGet = materi[i].howToGet;
	        materiInfo.land = materi[i].land;
	        materiInfo.itemPoint = materi[i].itemPoint;
	        materiInfo.sortNumber = materi[i].sortNumber;

			dicMateriInfo[materi[i].fileName] = materiInfo;
			//Debug.Log("materi"+dicMateriInfo[materi[i].id].itemName);
		}
		return dicMateriInfo;

	}

	public class materiInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string itemName = "";

        [SerializeField]
        public string fileName = "";

        [SerializeField]
        public string filePath = "";

        [SerializeField]
        public string genre = "";

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string howToGet = "";

        [SerializeField]
        public string land = "";

        [SerializeField]
        public float itemPoint = 0;

        [SerializeField]
        public int sortNumber = 0;

        public void ResetData() {
			id = "";
	        itemName = "";
			fileName = "";
			filePath = "";
	        genre = "";
	        description = "";
			howToGet = "";
	        land = "";
			itemPoint = 0;
			sortNumber = 0;
        }
	}
}
