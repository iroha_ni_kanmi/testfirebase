using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

[ExcelAsset]
public class MainQuestList : ScriptableObject
{
	public List<QuestEntity> MainQuest; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//questのIdをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < MainQuest.Count ;i++){
			listIds.Add(MainQuest[i].id);
		}
		return listIds;
	}

	public Dictionary<string, QuestInfo> getQuestInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, QuestInfo> dicQuestInfo = new Dictionary<string, QuestInfo>();

		for(int i = 0; i < MainQuest.Count ;i++){
			QuestInfo questInfo = new QuestInfo();

			//クエストに紐づいた情報を格納していく
        	questInfo.questName = MainQuest[i].questName;
        	questInfo.questSubName = MainQuest[i].questSubName;
        	questInfo.genre = MainQuest[i].genre;
        	questInfo.description = MainQuest[i].description;
        	questInfo.howToClear = MainQuest[i].howToClear;
        	questInfo.questcharactorName = MainQuest[i].questcharactorName;
        	questInfo.questIconFilePath = MainQuest[i].questIconFilePath;
        	questInfo.thankPoint = MainQuest[i].thankPoint;
        	questInfo.thankItems = MainQuest[i].thankItems;
        	questInfo.nextQuestId = MainQuest[i].nextQuestId;
        	questInfo.playerRank = MainQuest[i].playerRank;
        	questInfo.storyNo = MainQuest[i].storyNo;

			dicQuestInfo[MainQuest[i].id] = questInfo;
			//Debug.Log("main"+dicQuestInfo[MainQuest[i].id].questName);
		}
		return dicQuestInfo;

	}

	public class QuestInfo{

        [SerializeField]
        public string questName = "";

        [SerializeField]
        public string questSubName = "";

        [SerializeField]
        public QuestEntity.QuestGenre genre;

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string howToClear = "";

        [SerializeField]
        public string questcharactorName = "";

        [SerializeField]
        public string questIconFilePath = "";

        [SerializeField]
        public int thankPoint = 0;

        [SerializeField]
        public string thankItems = "";

        [SerializeField]
        public string nextQuestId = "";

        [SerializeField]
        public int playerRank = 1;

        [SerializeField]
        public int storyNo = 1;

        public void ResetData() {
        	questName = "";
        	questSubName = "";
        	genre = new QuestEntity.QuestGenre();
        	description = "";
        	howToClear = "";
        	questcharactorName = "";
        	questIconFilePath = "";
        	thankPoint = 0;
        	thankItems = "";
        	nextQuestId = "";
			playerRank = 1;
			storyNo = 1;
        }
	}
}
