using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;

[ExcelAsset]
public class TrophyList : ScriptableObject
{
	public List<TrophyEntity> Trophy; // Replace 'EntityType' to an actual type that is serializable.
	public List<string> getIds(){
		//TrophyのIdをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < Trophy.Count ;i++){
			listIds.Add(Trophy[i].id);
		}
		return listIds;
	}

	public Dictionary<string, TrophyInfo> getTrophyInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, TrophyInfo> dicTrophyInfo = new Dictionary<string, TrophyInfo>();

		for(int i = 0; i < Trophy.Count ;i++){
			TrophyInfo TrophyInfo = new TrophyInfo();

			//クエストに紐づいた情報を格納していく
        	TrophyInfo.id = Trophy[i].id;
        	TrophyInfo.trophyName = Trophy[i].trophyName;
        	TrophyInfo.genre = Trophy[i].genre;
        	TrophyInfo.genreCode = Trophy[i].genreCode;
        	TrophyInfo.description = Trophy[i].description;
        	TrophyInfo.storyNo = Trophy[i].storyNo;
    	    TrophyInfo.clearItemName = Trophy[i].clearItemName;
    	    TrophyInfo.clearItemGenre = Trophy[i].clearItemGenre;
    		TrophyInfo.clearItemFilePath = Trophy[i].clearItemFilePath;
    		TrophyInfo.clearItemDescription = Trophy[i].clearItemDescription;

			dicTrophyInfo[TrophyInfo.id] = TrophyInfo;
			//Debug.Log("sub"+dicTrophyInfo[Trophy[i].id].TrophyName);
		}
		return dicTrophyInfo;
	}

	public List<string> getListTrophyGenre(){
		//ジャンルの一覧をリストで取得する
		List<string> listTrophyGenre = new List<string>();

		for(int i = 0; i < Trophy.Count ;i++){
        	listTrophyGenre.Add(Trophy[i].genre);
		}
		//重複を消す
		IEnumerable<string> listTrophyGenre_D = listTrophyGenre.Distinct();
		return listTrophyGenre_D.ToList();
	}

	public Dictionary<int, string> getDicStoryNoAndTrophyGenre(){
		//StoryNo：ジャンルで取得する
		Dictionary<int, string> dicStoryNoAndTrophyGenre = new Dictionary<int, string>();

		for(int i = 0; i < Trophy.Count ;i++){
			dicStoryNoAndTrophyGenre[Trophy[i].storyNo] = Trophy[i].genre;
		}
		return dicStoryNoAndTrophyGenre;
	}

	public Dictionary<int, string> getDicStoryNoAndTrophyGenreCode(){
		//StoryNo：ジャンルコードで取得する
		Dictionary<int, string> dicStoryNoAndTrophyGenreCode = new Dictionary<int, string>();

		for(int i = 0; i < Trophy.Count ;i++){
			dicStoryNoAndTrophyGenreCode[Trophy[i].storyNo] = Trophy[i].genreCode;
		}
		return dicStoryNoAndTrophyGenreCode;
	}

	public Dictionary<string, int> getDicQuestCount(){
		//StoryNo：ジャンルで取得する
		Dictionary<string, int> dicQuestCount = new Dictionary<string, int>();

		for(int i = 0; i < Trophy.Count ;i++){
			if(dicQuestCount.ContainsKey(Trophy[i].trophyName)){
				dicQuestCount[Trophy[i].trophyName] += 1;
			}else{
				dicQuestCount[Trophy[i].trophyName] = 1;
			}
		}
		return dicQuestCount;
	}

	public List<TrophyInfo> getDicGenreAndTrophyInfo(string Genre){
		//ジャンルにひもづくTrophyInfoを取得する

		List<TrophyInfo> listTrophyInfo = new List<TrophyInfo>();

		for(int i = 0; i < Trophy.Count ;i++){
			if(Trophy[i].genre == Genre){
				TrophyInfo TrophyInfo = new TrophyInfo();

				//クエストに紐づいた情報を格納していく
        		TrophyInfo.id = Trophy[i].id;
        		TrophyInfo.trophyName = Trophy[i].trophyName;
        		TrophyInfo.genre = Trophy[i].genre;
        		TrophyInfo.genreCode = Trophy[i].genreCode;
    	    	TrophyInfo.description = Trophy[i].description;
	        	TrophyInfo.storyNo = Trophy[i].storyNo;
    	    	TrophyInfo.clearItemName = Trophy[i].clearItemName;
    	    	TrophyInfo.clearItemGenre = Trophy[i].clearItemGenre;
    	    	TrophyInfo.clearItemFilePath = Trophy[i].clearItemFilePath;
    	    	TrophyInfo.clearItemDescription = Trophy[i].clearItemDescription;

				listTrophyInfo.Add(TrophyInfo);
			}
		}

		return listTrophyInfo;
	}
	public class TrophyInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string trophyName = "";

        [SerializeField]
        public string genre = "";

        [SerializeField]
        public string genreCode = "";

        [SerializeField]
        public string description = "";

        [SerializeField]
        public int storyNo = 1;

        [SerializeField]
        public string clearItemName = "";

        [SerializeField]
        public string clearItemGenre = "";

        [SerializeField]
        public string clearItemFilePath = "";

        [SerializeField]
        public string clearItemDescription = "";

        public void ResetData() {
        	id = "";
        	trophyName = "";
        	genre = "";
			genreCode = "";
        	description = "";
			storyNo = 1;
        	clearItemName = "";
			clearItemGenre = "";
        	clearItemFilePath = "";
        	clearItemDescription = "";
        }
	}
}
