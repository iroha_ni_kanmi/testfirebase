﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class keyItemList : ScriptableObject
{
	public List<keyItemEntity> keyItem; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < keyItem.Count ;i++){
			listIds.Add(keyItem[i].id);
		}
		return listIds;
	}

	public Dictionary<string, keyItemInfo> getKeyItemInfo_id(){
		//各idに紐づいた情報を返す
		Dictionary<string, keyItemInfo> dickeyItemInfo = new Dictionary<string, keyItemInfo>();

		for(int i = 0; i < keyItem.Count ;i++){
			keyItemInfo keyItemInfo = new keyItemInfo();

			//クエストに紐づいた情報を格納していく
	        keyItemInfo.id = keyItem[i].id;
	        keyItemInfo.keyItemName = keyItem[i].keyItemName;
	        keyItemInfo.fileName = keyItem[i].fileName;
	        keyItemInfo.filePath = keyItem[i].filePath;
	        keyItemInfo.description = keyItem[i].description;
	        keyItemInfo.howToGet = keyItem[i].howToGet;
	        keyItemInfo.sortNumber = keyItem[i].sortNumber;

			dickeyItemInfo[keyItem[i].id] = keyItemInfo;
			//Debug.Log("keyItem"+dickeyItemInfo[keyItem[i].id].itemName);
		}
		return dickeyItemInfo;

	}

	public Dictionary<string, keyItemInfo> getKeyItemInfo_itemName(){
		//各idに紐づいた情報を返す
		Dictionary<string, keyItemInfo> dickeyItemInfo = new Dictionary<string, keyItemInfo>();

		for(int i = 0; i < keyItem.Count ;i++){
			keyItemInfo keyItemInfo = new keyItemInfo();

			//クエストに紐づいた情報を格納していく
	        keyItemInfo.id = keyItem[i].id;
	        keyItemInfo.keyItemName = keyItem[i].keyItemName;
	        keyItemInfo.fileName = keyItem[i].fileName;
	        keyItemInfo.filePath = keyItem[i].filePath;
	        keyItemInfo.description = keyItem[i].description;
	        keyItemInfo.howToGet = keyItem[i].howToGet;
	        keyItemInfo.sortNumber = keyItem[i].sortNumber;

			dickeyItemInfo[keyItem[i].keyItemName] = keyItemInfo;
			//Debug.Log("keyItem"+dickeyItemInfo[keyItem[i].id].itemName);
		}
		return dickeyItemInfo;

	}

	public Dictionary<string, keyItemInfo> getKeyItemInfo_fileName(){
		//各idに紐づいた情報を返す
		Dictionary<string, keyItemInfo> dickeyItemInfo = new Dictionary<string, keyItemInfo>();

		for(int i = 0; i < keyItem.Count ;i++){
			keyItemInfo keyItemInfo = new keyItemInfo();

			//クエストに紐づいた情報を格納していく
	        keyItemInfo.id = keyItem[i].id;
	        keyItemInfo.keyItemName = keyItem[i].keyItemName;
	        keyItemInfo.fileName = keyItem[i].fileName;
	        keyItemInfo.filePath = keyItem[i].filePath;
	        keyItemInfo.description = keyItem[i].description;
	        keyItemInfo.howToGet = keyItem[i].howToGet;
	        keyItemInfo.sortNumber = keyItem[i].sortNumber;

			dickeyItemInfo[keyItem[i].fileName] = keyItemInfo;
			//Debug.Log("keyItem"+dickeyItemInfo[keyItem[i].id].itemName);
		}
		return dickeyItemInfo;

	}

	public class keyItemInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string keyItemName = "";

        [SerializeField]
        public string fileName = "";

        [SerializeField]
        public string filePath = "";

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string howToGet = "";

        [SerializeField]
        public int sortNumber = 0;

        public void ResetData() {
			id = "";
	        keyItemName = "";
			fileName = "";
			filePath = "";
	        description = "";
			howToGet = "";
			sortNumber = 0;
        }
	}
}
