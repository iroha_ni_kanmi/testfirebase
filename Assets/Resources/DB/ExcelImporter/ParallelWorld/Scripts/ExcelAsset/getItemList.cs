﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

[ExcelAsset]
public class getItemList : ScriptableObject
{
	public List<getItemEntity> getItem; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//questのIdをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < getItem.Count ;i++){
			listIds.Add(getItem[i].id);
		}
		return listIds;
	}

	public Dictionary<string, QuestInfo> getItemInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, QuestInfo> dicGetItemInfo = new Dictionary<string, QuestInfo>();

		for(int i = 0; i < getItem.Count ;i++){
			QuestInfo questInfo = new QuestInfo();

			//クエストに紐づいた情報を格納していく
        	questInfo.id = getItem[i].id;
        	questInfo.needItem = getItem[i].needItem;

			dicGetItemInfo[getItem[i].id] = questInfo;
			//Debug.Log("main"+dicQuestInfo[MainQuest[i].id].questName);
		}
		return dicGetItemInfo;

	}

	public class QuestInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string needItem = "";

        public void ResetData() {
        	id = "";
        	needItem = "";
        }
	}
}
