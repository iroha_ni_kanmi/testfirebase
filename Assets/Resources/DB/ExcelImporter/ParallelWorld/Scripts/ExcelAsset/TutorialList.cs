﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class TutorialList : ScriptableObject
{
	public List<TutorialEntity> Tutorial; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < Tutorial.Count ;i++){
			listIds.Add(Tutorial[i].id);
		}
		return listIds;
	}

	public Dictionary<string, TutorialInfo> getDicTutorialInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, TutorialInfo> dicTutorialInfo = new Dictionary<string, TutorialInfo>();

		for(int i = 0; i < Tutorial.Count ;i++){
			TutorialInfo TutorialInfo = new TutorialInfo();

			//クエストに紐づいた情報を格納していく
	        TutorialInfo.id = Tutorial[i].id;
	        TutorialInfo.tutorialName = Tutorial[i].tutorialName;
			
	        TutorialInfo.tutorialDescription.Add(Tutorial[i].tutorialDescription01);
	        TutorialInfo.tutorialDescription.Add(Tutorial[i].tutorialDescription02);
	        TutorialInfo.tutorialDescription.Add(Tutorial[i].tutorialDescription03);

			TutorialInfo.tutorialDescription = deleteNullValue(TutorialInfo.tutorialDescription);

			TutorialInfo.tutorialImage.Add(Tutorial[i].tutorialImage01);
	        TutorialInfo.tutorialImage.Add(Tutorial[i].tutorialImage02);
	        TutorialInfo.tutorialImage.Add(Tutorial[i].tutorialImage03);

			TutorialInfo.tutorialImage = deleteNullValue(TutorialInfo.tutorialImage);

			dicTutorialInfo[Tutorial[i].id] = TutorialInfo;
			//Debug.Log("Tutorial"+dicTutorialInfo[Tutorial[i].id].itemName);
		}
		return dicTutorialInfo;

	}

	private List<string> deleteNullValue(List<string> listObj){
		//""の値があったら、リストから削除する
		List<string> listReturn = new List<string>();

		for(int i = 0; i < listObj.Count(); i ++){
			if(listObj[i] != ""){
				listReturn.Add(listObj[i]);
			}
		}
		return listReturn;

	}

	public class TutorialInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string tutorialName = "";

        [SerializeField]
        public List<string> tutorialDescription = new List<string>();

        [SerializeField]
        public List<string> tutorialImage = new List<string>();

        public void ResetData() {
			id = "";
	        tutorialName = "";
			tutorialDescription = new List<string>();
			tutorialImage = new List<string>();
        }
	}
}
