using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class naoshiQuestList : ScriptableObject
{
	public List<naoshiEntity> naoshiQuest; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < naoshiQuest.Count ;i++){
			listIds.Add(naoshiQuest[i].id);
		}
		return listIds;
	}

	public Dictionary<string, naoshiInfo> getNaoshiInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, naoshiInfo> dicNaoshiInfo = new Dictionary<string, naoshiInfo>();

		for(int i = 0; i < naoshiQuest.Count ;i++){
			naoshiInfo naoshiInfo = new naoshiInfo();

			//クエストに紐づいた情報を格納していく
	        naoshiInfo.dollName = naoshiQuest[i].dollName;
	        naoshiInfo.dollImage = naoshiQuest[i].dollImage;
	        naoshiInfo.needItem = naoshiQuest[i].needItem;
	        naoshiInfo.onlyItems = naoshiQuest[i].onlyItems;
	        naoshiInfo.exceptItems = naoshiQuest[i].exceptItems;
	        naoshiInfo.numberingItemGenre = naoshiQuest[i].numberingItemGenre;
	        naoshiInfo.numberingPoint = naoshiQuest[i].numberingPoint;
	        naoshiInfo.numberingItemName = naoshiQuest[i].numberingItemName;
	        naoshiInfo.timeLimit = naoshiQuest[i].timeLimit;

			dicNaoshiInfo[naoshiQuest[i].id] = naoshiInfo;
			//Debug.Log("naoshi"+dicNaoshiInfo[naoshiQuest[i].id].dollName);
		}
		return dicNaoshiInfo;

	}

	public class naoshiInfo{

        [SerializeField]
        public string dollName = "";

        [SerializeField]
        public string dollImage = "";

        [SerializeField]
        public string needItem = "";

        [SerializeField]
        public string onlyItems = "";

        [SerializeField]
        public string exceptItems = "";

        [SerializeField]
        public string numberingItemGenre = "";

        [SerializeField]
        public string numberingPoint = "";

        [SerializeField]
        public string numberingItemName = "";

        [SerializeField]
        public int timeLimit  = 0;

        public void ResetData() {
	        dollName = "";
	        dollImage = "";
	        needItem = "";
	        onlyItems = "";
	        exceptItems = "";
	        numberingItemGenre = "";
	        numberingPoint = "";
	        numberingItemName = "";
			timeLimit  = 0;
        }
	}
}
