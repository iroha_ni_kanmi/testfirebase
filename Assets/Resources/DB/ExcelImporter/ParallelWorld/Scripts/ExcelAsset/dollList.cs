﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class dollList : ScriptableObject
{
	public List<dollEntity> doll; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listDollKeyName = new List<string>();
		for(int i = 0; i < doll.Count ;i++){
			listDollKeyName.Add(doll[i].dollKeyName);
		}
		return listDollKeyName;
	}

	public Dictionary<string, dollInfo> getDollInfo(){
		//各dollKeyNameに紐づいた情報を返す
		Dictionary<string, dollInfo> dicDollInfo = new Dictionary<string, dollInfo>();

		for(int i = 0; i < doll.Count;i++){
			dollInfo dollInfo = new dollInfo();

			//dollKeyNameに紐づいた情報を格納していく
	        dollInfo.dollKeyName = doll[i].dollKeyName;
	        dollInfo.dollName = doll[i].dollName;
	        dollInfo.dollImageFilePath = doll[i].dollImageFilePath;
            dollInfo.dollDescription = doll[i].dollDescription;
            dollInfo.sortNumber = doll[i].sortNumber;

			dicDollInfo[doll[i].dollKeyName] = dollInfo;
		}
		return dicDollInfo;

	}

	public class dollInfo{

        [SerializeField]
        public string dollKeyName = "";

        [SerializeField]
        public string dollName = "";

        [SerializeField]
        public string dollImageFilePath = "";

        [SerializeField]
        public string dollDescription = "";

        [SerializeField]
        public int sortNumber = 0;

        public void ResetData() {
	        dollKeyName = "";
	        dollName = "";
	        dollImageFilePath = "";
	        dollDescription = "";
			sortNumber = 0;
        }
	}
}
