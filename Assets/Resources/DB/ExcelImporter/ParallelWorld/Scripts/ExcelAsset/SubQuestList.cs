using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class SubQuestList : ScriptableObject
{
	public List<QuestEntity> SubQuest; // Replace 'EntityType' to an actual type that is serializable.
	public List<string> getIds(){
		//questのIdをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < SubQuest.Count ;i++){
			listIds.Add(SubQuest[i].id);
		}
		return listIds;
	}

	public Dictionary<string, QuestInfo> getQuestInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, QuestInfo> dicQuestInfo = new Dictionary<string, QuestInfo>();

		for(int i = 0; i < SubQuest.Count ;i++){
			QuestInfo questInfo = new QuestInfo();

			//クエストに紐づいた情報を格納していく
        	questInfo.questName = SubQuest[i].questName;
        	questInfo.questSubName = SubQuest[i].questSubName;
        	questInfo.genre = SubQuest[i].genre;
        	questInfo.description = SubQuest[i].description;
        	questInfo.howToClear = SubQuest[i].howToClear;
        	questInfo.questcharactorName = SubQuest[i].questcharactorName;
        	questInfo.questIconFilePath = SubQuest[i].questIconFilePath;
        	questInfo.thankPoint = SubQuest[i].thankPoint;
        	questInfo.thankItems = SubQuest[i].thankItems;
        	questInfo.nextQuestId = SubQuest[i].nextQuestId;
        	questInfo.playerRank = SubQuest[i].playerRank;
        	questInfo.storyNo = SubQuest[i].storyNo;

			dicQuestInfo[SubQuest[i].id] = questInfo;
			//Debug.Log("sub"+dicQuestInfo[SubQuest[i].id].questName);
		}
		return dicQuestInfo;

	}

	public class QuestInfo{

        [SerializeField]
        public string questName = "";

        [SerializeField]
        public string questSubName = "";

        [SerializeField]
        public QuestEntity.QuestGenre genre;

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string howToClear = "";

        [SerializeField]
        public string questcharactorName = "";

        [SerializeField]
        public string questIconFilePath = "";

        [SerializeField]
        public int thankPoint = 0;

        [SerializeField]
        public string thankItems = "";

        [SerializeField]
        public string nextQuestId = "";

        [SerializeField]
        public int playerRank = 1;

        [SerializeField]
        public int storyNo = 1;

        public void ResetData() {
        	questName = "";
        	questSubName = "";
        	genre = new QuestEntity.QuestGenre();
        	description = "";
        	howToClear = "";
        	questcharactorName = "";
        	questIconFilePath = "";
        	thankPoint = 0;
        	thankItems = "";
        	nextQuestId = "";
			playerRank = 1;
			storyNo = 1;
        }
	}
}
