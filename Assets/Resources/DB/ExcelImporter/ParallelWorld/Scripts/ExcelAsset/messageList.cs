﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class messageList : ScriptableObject
{
	public List<messageEntity> message; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < message.Count ;i++){
			listIds.Add(message[i].id);
		}
		return listIds;
	}

	public Dictionary<string, messageInfo> getDicMessageInfo(){
		//各idに紐づいた情報を返す
		Dictionary<string, messageInfo> dicMessageInfo = new Dictionary<string, messageInfo>();

		for(int i = 0; i < message.Count ;i++){
			messageInfo messageInfo = new messageInfo();

			//idに紐づいた情報を格納していく
	        messageInfo.id = message[i].id;
	        messageInfo.characterName = message[i].characterName;
	        messageInfo.messageTitle = message[i].messageTitle;
	        messageInfo.messageText = message[i].messageText;
	        messageInfo.imageFilePath = message[i].imageFilePath;
			
			dicMessageInfo[message[i].id] = messageInfo;
		}
		return dicMessageInfo;

	}

	public class messageInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string characterName = "";

        [SerializeField]
        public string messageTitle = "";

        [SerializeField]
        public string messageText = "";

        [SerializeField]
        public string imageFilePath = "";

        public void ResetData() {
			id = "";
	        characterName = "";
			messageTitle = "";
			messageText = "";
			imageFilePath = "";
        }
	}
}
