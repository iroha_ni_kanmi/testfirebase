﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExcelAsset]
public class clothingList : ScriptableObject
{
	public List<clothingEntity> clothing; // Replace 'EntityType' to an actual type that is serializable.

	public List<string> getIds(){
		//Idをリストで返す
		List<string> listIds = new List<string>();
		for(int i = 0; i < clothing.Count ;i++){
			listIds.Add(clothing[i].id);
		}
		return listIds;
	}

	public Dictionary<string, clothingInfo> getClothingInfo_id(){
		//各idに紐づいた情報を返す
		Dictionary<string, clothingInfo> dicclothingInfo = new Dictionary<string, clothingInfo>();

		for(int i = 0; i < clothing.Count ;i++){
			clothingInfo clothingInfo = new clothingInfo();

			//クエストに紐づいた情報を格納していく
	        clothingInfo.id = clothing[i].id;
	        clothingInfo.clothingName = clothing[i].clothingName;
	        clothingInfo.fileName = clothing[i].fileName;
	        clothingInfo.filePath = clothing[i].filePath;
	        clothingInfo.modelName = clothing[i].modelName;
	        clothingInfo.modelFilePath = clothing[i].modelFilePath;
	        clothingInfo.description = clothing[i].description;
	        clothingInfo.howToGet = clothing[i].howToGet;
	        clothingInfo.sortNumber = clothing[i].sortNumber;

			dicclothingInfo[clothing[i].id] = clothingInfo;
			//Debug.Log("clothing"+dicclothingInfo[clothing[i].id].clothingName);
		}
		return dicclothingInfo;

	}

	public Dictionary<string, clothingInfo> getClothingInfo_clothingName(){
		//各idに紐づいた情報を返す
		Dictionary<string, clothingInfo> dicclothingInfo = new Dictionary<string, clothingInfo>();

		for(int i = 0; i < clothing.Count ;i++){
			clothingInfo clothingInfo = new clothingInfo();

			//クエストに紐づいた情報を格納していく
	        clothingInfo.id = clothing[i].id;
	        clothingInfo.clothingName = clothing[i].clothingName;
	        clothingInfo.fileName = clothing[i].fileName;
	        clothingInfo.filePath = clothing[i].filePath;
	        clothingInfo.modelName = clothing[i].modelName;
	        clothingInfo.modelFilePath = clothing[i].modelFilePath;
	        clothingInfo.description = clothing[i].description;
	        clothingInfo.howToGet = clothing[i].howToGet;
	        clothingInfo.sortNumber = clothing[i].sortNumber;

			dicclothingInfo[clothing[i].clothingName] = clothingInfo;
			//Debug.Log("clothing"+dicclothingInfo[clothing[i].id].clothingName);
		}
		return dicclothingInfo;

	}

	public Dictionary<string, clothingInfo> getClothingInfo_fileName(){
		//各idに紐づいた情報を返す
		Dictionary<string, clothingInfo> dicclothingInfo = new Dictionary<string, clothingInfo>();

		for(int i = 0; i < clothing.Count ;i++){
			clothingInfo clothingInfo = new clothingInfo();

			//クエストに紐づいた情報を格納していく
	        clothingInfo.id = clothing[i].id;
	        clothingInfo.clothingName = clothing[i].clothingName;
	        clothingInfo.fileName = clothing[i].fileName;
	        clothingInfo.filePath = clothing[i].filePath;
	        clothingInfo.modelName = clothing[i].modelName;
	        clothingInfo.modelFilePath = clothing[i].modelFilePath;
	        clothingInfo.description = clothing[i].description;
	        clothingInfo.howToGet = clothing[i].howToGet;
	        clothingInfo.sortNumber = clothing[i].sortNumber;

			dicclothingInfo[clothing[i].fileName] = clothingInfo;
			//Debug.Log("clothing"+dicclothingInfo[clothing[i].id].clothingName);
		}
		return dicclothingInfo;

	}

	public class clothingInfo{

        [SerializeField]
        public string id = "";

        [SerializeField]
        public string clothingName = "";

        [SerializeField]
        public string fileName = "";

        [SerializeField]
        public string filePath = "";

        [SerializeField]
        public string modelName = "";

        [SerializeField]
        public string modelFilePath = "";

        [SerializeField]
        public string description = "";

        [SerializeField]
        public string howToGet = "";

        [SerializeField]
        public int sortNumber = 0;

        public void ResetData() {
			id = "";
	        clothingName = "";
			fileName = "";
			filePath = "";
	        modelName = "";
	        modelFilePath = "";
	        description = "";
			howToGet = "";
			sortNumber = 0;
        }
	}
}
