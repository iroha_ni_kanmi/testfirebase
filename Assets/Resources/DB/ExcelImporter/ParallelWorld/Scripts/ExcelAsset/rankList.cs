﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;

[ExcelAsset]
public class rankList : ScriptableObject
{
	public List<rankEntity> rank; // Replace 'EntityType' to an actual type that is serializable.
	public List<int> getRankNoes(){
		//rankのIdをリストで返す
		List<int> listRankNoes = new List<int>();
		for(int i = 0; i < rank.Count ;i++){
			listRankNoes.Add(rank[i].rankNo);
		}
		return listRankNoes;
	}

	public Dictionary<int, rankInfo> getRankInfo(){
		//各ランクナンバーに紐づいた情報を返す
		Dictionary<int, rankInfo> dicrankInfo = new Dictionary<int, rankInfo>();

		for(int i = 0; i < rank.Count ;i++){
			rankInfo rankInfo = new rankInfo();

			//ランクに紐づいた情報を格納していく
        	rankInfo.rankNo = rank[i].rankNo;
        	rankInfo.rankName = rank[i].rankName;
        	rankInfo.rankDescription = rank[i].rankDescription;
        	rankInfo.badgeFilePath = rank[i].badgeFilePath;
        	rankInfo.needThankPoints = rank[i].needThankPoints;
        	rankInfo.rankGenre = rank[i].rankGenre;

            dicrankInfo[rankInfo.rankNo] = rankInfo;
		}
		return dicrankInfo;
	}


	public class rankInfo{

        [SerializeField]
        public int rankNo = 0;

        [SerializeField]
        public string rankName = "";

        [SerializeField]
        public string rankDescription = "";

        [SerializeField]
        public string badgeFilePath = "";

        [SerializeField]
        public int needThankPoints = 0;

        [SerializeField]
        public string rankGenre = "";

        public void ResetData() {
        	rankNo = 0;
        	rankName = "";
        	rankDescription = "";
			badgeFilePath = "";
			needThankPoints = 0;
        	rankGenre = "";
        }
	}
}
