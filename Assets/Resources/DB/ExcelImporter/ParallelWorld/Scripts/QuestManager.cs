﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{

    //データセット用関数
    public MainQuestList mainQuestList;
    public SubQuestList subQuestList;
    public storyList storyList;
    public naoshiQuestList naoshiQuestList;
    public naoshiDialogList naoshiDialogList;
    public materiList materiList;

    //内部判定用関数
    private int phaseNumber;
    private int totalPhaseNumber;
    private float totalPoint;

    //アイテム制限周りのList
    private List<string> listOnlyItems = new List<string>();
    private List<string> listExceptItems = new List<string>();
    //Phaseごとにしか適用できないアイテムのリスト
    private List<string> listPhaseItem = new List<string>();


    //判定用辞書宣言
    private Dictionary<string, MainQuestList.QuestInfo> dicMainQuestInfo = new Dictionary<string, MainQuestList.QuestInfo>();
    private Dictionary<string, SubQuestList.QuestInfo> dicSubQuestInfo = new Dictionary<string, SubQuestList.QuestInfo>();
    private Dictionary<string, naoshiQuestList.naoshiInfo> dicNaoshiQuestInfo = new Dictionary<string, naoshiQuestList.naoshiInfo>();    
    private Dictionary<string, naoshiDialogList.naoshiDialogInfo> dicNaoshiDialogInfo = new Dictionary<string, naoshiDialogList.naoshiDialogInfo>();    
    private Dictionary<int, storyList.storyInfo> dicStoryListInfo = new Dictionary<int, storyList.storyInfo>();    
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_id = new Dictionary<string, materiList.materiInfo>();    
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_name = new Dictionary<string, materiList.materiInfo>(); 

    //判定用クエストデータセット
    private Dictionary<int, naoshiQuestDataSet> dicPhaseQuest = new Dictionary<int, naoshiQuestDataSet>(); 

    //テスト関数（デバッグ用）
    //=======================
    public Text TextItemGenre;
    public Text TextItemName;
    public Text TextPhase;
    public Text TextPoint;
    public Text TextPointToClear;
    public Text TextId;

    public bool testMode = false;
    //=======================ここまで


    void Awake(){
        //データセット
        dicMainQuestInfo = mainQuestList.getQuestInfo();
        dicSubQuestInfo = subQuestList.getQuestInfo();
        dicNaoshiQuestInfo = naoshiQuestList.getNaoshiInfo();
        dicNaoshiDialogInfo = naoshiDialogList.getNaoshiDialogInfo();
        dicStoryListInfo = storyList.getDicStoryInfo();
        dicMateriListInfo_id = materiList.getMateriInfo_id();
        dicMateriListInfo_name = materiList.getMateriInfo_itemName();

        dicPhaseQuest = new Dictionary<int, naoshiQuestDataSet>(); 

    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(testMode){
            if(dicPhaseQuest.ContainsKey(phaseNumber)){
                TextItemGenre.text = dicPhaseQuest[phaseNumber].itemGenre.ToString();
                TextPhase.text = phaseNumber.ToString();
                TextPoint.text = totalPoint.ToString();
                TextItemName.text = dicPhaseQuest[phaseNumber].itemName;
                TextPointToClear.text = totalPhaseNumber.ToString();
            }
        }
    }

    public void setNaoshiQuest(string id){
        testMode = true;

        if(naoshiQuestList.getIds().Contains(id)){

            //データセット
            //===========================Phase
            //なおしクエストデータを取得
            string[] arrItemGenre = dicNaoshiQuestInfo[id].numberingItemGenre.Split(',');
            string[] arrPoint = dicNaoshiQuestInfo[id].numberingPoint.Split(',');
            string[] arrItemName = dicNaoshiQuestInfo[id].numberingItemName.Split(',');
            totalPhaseNumber = arrItemGenre.Length;

            //Excelのアイテムの順序・上限ポイント・特定アイテムを格納
            for(int i = 0; i < arrItemGenre.Length ;i++){
                naoshiQuestDataSet naoshiQuestData = new naoshiQuestDataSet();
                naoshiQuestData.itemGenre = convertStringToItemGenre(arrItemGenre[i]);
                naoshiQuestData.point = int.Parse(arrPoint[i]);
                naoshiQuestData.itemName = arrItemName[i];

                dicPhaseQuest[i] = naoshiQuestData;
                Debug.Log(i);
            }

            phaseNumber = 0;

            //===========================listOnlyItems
            //使用アイテムに制限があるか否か
            if(dicNaoshiQuestInfo[id].onlyItems != "-"){

                //存在した場合は、使用可能なアイテムのリストを作成
                listOnlyItems.AddRange(dicNaoshiQuestInfo[id].onlyItems.Split(','));

            }else{
                listOnlyItems = new List<string>();
            }

            //===========================listExceptItems
            //使用アイテムに除外するものがあるか否か
            if(dicNaoshiQuestInfo[id].exceptItems != "-"){

                //存在した場合は、使用不可能なアイテムのリストを作成
                listExceptItems.AddRange(dicNaoshiQuestInfo[id].exceptItems.Split(','));


            }else{
                listExceptItems = new List<string>();
            }

            //テストデータセット

        }
    }

    public float setNaoshiItem(string itemName){

            //適切なアイテムがドロップされたかどうか
            //アイテムの制限周りの判定
            if(listOnlyItems.Any()){
                //特定アイテムでなければ、pass
                if(!listOnlyItems.Contains(itemName)){
                    Debug.Log("not特定アイテム0P");
                    return 0;
                }
            }
            //Debug.Log("Pass1");

            if(listExceptItems.Any()){
                //特定アイテムを使用していれば、pass
                if(listExceptItems.Contains(itemName)){
                    Debug.Log("禁止アイテム0P");
                    return 0;
                }
            }
            //Debug.Log("Pass2");

            //Phaseによる制限周りの判定

            //辞書に登録されているkey、かつポイント加算対象となるジャンルかどうか
            if(dicPhaseQuest.ContainsKey(phaseNumber) && dicPhaseQuest[phaseNumber].itemGenre == convertStringToItemGenre(dicMateriListInfo_name[itemName].genre)){
                //Debug.Log("Pass3");

                //特定アイテムがあるかどうか
                if(dicPhaseQuest[phaseNumber].itemName != "-"){
                    
                    if(itemName == dicPhaseQuest[phaseNumber].itemName){
                        //特定アイテムであった場合、それに応じたポイントを加算する
                        Debug.Log("特定アイテム20P");
                        return getParameterItem(itemName);

                    }else{
                        //特定アイテム出なかった場合、特定アイテムのポイントを100とした時の
                        //アイテムのポイントを返す
                        Debug.Log("not特定but同属性アイテム5P");
                        return getParameterItem(dicPhaseQuest[phaseNumber].itemName, itemName);

                    }

                }else{
                    //Debug.Log("Pass4");
                    return getParameterItem(itemName);
                }


            }else{
                Debug.Log("フェーズ外");
                Debug.Log(phaseNumber);
                return 0;
            }
    }

    public float getParameterItem(string itemName){
        //アイテムDBから、アイテムのポイントを引っ張ってくる
        if(dicMateriListInfo_name.ContainsKey(itemName)){
            //Debug.Log(dicMateriListInfo_name[itemName].itemPoint);
            return dicMateriListInfo_name[itemName].itemPoint;
        }else{
            Debug.Log("登録されていないアイテムです");
            return 0;
        }
    }

    public float getParameterItem(string correctItemName, string itemName){
        //アイテムDBから、アイテムのポイントを引っ張ってくる
        //同じ属性だが、正解のアイテムでない場合、
        //正解のアイテム:postしたアイテムを100:xとした比率のポイントを返す
        if(dicMateriListInfo_name.ContainsKey(itemName)){
            float itemPoint = dicMateriListInfo_name[itemName].itemPoint;
            float correctItemPoint = dicMateriListInfo_name[correctItemName].itemPoint;
            float returnPoint = (itemPoint/correctItemPoint) * itemPoint; 
            //Debug.Log(dicMateriListInfo_name[itemName].itemPoint);
            //Debug.Log(dicMateriListInfo_name[correctItemName].itemPoint);
            //Debug.Log(returnPoint);
            return returnPoint;
        }else{
            Debug.Log("登録されていないアイテムです");
            return 0;
        }
    }

    public void OnClickTestButton(string itemName){
        totalPoint += setNaoshiItem(itemName);
        testMode = false;
        if(dicPhaseQuest.ContainsKey(phaseNumber) && totalPoint >= dicPhaseQuest[phaseNumber].point){
            //フェーズの更新
            phaseNumber += 1;
            totalPoint = 0;
            testMode = true;

        }
        if(phaseNumber >= totalPhaseNumber){
            //フェーズが完了したら、到達のメッセージ
            //終了の処理
            Debug.Log("Finished!");
        }
        //Debug.Log(phaseNumber);
        //Debug.Log(totalPhaseNumber);
        //Debug.Log(totalPoint);
    }

	public class naoshiQuestDataSet{

        //判定に使用するアイテムに関するデータセット

        [SerializeField]
        public typeItem itemGenre = new typeItem();

        [SerializeField]
        public int point = 0;

        [SerializeField]
        public string itemName = "";


        public void ResetData() {
            itemGenre = new typeItem();
            point = 0;
            itemName = "";
        }

        public enum typeItem{
            hari,
    		ito,
	    	nuno,
	    	wata,
            other,
        }
	}  

    //アイテムの種類を定義
    public naoshiQuestDataSet.typeItem convertStringToItemGenre(string genre){
        switch(genre){
            case "ハリ":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "Hari":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "hari":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "イト":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "Ito":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "ito":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "ヌノ":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "Nuno":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "nuno":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "ワタ":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            case "Wata":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            case "wata":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            default:
            return naoshiQuestDataSet.typeItem.other;
            break;
        }

    }

}
