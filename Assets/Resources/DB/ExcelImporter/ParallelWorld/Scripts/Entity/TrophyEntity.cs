using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TrophyEntity
{
	public string id;
	public string trophyName;
	public string genre;
	public string genreCode;
	public string description;
	public int storyNo;	
	public string clearItemName;
	public string clearItemGenre;
	public string clearItemFilePath;
	public string clearItemDescription;
}