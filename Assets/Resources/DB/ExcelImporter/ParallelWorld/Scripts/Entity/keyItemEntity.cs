﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class keyItemEntity
{
	public string id;
	public string keyItemName;
	public string fileName;
	public string filePath;
	public string description;
	public string howToGet;
	public int sortNumber;
}