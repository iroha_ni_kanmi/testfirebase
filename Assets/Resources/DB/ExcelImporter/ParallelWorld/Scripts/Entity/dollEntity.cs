﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class dollEntity
{
	public string dollKeyName;
	public string dollName;
	public string dollImageFilePath;
	public string dollDescription;
	public int sortNumber;
}