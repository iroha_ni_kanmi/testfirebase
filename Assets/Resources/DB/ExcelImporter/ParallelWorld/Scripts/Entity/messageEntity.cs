﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class messageEntity
{
	public string id;
	public string characterName;
	public string messageTitle;
	public string messageText;
	public string imageFilePath;
	
}