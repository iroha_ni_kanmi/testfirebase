﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class materiEntity
{
	public string id;
	public string itemName;
	public string fileName;
	public string filePath;
	public string genre;
	public string description;
	public string howToGet;
	public string land;
	public float itemPoint;
	public int sortNumber;
}