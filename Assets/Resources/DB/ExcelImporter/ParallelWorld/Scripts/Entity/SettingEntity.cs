﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SettingEntity
{
	public string id;
	public string settingName;
}