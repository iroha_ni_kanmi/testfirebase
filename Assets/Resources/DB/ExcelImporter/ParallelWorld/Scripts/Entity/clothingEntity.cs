﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class clothingEntity
{
	public string id;
	public string clothingName;
	public string fileName;
	public string filePath;
	public string modelName;
	public string modelFilePath;
	public string description;
	public string howToGet;
	public int sortNumber;
}