﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class QuestEntity
{
	public string id;
	public string questName;
	public string questSubName;
	public QuestGenre genre;
	public string description;
	public string howToClear;
	public string questcharactorName;
	public string questIconFilePath;
	public int thankPoint;
	public string thankItems;
	public string nextQuestId;
	public int playerRank;
	public int storyNo;
	
	public enum QuestGenre
	{
		getItem,
		naoshi,
		meetCharacter,
	}
}