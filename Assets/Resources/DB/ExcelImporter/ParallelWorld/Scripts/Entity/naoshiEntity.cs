﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class naoshiEntity
{
	public string id;
	public string dollName;
	public string dollImage;
	public string needItem;
	public string onlyItems;
	public string exceptItems;
	public string numberingItemGenre;
	public string numberingPoint;
	public string numberingItemName;
	public int timeLimit;
}