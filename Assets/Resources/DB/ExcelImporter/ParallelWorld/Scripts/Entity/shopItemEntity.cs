﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class shopItemEntity
{
	public string id;
	public string itemName;
	public string fileName;
	public string filePath;
	public string genre;
	public string description;
	public string tradeItem;
	public int minStoryNo;
	public int sortNumber;
}