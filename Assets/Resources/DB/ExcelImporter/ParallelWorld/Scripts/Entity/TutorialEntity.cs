﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TutorialEntity
{
	public string id;
	public string tutorialName;
	public string tutorialDescription01;
	public string tutorialDescription02;
	public string tutorialDescription03;
	public string tutorialImage01;
	public string tutorialImage02;
	public string tutorialImage03;
}