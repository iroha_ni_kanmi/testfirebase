﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class storyEntity
{
	public int storyNo;
	public string storyName;
	public string storyHeadingName;
	public string storyDescription;
	public string storyImageFilePath;
	public string keyIds;
	public string transitionFilePath;
}