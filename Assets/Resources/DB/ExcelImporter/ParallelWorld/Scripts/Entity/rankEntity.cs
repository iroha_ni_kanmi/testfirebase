﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class rankEntity
{
	public int rankNo;
	public string rankName;
	public string rankDescription;
	public string badgeFilePath;
	public int needThankPoints;
	public string rankGenre;
}