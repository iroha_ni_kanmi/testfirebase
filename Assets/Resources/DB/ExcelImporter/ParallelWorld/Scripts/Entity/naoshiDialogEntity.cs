﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class naoshiDialogEntity
{
	public string id;
	public string clientIcon;
	public string dialogStart;
	public string dialogPhase01;
	public string dialogPhase02;
	public string dialogPhase03;
	public string dialogEnd;
	public string dialogWrong;
	public string dialogRight;
}