﻿using UnityEngine;
using System.Collections;
 
public class NetworkChecker : MonoBehaviour {
    void Update() {
        if ( !Input.GetMouseButtonDown(0) ) {
            return;
        }
 
        // ネットワークの状態を出力
        switch ( Application.internetReachability ) {
        case NetworkReachability.NotReachable:
            Debug.Log("ネットワークには到達不可");
            StatusDataStore.Instance.data.networkStatus = "OFF";
            break;
        case NetworkReachability.ReachableViaCarrierDataNetwork:
            Debug.Log("キャリアデータネットワーク経由で到達可能");
            StatusDataStore.Instance.data.networkStatus = "ON";
            break;
        case NetworkReachability.ReachableViaLocalAreaNetwork:
            Debug.Log("Wifiまたはケーブル経由で到達可能");
            StatusDataStore.Instance.data.networkStatus = "ON";
            break;
        }
    }
}