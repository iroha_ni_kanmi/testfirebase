﻿using UnityEngine;
using System.Collections;

#if UNITY_IOS
using UnityEngine.iOS;
#endif

/// <summary>
/// 定数定義クラス
/// </summary>
static public class Constants
{
    /// <summary>
    /// 基準とするスクリーンサイズの高さ（H）iPhone5を基準とする。
    /// </summary>
    public const float DefaultScreenHeight = 1136;

    /// <summary>
    /// 基準とするスクリーンサイズの幅（W）iPhone5を基準とする。
    /// </summary>
    public const float DefaultScreenWidth = 640;

    public static readonly Vector2 PortraitReferanceResolution = new Vector2(DefaultScreenWidth, DefaultScreenHeight);

    public static readonly Vector2 LandscapeReferanceResolution = new Vector2(DefaultScreenHeight, DefaultScreenWidth);

    public static readonly float ListViewShouldUpdatePullDownHeight = 80;

    public static readonly float AndroidVerticalNavigationBarHeight = 0;

    public static readonly float AndroidHorizontalNavigationBarHeight = 0;

    public static readonly float AndroidStatusBarHeight = 0;

    public static readonly float IosStatusBarHeight = 40f;

    static float appTabBarHeight = 110f;

    public static float AppTabBarHeight { get { return appTabBarHeight; } set { appTabBarHeight = value; } }

    public static readonly float PageTabBarHeight = 130f;

    public static readonly string DateTimeFormatString = "yyyy/MM/dd HH:mm:ss";

    /// <summary>
    /// デフォルト値に対する現在の画面サイズ比率を返す。
    /// </summary>
    /// <returns>The screen scale rate.</returns>
    public static float GetScreenScaleRate()
    {
        return DefaultScreenWidth / (float)Screen.width;
    }

    /// <summary>
    /// デバイスのOSバージョンを返す。iOSでは小数点第一位まで、Androidでは整数桁のみ。
    /// </summary>
    /// <returns>The OS version.</returns>
    public static string GetUserAgent()
    {
        string appInfo = "Bittradeapp/" + Application.version + "/";

#if UNITY_IOS
		return appInfo + "ios(" + Constants.GetDeviceModelName() + "/" + Constants.GetOSVersion() + ")";
#elif UNITY_ANDROID
        return appInfo + "android(" + Constants.GetDeviceModelName() + "/" + Constants.GetOSVersion() + ")";
#endif

        return appInfo;
    }


    /// <summary>
    /// デバイスのOSバージョンを返す。iOSでは小数点第一位まで、Androidでは整数桁のみ。
    /// </summary>
    /// <returns>The OS version.</returns>
    public static string GetOSVersion()
    {
#if UNITY_EDITOR
    #if UNITY_IOS
        return "11.2";
    #else
        return "27";
    #endif
#else
        #if UNITY_IOS
        string versionString = Device.systemVersion;
        return versionString;
        #elif UNITY_ANDROID
        //string androidVersionStr = SystemInfo.operatingSystem;
        //androidVersionStr = androidVersionStr.Split('.')[0];
        //string[] stringSplits = androidVersionStr.Split(' ');
        //return androidVersionStr = stringSplits[stringSplits.Length - 1];

        string androidVersionStr = SystemInfo.operatingSystem;
        androidVersionStr = androidVersionStr.Split('-')[1];
        return androidVersionStr.Split(' ')[0];
        #endif
#endif
	}

	/// <summary>
	/// 端末モデル名を返す。空白を無くした文字列を返す。
	/// </summary>
	/// <returns>The android device name.</returns>
	public static string GetDeviceModelName()
	{
		string modelName = "";

#if UNITY_IOS && !UNITY_EDITOR
		modelName = string.Format("{0}", UnityEngine.iOS.Device.generation);
#elif UNITY_ANDROID && !UNITY_EDITOR
		modelName = SystemInfo.deviceModel;
#else
		modelName = "UnityEditor";
#endif

		return modelName.Replace(" ", "");
	}

	/// <summary>
	/// ステータスバーの高さを返す。
	/// </summary>
	/// <returns>The android status bar height.</returns>
	public static float GetDeviceStatusBarHeight()
	{
#if UNITY_IOS
		return IosStatusBarHeight;
#elif UNITY_ANDROID
		return AndroidStatusBarHeight;
#else
		return 0;
#endif
	}
}

