﻿using UnityEngine;

namespace App
{
    public class CameraFinder : MonoBehaviour
    {
    	//各シーンのキャンバスに付与するスクリプト
    	//DisposeCameraに各シーンで使用しているカメラを付与する
        public Camera DisposeCamera;
        private Canvas _canvas;

        // Use this for initialization
        void Start()
        {
        	//キャンバスを取得
            this._canvas = this.GetComponent<Canvas>();

            if (Camera.main != null
                && this.DisposeCamera != null
                && !System.Object.ReferenceEquals(this.DisposeCamera, Camera.main))
            {
                this._canvas.worldCamera = Camera.main;
                Destroy(this.DisposeCamera.gameObject);
            }
        }
    }
}