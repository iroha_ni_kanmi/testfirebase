﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollBarFade : MonoBehaviour {

    private IDisposable fadeObserver;

    [SerializeField]
    private Image scrollHandle; // スクロールバーのHandle(Image)

    private void Start() {

        scrollHandle.canvasRenderer.SetAlpha(0.0f);

        scrollHandle.canvasRenderer.ObserveEveryValueChanged( X => X.GetAlpha() == 1.0f)
            .Subscribe(_ => {
                scrollHandle.canvasRenderer.SetAlpha(0.0f);
            }).AddTo(this);
    }

    public  void OnBeginDrag() {
        if (fadeObserver != null) {
            fadeObserver.Dispose();
        }
        scrollHandle.CrossFadeAlpha(0.5f, 0.0f, true);  // フェードイン
    }

    public void OnEndDrag() {
        fadeObserver = Observable.Timer(System.TimeSpan.FromSeconds(0.25f))
            .Subscribe(_ => {
                scrollHandle.CrossFadeAlpha(0.0f, 0.25f, true); // フェードアウト
            }).AddTo(this);
    }
}
