﻿ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[CreateAssetMenu(fileName = "ItemDataBase", menuName="CreateItemDataBase")]
public class ItemDataBase : ScriptableObject {
 
	[SerializeField]
	private List<Item> materiLists = new List<Item>();
	[SerializeField]
	private List<Item> keyItemLists = new List<Item>();
	[SerializeField]
	private List<Item> clothingLists = new List<Item>();

	private List<Item> itemLists = new List<Item>();

 
	//-----以下関数---------------

	//　アイテムリストを返す
	public List<Item> GetItemLists() {
		itemLists = new List<Item>();
		itemLists.AddRange(materiLists);
		itemLists.AddRange(keyItemLists);
		itemLists.AddRange(clothingLists);
		return itemLists;
	}

	//[id:sprite]で構成される辞書を返す
	public Dictionary<string, Sprite> GetDicItemSprite(){
		itemLists = GetItemLists();

		Dictionary<string, Sprite> dicItemSprite = new Dictionary<string, Sprite>();

		/*

		Debug.Log(itemLists);
		Debug.Log(itemLists.Count);
		foreach(Item item in itemLists){
			Debug.Log(item);
		}
		 */

		foreach (Item item in itemLists){
			dicItemSprite[item.GetItemid()] = item.GetIcon();
		}

		return dicItemSprite;
	}

	//[id:name]で構成される辞書を返す
	public Dictionary<string, string> GetDicItemName(){
		itemLists = GetItemLists();

		Dictionary<string, string> dicItemName = new Dictionary<string, string>();
		
		foreach (Item item in itemLists){
			dicItemName[item.GetItemid()] = item.GetItemName();
		}

		return dicItemName;
	}

	//[spriteName:id]で構成される辞書を返す
	public Dictionary<string, string> GetDicItemId(){
		itemLists = GetItemLists();

		Dictionary<string, string> dicItemId = new Dictionary<string, string>();
		
		foreach (Item item in itemLists){
			dicItemId[item.GetIcon().name] = item.GetItemid();
		}

		return dicItemId;
	}


	//[id:genre]で構成される辞書を返す
	public Dictionary<string, Item.Genre> GetDicItemGenre(){
		itemLists = GetItemLists();

		Dictionary<string, Item.Genre> dicItemGenre = new Dictionary<string, Item.Genre>();
		
		foreach (Item item in itemLists){
			dicItemGenre[item.GetItemid()] = item.GetItemGenre();
		}

		return dicItemGenre;
	}

	//[id:subGenre]で構成される辞書を返す
	public Dictionary<string, Item.SubGenre> GetDicItemSubGenre(){
		itemLists = GetItemLists();

		Dictionary<string, Item.SubGenre> dicItemSubGenre = new Dictionary<string, Item.SubGenre>();
		
		foreach (Item item in itemLists){
			dicItemSubGenre[item.GetItemid()] = item.GetItemSubGenre();
		}

		return dicItemSubGenre;
	}

	public Dictionary<string, string> GetDicItemDescription(){
		itemLists = GetItemLists();

		Dictionary<string, string> dicItemDescription = new Dictionary<string, string>();
		
		foreach (Item item in itemLists){
			dicItemDescription[item.GetItemid()] = item.GetItemDescription();
		}

		return dicItemDescription;
	}

	public Dictionary<string, string> GetDicItemHowToGet(){
		itemLists = GetItemLists();

		Dictionary<string, string> dicItemHowToGet = new Dictionary<string, string>();
		
		foreach (Item item in itemLists){
			dicItemHowToGet[item.GetItemid()] = item.GetItemHowToGet();
		}

		return dicItemHowToGet;
	}

	//[spriteName:id]で構成される辞書を返す
	public Dictionary<string, float> GetDicItemSortNumber(){
		itemLists = GetItemLists();

		Dictionary<string, float> dicItemSortNumber = new Dictionary<string, float>();
		
		foreach (Item item in itemLists){
			dicItemSortNumber[item.GetItemid()] = item.GetItemSortNumber();
		}

		return dicItemSortNumber;
	}

}
 
