﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour 
{
    void Update () {
	#if UNITY_EDITOR
    	if(EventSystem.current.IsPointerOverGameObject()){
    	    return;
    	}
	#else 
    	if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
        	return;
    	}
	#endif
    }
}