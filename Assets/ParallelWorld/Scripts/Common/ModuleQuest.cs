﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utage;
using ParallelWorld.DBModel;

public class ModuleQuest : MonoBehaviour {
	// Use this for initialization
	public void AddQuestToPlayerDB(string questId){
			/*
			//登録される前のログ
			foreach (string item in PlayerDataStore.Instance.GetQuestIds())
			{
				Debug.Log(item);
			} */

			//新しいクエストの登録
			QuestInventory newQuest = new QuestInventory();
			newQuest.id = questId;
			newQuest.progress = false;

			//要素の追加
            Array.Resize(ref PlayerDataStore.Instance.data.quests, PlayerDataStore.Instance.data.quests.Length + 1);
            PlayerDataStore.Instance.data.quests[PlayerDataStore.Instance.data.quests.Length  - 1] = newQuest;

			/*
			//登録されたかの確認ログ
			Debug.Log("got startQuest");
			foreach (string item in PlayerDataStore.Instance.GetQuestIds())
			{
				Debug.Log(item);
			}
			 */		
	}


	public string ChkQuestProgress(){
		//タップしたクエストIDがプレイヤーDBに存在するか否かを判定
		//存在していればprogressがどうなっているかを確認

		string questId = "";
		List<string> PlayerDataQuestIds = PlayerDataStore.Instance.GetQuestIds();
		Dictionary<string,bool> dicQuest = PlayerDataStore.Instance.GetDicQuest();
		List<string> questIds = TappedObjectInfoDataStore.Instance.data.listQuestId;

		bool startFlg = false;
		bool doingFlg = false;
		string resultKey = "";

		foreach (string id in questIds)
		{
			questId = id;
			if (PlayerDataQuestIds.Contains(id))
			{
				//クエストIDが含まれている場合(進行中クエスト)
				if(dicQuest[id]){
					continue;
				}else{
					doingFlg = true;
					break;
				}

			}else{
				//クエストIDが含まれていない場合(新規クエスト)
				startFlg = true;
				break;

			}
		}

		if(doingFlg){
			Debug.Log("found doingQuest");

			//scenarioLabelの発行
			//進行中のテキスト
			resultKey = "-doing-"+questId;
		}else if(startFlg){
			Debug.Log("found startQuest");

			AddQuestToPlayerDB(questId);

			//scenarioLabelの発行
			//新規のクエスト登録テキスト
			resultKey = "-start-"+questId;

		}else{
			Debug.Log("found completedQuest");

			//scenarioLabelの発行
			//完了クエストのテキスト
			resultKey = "-completed-"+questId;
		}

		return resultKey;

	}

	public bool searchQuestId(string questObjId){
		//クエストのタイプを選別し、進捗状況を見る
		bool flgQuestComplete = false;
		//クエストidを検索
		if(QuestDataStore.Instance.data.mainQuestList.getIds().Contains(questObjId)){
			Debug.Log("mainQuestです");
			flgQuestComplete = switchQuestMode(questObjId,QuestDataStore.Instance.data.mainQuestList.getQuestInfo()[questObjId].genre);
		}else if(QuestDataStore.Instance.data.subQuestList.getIds().Contains(questObjId)){
			Debug.Log("subQuestです");
			flgQuestComplete = switchQuestMode(questObjId,QuestDataStore.Instance.data.subQuestList.getQuestInfo()[questObjId].genre);
		}else{
			//どちらのクエストリストにも登録されていなかった時
			Debug.Log("不正なクエストIDです");
		}
		return flgQuestComplete;
	}

	public bool switchQuestMode(string questObjId, QuestEntity.QuestGenre genre){
		bool flgQuestComplete = false;

		//クエストidから、クエストを読み取る
		switch(genre){
			case QuestEntity.QuestGenre.getItem:
				//getItemの場合の処理
				Debug.Log("Type:getItem");
				string needGetItem = QuestDataStore.Instance.data.getItemList.getItemInfo()[questObjId].needItem;
				if(CheckGetItem(questObjId, needGetItem)){
					//アイテムを十分に持っていたら、クエスト完了
					Debug.Log("getItemOK");
					flgQuestComplete = true;

				}else{
					//アイテムを十分に持っていなければ、クエスト未完了
					Debug.Log("getItemNG");

				}
				break;

			case QuestEntity.QuestGenre.meetCharacter:
				//meetCharacterの場合の処理
				//meetCharacterの会う対象のキャラは、別途処理を行う
				//ここでは、会う対象のキャラ全員にあえているかどうかの、状態の確認を行う
				Debug.Log("Type:meetCharacter");
				Dictionary<string,string[]> dicMeetCharacterIds = PlayerDataStore.Instance.data.GetDicMeetCharacterIds();
				if(dicMeetCharacterIds.ContainsKey(questObjId) && dicMeetCharacterIds[questObjId].Length == 0){
					//全てのキャラクターに会っていれば、クエスト完了の通知
					Debug.Log("meetCharacterOK");
					flgQuestComplete = true;

				}else{
					Debug.Log("meetCharacterNG");
				}
				
				break;

			case QuestEntity.QuestGenre.naoshi:
				//なおしクエストの場合の処理
				Debug.Log("Type:naoshiQuest");
				string needNaoshiItem = QuestDataStore.Instance.data.naoshiQuestList.getNaoshiInfo()[questObjId].needItem;
				if(CheckGetItem(questObjId,needNaoshiItem)){
					//アイテムを十分に持っていたら、なおしクエスト開始
					Debug.Log("getItem(naoshi)OK");
					//flgQuestComplete = true;
					
				}else{
					//アイテムを持っていなければ、クエスト未開始
					Debug.Log("getItem(naoshi)NG");
				}
				break;
		}

		if(flgQuestComplete){
			for(int i = 0; i < PlayerDataStore.Instance.data.quests.Length; i++){
				if(PlayerDataStore.Instance.data.quests[i].id == questObjId){
					PlayerDataStore.Instance.data.quests[i].progress = true;
				}
			}
		}

		return flgQuestComplete;
	}

	public bool CheckGetItem(string id, string needItem){
		//必要アイテムを取得しているか否か
		bool flgItemGetted = false;

		if(needItem == "-"){
			//「-」が入力されていた場合、無条件にクエスト開始 or クリア可能
			flgItemGetted = true;
		}else{
			//string needItem = QuestDataStore.Instance.data.getItemList.getItemInfo()[id].needItem;
			string[] aryNeedItem = needItem.Split(',');

			//所持数確認のdictionary作成
			bool flgGetted = true;
	
			for (int i = 0; i < aryNeedItem.Length; i ++){
				if(aryNeedItem[i].Contains("*")){
					//アイテムの必要数を確認していく
					string nameNeedItem = aryNeedItem[i].Split('*')[0];
					int numberNeedItem = int.Parse(aryNeedItem[i].Split('*')[1]);
				
					//アイテムのidで、アイテム所持数を確認
					string itemId = ItemDataStore.Instance.data.materiList.getMateriInfo_itemName()[nameNeedItem].id;
					Dictionary<string,int> dicItemQuantity = PlayerDataStore.Instance.data.GetDicItemQuantity();
				
					//所持数と、規定の取得数を比較
					//一つでもNGがあれば、フラグをfalseに更新
					if(dicItemQuantity.ContainsKey(itemId) && dicItemQuantity[itemId] < numberNeedItem){
						flgGetted = false;
						//Debug.Log("アイテム："+nameNeedItem);
						//Debug.Log(dicItemQuantity[itemId]);
						//Debug.Log(numberNeedItem);
					}
				
				}
			}
			//不足アイテムがなければ、trueを返す
			if(flgGetted){
				flgItemGetted = true;
			}
		}

		return flgItemGetted;

	}

}
