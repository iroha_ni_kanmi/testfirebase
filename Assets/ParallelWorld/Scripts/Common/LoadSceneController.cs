﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class LoadSceneController{

//#if UNITY_IPHONE

//    [DllImport("__Internal")]
//    private static extern void _LoadSceneObjects(string SceneName,params string[] ExceptScenes);
//#endif

	public static void LoadSceneObjects(string SceneName,params string[] ExceptScenes){
		//シーンの追加		
		string _scenename = SceneName;
		/*
		//シーンをButtonで呼び出したとき、読み込むシーン名を取得するために文字列をSplitする
		if(SceneName.Contains("Button")){
			_scenename = Regex.Split(SceneName, "Button")[0];
		}else{
			_scenename = SceneName;
		}

        bool isLoaded = false;
		//現在読み込まれているシーン数だけループ
		for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount ; i++) {
			//読み込まれているシーンを取得し、
			//これから生成しようとしているシーンが
			//すでに生成されているシーンかどうかを判定する
			string ExistSceneName = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name; 

			//重複したシーンを読み込まないようにする
			if(_scenename == ExistSceneName){
                isLoaded = true;
			}
			
			//メニューと呼びたいシーン以外が余計に残っていたら削除処理
			//if(_scenename != ExistSceneName && !ExceptScenes.Contains(ExistSceneName) && ExistSceneName != "MainView" && ExistSceneName != "ParallelWorld" && ExistSceneName != "GameManager"){
			if(_scenename != ExistSceneName && !ExceptScenes.Contains(ExistSceneName) && ExistSceneName != "ParallelWorld" && ExistSceneName != "GameManager"){
				SceneManager.UnloadSceneAsync(ExistSceneName);
				
				//デバッグログ
				//Debug.Log("Delete"+ExistSceneName);
			}
		}
		
		//シーンが何も読み込まれていなければ、そのまま読み込む
        if (!isLoaded)
        {
            SceneManager.LoadScene(_scenename, LoadSceneMode.Additive);
			//Debug.Log("Loaded"+_scenename);
        }
		*/
		
		SceneManager.LoadScene(_scenename, LoadSceneMode.Additive);

	}
	
	public static void UnLoadScene(string SceneName){
		SceneManager.UnloadScene(SceneName);
	}

	public static void ResetScenes(){
		for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount ; i++) {
			string ExistSceneName = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name;
			//ParallelWorld.SceneとGameManager.Scene以外は削除
			if(ExistSceneName != "ParallelWorld" && ExistSceneName != "GameManager"){
				SceneManager.UnloadSceneAsync(ExistSceneName);
			}
		}
	}

	public static void SetScenes(){
		
		string viewMode = "";

		if(StatusDataStore.Instance.data.viewMode.Contains("Button")){
			viewMode = Regex.Split(StatusDataStore.Instance.data.viewMode, "Button")[0];
		}else{
			viewMode = StatusDataStore.Instance.data.viewMode;
		}

		ResetScenes();

		bool TitleViewFlg = false;
		bool PWFlg = false;

		for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount ; i++) {
			string ExistSceneName = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name;
			if(ExistSceneName == "ParallelWorld"){
				PWFlg = true;
			}else if(ExistSceneName == "TitleView"){
				TitleViewFlg = true;
			}else if(ExistSceneName == viewMode){
				return;
			}
		}

		switch(viewMode){

			case "TitleView":
				//タイトル画面の読み込み
				Debug.Log("doitTitle");
				if(!TitleViewFlg){
					LoadSceneController.LoadSceneObjects("TitleView");
				}
				break;

			case "MainView":
				//メイン画面の読み込み
				Debug.Log("doitMain");

				if(StatusDataStore.Instance.data.loadingStatus){
					LoadSceneController.LoadSceneObjects("MainView");
					LoadSceneController.LoadSceneObjects("TransitionView");
					LoadSceneController.LoadSceneObjects("ParallelWorld");
				}else{
					if(!PWFlg){
						LoadSceneController.LoadSceneObjects("ParallelWorld");
					}
					LoadSceneController.LoadSceneObjects("MainView");
				}
				break;

			case "AreaView":
				//エリア画面の読み込み
				LoadSceneController.LoadSceneObjects("AreaView");
				break;

			case "CreateView":
				//なおし画面の読み込み
				LoadSceneController.LoadSceneObjects("CreateView");
				break;

			case "InfoView":
				//インフォ画面の読み込み
				LoadSceneController.LoadSceneObjects("InfoView");
				break;

			case "ItemView":
				//アイテム画面の読み込み
				LoadSceneController.LoadSceneObjects("ItemView");
				break;

			case "QuestView":
				//クエスト画面の読み込み
				LoadSceneController.LoadSceneObjects("QuestView");
				break;

			case "SettingView":
				//設定画面の読み込み
				LoadSceneController.LoadSceneObjects("SettingView");
				break;

			case "StoryView":
				//ストーリー画面の読み込み
				LoadSceneController.LoadSceneObjects("StoryView");
				break;

    	    case "TutorialView":
                //チュートリアル画面の読み込み
        	    LoadSceneController.LoadSceneObjects("TutorialView");
                break;

			case "TrophyView":
				//トロフィー画面の読み込み
				LoadSceneController.LoadSceneObjects("TrophyView");
				break;

			case "RankView":
				//称号画面の読み込み
				LoadSceneController.LoadSceneObjects("RankView");
				break;

			case "DollView":
				//ぬいぐるみ一覧画面の読み込み
				LoadSceneController.LoadSceneObjects("DollView");
				break;

			case "LetterView":
				//お便り一覧画面の読み込み
				LoadSceneController.LoadSceneObjects("LetterView");
				break;

			case "DialogView":
				//会話画面の読み込み
				LoadSceneController.LoadSceneObjects("DialogView");
				break;
	    }
	}


}
