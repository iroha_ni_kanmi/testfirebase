﻿ 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[CreateAssetMenu(fileName = "QuestDataBase", menuName="CreateQuestDataBase")]
public class QuestModelDataBase : ScriptableObject {
 
	[SerializeField]
	private List<QuestModel> mainQuestLists = new List<QuestModel>();
	[SerializeField]
	private List<QuestModel> subQuestLists = new List<QuestModel>();

	private List<QuestModel> QuestModelLists = new List<QuestModel>();

 
	//-----以下関数---------------

	//　クエストのリストを返す
	public List<QuestModel> GetQuestModelLists() {
		QuestModelLists.AddRange(mainQuestLists);
		QuestModelLists.AddRange(subQuestLists);
		return QuestModelLists;
	}

	//[id:questIcon]で構成される辞書を返す
	public Dictionary<string, Sprite> GetDicQuestIcon(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, Sprite> dicQuestIcon = new Dictionary<string, Sprite>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestIcon[questModel.GetQuestid()] = questModel.GetIcon();
		}

		return dicQuestIcon;
	}

	//[id:questName]で構成される辞書を返す
	public Dictionary<string, string> GetDicQuestName(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, string> dicQuestName = new Dictionary<string, string>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestName[questModel.GetQuestid()] = questModel.GetQuestName();
		}

		return dicQuestName;
	}

	//[id:questSubName]で構成される辞書を返す
	public Dictionary<string, string> GetDicQuestSubName(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, string> dicQuestSubName = new Dictionary<string, string>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestSubName[questModel.GetQuestid()] = questModel.GetQuestSubName();
		}

		return dicQuestSubName;
	}

	//[id:Genre]で構成される辞書を返す
	public Dictionary<string, QuestModel.Genre> GetDicQuestGenre(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, QuestModel.Genre> dicQuestGenre = new Dictionary<string, QuestModel.Genre>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestGenre[questModel.GetQuestid()] = questModel.GetQuestGenre();
		}

		return dicQuestGenre;
	}

	//[id:Purpose]で構成される辞書を返す
	public Dictionary<string, QuestModel.Purpose> GetDicQuestPurpose(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, QuestModel.Purpose> dicQuestPurpose = new Dictionary<string, QuestModel.Purpose>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestPurpose[questModel.GetQuestid()] = questModel.GetQuestPurpose();
		}

		return dicQuestPurpose;
	}

	//[id:questDescription]で構成される辞書を返す
	public Dictionary<string, string> GetDicQuestDescription(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, string> dicQuestDescription = new Dictionary<string, string>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestDescription[questModel.GetQuestid()] = questModel.GetDescription();
		}

		return dicQuestDescription;
	}

	//[id:questHowToClear]で構成される辞書を返す
	public Dictionary<string, string> GetDicQuestHowToClear(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, string> dicQuestHowToClear = new Dictionary<string, string>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestHowToClear[questModel.GetQuestid()] = questModel.GetHowToClear();
		}

		return dicQuestHowToClear;
	}

	//[id:questHowToClear]で構成される辞書を返す
	public Dictionary<string, int> GetDicQuestThankPoint(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, int> dicQuestThankPoint = new Dictionary<string, int>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestThankPoint[questModel.GetQuestid()] = questModel.GetQuestThankPoint();
		}

		return dicQuestThankPoint;
	}

	//[id:QuestItemTable]で構成される辞書を返す
	public Dictionary<string, QuestModel.QuestItemTable> GetDicQuestItemTable(){
		QuestModelLists = GetQuestModelLists();


		Dictionary<string, QuestModel.QuestItemTable> dicQuestItemTable = new Dictionary<string, QuestModel.QuestItemTable>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestItemTable[questModel.GetQuestid()] = questModel.GetQuestItemTable();
		}

		return dicQuestItemTable;
	}

	//[id:questName]で構成される辞書を返す
	public Dictionary<string, GameObject> GetDicPlaceObj(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, GameObject> dicQuestPlaceObj = new Dictionary<string, GameObject>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestPlaceObj[questModel.GetQuestid()] = questModel.GetPlaceObj();
		}

		return dicQuestPlaceObj;
	}

	//[id:questName]で構成される辞書を返す
	public Dictionary<string, QuestModel> GetListCharactor(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, QuestModel> dicCharactorList = new Dictionary<string, QuestModel>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicCharactorList[questModel.GetQuestid()] = questModel;
		}

		return dicCharactorList;
	}

	//[id:DollImage]で構成される辞書を返す
	public Dictionary<string, Sprite> GetDicQuestDollImage(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, Sprite> dicQuestDollImage = new Dictionary<string, Sprite>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestDollImage[questModel.GetQuestid()] = questModel.GetDollImage();
		}

		return dicQuestDollImage;
	}

	//[id:QuestItemTable]で構成される辞書を返す
	public Dictionary<string, QuestModel.QuestDollPointTable> GetDicQuestDollPoint(){
		QuestModelLists = GetQuestModelLists();


		Dictionary<string, QuestModel.QuestDollPointTable> dicQuestDollPoint = new Dictionary<string, QuestModel.QuestDollPointTable>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestDollPoint[questModel.GetQuestid()] = questModel.GetQuestDollPoint();
		}

		return dicQuestDollPoint;
	}

	//[id:DollImage]で構成される辞書を返す
	public Dictionary<string, bool> GetDicQuestCompleteFlg(){
		QuestModelLists = GetQuestModelLists();

		Dictionary<string, bool> dicQuestCompleteFlg = new Dictionary<string, bool>();
		
		foreach (QuestModel questModel in QuestModelLists){
			dicQuestCompleteFlg[questModel.GetQuestid()] = questModel.GetCompleteFlg();
		}

		return dicQuestCompleteFlg;
	}
}