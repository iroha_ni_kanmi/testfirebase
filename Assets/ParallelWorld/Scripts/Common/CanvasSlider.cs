﻿//	以下のサイトから拝借しています。(田巻)
//	http://hoge465.seesaa.net/article/421400008.html
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSlider : MonoBehaviour {
	
    public AnimationCurve animCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public Vector3 inPosition;        // スライドイン後の位置
    public Vector3 outPosition;      // スライドアウト後の位置
    public float duration = 1.0f;    // スライド時間（秒）

    // スライドインポジションに移動する
    public void ToInPosition()
    {
        transform.localPosition = inPosition;
    }

    // スライドアウトポジションに移動する
    public void ToOutPosition() {
        transform.localPosition = outPosition;
    }

    // スライドイン（Pauseボタンが押されたときに、これを呼ぶ）
    public void SlideIn(Action onFinished = null){
        StartCoroutine( StartSlidePanel(true, onFinished) );
    }
 
    // スライドアウト
    public void SlideOut( Action onFinished = null ){
        StartCoroutine( StartSlidePanel(false, onFinished) );
    }
 
    private IEnumerator StartSlidePanel( bool isSlideIn, Action onFinished ){
        float startTime = Time.time;    // 開始時間
        Vector3 startPos = transform.localPosition;  // 開始位置
        Vector3 moveDistance;            // 移動距離および方向
 
        if( isSlideIn ){
            moveDistance = (inPosition - startPos);
        }else{
            moveDistance = (outPosition - startPos);
           }
 
        while((Time.time - startTime) < duration){
            transform.localPosition = startPos + moveDistance * animCurve.Evaluate((Time.time - startTime) / duration);

            yield return 0;        // 1フレーム後、再開
        }
        transform.localPosition = startPos + moveDistance;

        if( onFinished != null ) {
            onFinished();
        }
    }
}
