﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


static class MonoBehaviourExtensions
{
	public static T CreateGameObject<T>(Transform parent) where T : MonoBehaviour
	{
		string name = typeof(T).Name;
		GameObject gameObject = new GameObject(name);
		gameObject.transform.SetParent (parent.transform);
		return gameObject.AddComponent<T> ();
	}
	public static T CreateGameObject<T>() where T : MonoBehaviour
	{
		string name = typeof(T).Name;
		GameObject gameObject = new GameObject(name);
		return gameObject.AddComponent<T> ();
	}

	public static T CreateGameObject<T>(this MonoBehaviour parent) where T : MonoBehaviour
	{
		string name = typeof(T).Name;
		GameObject gameObject = new GameObject(name);
		gameObject.transform.SetParent (parent.transform);
		return gameObject.AddComponent<T> ();
	}

}

public static class GameObjectExtensions
{
    public static T[] GetComponentsInChildrenWithoutSelf<T>(this GameObject self) where T : Component
    {
        return self.GetComponentsInChildren<T>().Where(c => self != c.gameObject).ToArray();
    }

    public static bool HasChild(this GameObject gameObject)
    {
        return 0 < gameObject.transform.childCount;
    }

    /// <summary>
    /// 指定されたコンポーネントがアタッチされているかどうかを返します
    /// </summary>
    public static bool HasComponent<T>(this GameObject self) where T : Component
    {
        return self.GetComponent<T>() != null;
    }
}

/// <summary>
/// Transform 型の拡張メソッドを管理するクラス
/// </summary>
public static partial class TransformExtensions
{
    public static bool HasChild(this Transform transform)
    {
        return 0 < transform.childCount;
    }
}