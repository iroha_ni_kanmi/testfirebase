﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using AM1.Nav;

/// <summary>
/// マウスのある場所にターゲットを設定するテスト用クラスです。
/// resetDestinationDistanceに設定した距離よりも、マウスが移動していたら、新しくルートを設定します。
/// </summary>
[RequireComponent(typeof(NavController))]
public class NavMeshWalkerController : MonoBehaviour
{
    //どう森ライクな移動を実装する
    //NavMeshWalkerを使用する前提

    [Header("NavMesh")]
    [TooltipAttribute("目的地を変更する距離"), SerializeField]
    float resetDestinationDistance = 0.5f;
    Vector3 setDestination = Vector3.zero;

    private bool flgDrag,flgClick;
    private Vector3 mpos;
    public GameObject NavMeshWalkerObj;
    public NavMeshAgent agent;

    NavController navCon;

    [Header("GOCoordinatesRaycastSetting"), SerializeField]
	public GameObject projectorPrefab;
	private GameObject currentProjector;

    //内部関数
    private bool flgMove = false;

    private void Awake()
    {
        navCon = GetComponent<NavController>();
        agent = GetComponent<NavMeshAgent>();
    }

    private void FixedUpdate()
    {
        if(flgDrag){
            //ドラックしている間は、マウスの方向へ移動し続ける
            SetTargetOnDrag(Input.mousePosition);
        }
        if(flgClick){
            //クリックしたポイントへ移動
            SetTargetOnClick(mpos);
        }

        //クリック移動かつ、移動が完了したら
        //ピンを削除する処理
        
        //pathPendingがFalseの状態でもなぜか削除処理まで行ってしまうので、
        //確実に移動が開始（pathPending=true）の状態(flgMove=true)の時に削除処理を限定する
        if(agent.pathPending){
            flgMove = true;
        }

        if(flgMove){
            //経路を保持しておらず、立ち止まっている状態の時
            //移動フラグをfalseにし、ピンを削除する
            if(!agent.pathPending && !agent.hasPath){
                flgMove = false;

	        	if (currentProjector != null){
		    		GameObject.Destroy(currentProjector);
                    
                }
            }

        }
        
    	
        
    }

    private void SetTargetOnDrag(Vector3 targetPoint){
        //NavMeshWalkerのスクリプトから引用
        //ドラッグ専用の処理
        mpos = targetPoint;
        Ray ray = Camera.main.ScreenPointToRay(mpos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (!hit.collider.CompareTag("Player"))
            {
                Vector3 target = hit.point;
                if (Vector3.Distance(target, setDestination) > resetDestinationDistance)
                {
                    //カメラが動き続ける限り、
                    //キャラクターの向かう座標も動き続ける

                    setDestination = target;
                    navCon.SetDestination(target);

                    //ピンを初期化
    	    	    if (currentProjector != null){
	    		    	GameObject.Destroy(currentProjector);
                    }

                }
            }
        }
        //移動を止める範囲を指定
        agent.stoppingDistance = agent.remainingDistance*0.3f;

    }

    private void SetTargetOnClick(Vector3 targetPoint){
        //NavMeshWalkerのスクリプトから引用
        mpos = targetPoint;
        Ray ray = Camera.main.ScreenPointToRay(mpos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (!hit.collider.CompareTag("Player"))
            {
                Vector3 target = hit.point;
                if (Vector3.Distance(target, setDestination) > resetDestinationDistance)
                {
                    setDestination = target;
                    navCon.SetDestination(target);
                    

                    //Add a simple projector to the tapped point
                    //ピンを初期化
                    if (currentProjector != null){
            			GameObject.Destroy(currentProjector);
                        }
                    //移動ポイントに立てるピンを生成
	            	currentProjector = GameObject.Instantiate(projectorPrefab);
		            target.y += 5.5f;
    		        currentProjector.transform.position = target;
                }
                
            }
        }

        flgClick = false;
        //移動を止める範囲を指定
        //agent.stoppingDistance = agent.remainingDistance*0.3f;

    }

    public void OnDragArea(){
        //ドラッグの判定をtrueに
        flgDrag = true;
        flgClick = false;
    }
    public void OnClickArea(){
        if(flgDrag){
            //ドラッグ最中であれば、プレイヤーの現在地で止まるようにする
            setDestination = NavMeshWalkerObj.transform.position;
            navCon.SetDestination(NavMeshWalkerObj.transform.position);
            flgDrag = false;
        }else{
            //未ドラックであれば、プレイヤーがクリックした地点まで行くようにする
            flgClick = true;
            mpos = Input.mousePosition;
            Debug.Log("click");
        }
    }

    public void OnPointerExitArea(){
        if(flgDrag){
            //ドラッグ最中であれば、プレイヤーの現在地で止まるようにする
            setDestination = NavMeshWalkerObj.transform.position;
            navCon.SetDestination(NavMeshWalkerObj.transform.position);
            flgDrag = false;
        }
    }

}
