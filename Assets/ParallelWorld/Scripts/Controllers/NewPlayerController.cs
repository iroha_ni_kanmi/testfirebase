﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GoShared;

public class NewPlayerController : MonoBehaviour {

    [Header("MoveAvaterSetting"), SerializeField]
    private NavMeshAgent agent;
    private bool moveFlg, doubleClickFlg, drag, click;
    public GameObject MovablePanel;
    
    [Header("GOCoordinatesRaycastSetting"), SerializeField]
	public GameObject projectorPrefab;
	private GameObject currentProjector;

	public bool debugLog = false;
	public bool atomic = true;


    [Header("DebugSetting"), SerializeField]
    //軌道test用変数
    public LineRenderer line;
	private NavMeshPath path;
	
    [Header("DragPlayerSetting"), SerializeField]
	//　レイを飛ばす距離
	private float rayRange = 100000f;
	//　移動する位置
	private Vector3 targetPosition;
	//　速度
	private Vector3 velocity;
	//　移動スピード
	[SerializeField]
	private float moveSpeed = 3f;
	//　マウスクリックで移動する位置を決定するかどうか
	[SerializeField]
	private bool mouseDownMode = true;
	//　スムースにキャラクターの向きを変更するかどうか
	[SerializeField]
	private bool smoothRotateMode = true;
	//　回転度合い
	[SerializeField]
	private float smoothRotateSpeed = 500f;
	private CharacterController characterController;

	
	private void Start() {
		characterController = GetComponent<CharacterController>();
		targetPosition = transform.position;
		velocity = Vector3.zero;
        agent = GetComponent<NavMeshAgent>();
	}

    void FixedUpdate(){

        if(drag) {

			if (currentProjector != null && atomic)
					GameObject.Destroy(currentProjector);
					
            velocity = Vector3.zero;
			//　マウスクリックまたはmouseDownModeがOffの時マウスの位置を移動する位置にする
			if(Input.GetButton("Fire1") || !mouseDownMode) {
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit, rayRange)) {
					//targetPosition = hit.point;
					Vector3 worldVector = hit.point;
					agent.SetDestination( worldVector );
				}else{
					//Debug.Log("not hit");
					Debug.Log(LayerMask.GetMask ("Field"));
				}
			}

			//　移動の目的地と0.1mより距離がある時は速度を計算
			if(Vector3.Distance(transform.position, targetPosition) > 0.1f) {
				var moveDirection = (targetPosition - transform.position).normalized;
				velocity = new Vector3(moveDirection.x * moveSpeed, velocity.y, moveDirection.z * moveSpeed);
				//　スムースモードの時は徐々にキャラクターの向きを変更する
				if(smoothRotateMode) {
					transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(new Vector3(-moveDirection.x, 0, -moveDirection.z)), smoothRotateSpeed * Time.deltaTime);
					//　スムースモードでなければ一気に目的地の方向を向かせる
				} else {
					transform.LookAt(transform.position + new Vector3(-moveDirection.x, 0, -moveDirection.z));
				}
				//　アニメーションパラメータの設定
				//animator.SetFloat("Speed", moveDirection.magnitude);
				//　目的地に近付いたら走るアニメーションをやめる
			} else {
				//animator.SetFloat("Speed", 0f);
			}
		    //}
 
		    velocity.y += Physics.gravity.y * Time.deltaTime;
		    characterController.Move(velocity * Time.deltaTime);

        }
    	
        if (click && !drag) {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {

                //From the raycast data it's easy to get the vector3 of the hit point 
                Vector3 worldVector = hit.point;
                //And it's just as easy to get the gps coordinate of the hit point.
                //Coordinates gpsCoordinates = Coordinates.convertVectorToCoordinates(hit.point);

                agent.SetDestination( worldVector );
                
                //移動を止める範囲を指定
                agent.stoppingDistance = agent.remainingDistance*0.3f;
                
                // 軌道可視化スクリプト
                // 経路取得用のインスタンス作成
		        path = new NavMeshPath ();
    		    // 明示的な経路計算実行
        		agent.CalculatePath (worldVector, path);

		        // LineRendererで経路描画
        		line.SetVertexCount (path.corners.Length);
        		line.SetPositions (path.corners);

                //GOCoordinatesRaycast処理
				if (currentProjector != null && atomic)
					GameObject.Destroy(currentProjector);

				//Add a simple projector to the tapped point
				currentProjector = GameObject.Instantiate(projectorPrefab);
				worldVector.y += 5.5f;
				currentProjector.transform.position = worldVector;
        		
//        		Debug.Log(hit.collider.gameObject.name);
                float distance = 100; // 飛ばす&表示するRayの長さ
                float duration = 3;   // 表示期間（秒）
                Debug.DrawRay (ray.origin, ray.direction * distance, Color.red, duration, false);

                if (Physics.Raycast (ray, out hit, distance)) {
                    GameObject hitObject = hit.collider.gameObject;
                }
            }
            
    	}
        //移動開始の判定
        if(agent.hasPath){
            moveFlg = true;
        	}
        
        //移動が終了したら、NavMeshSurfaceをもつオブジェクト(MovablePlane)を移動させる
        if(!agent.hasPath && moveFlg){
            MovablePanel.transform.position = agent.transform.position;
            moveFlg = false;
			if (currentProjector != null && atomic)
					GameObject.Destroy(currentProjector);
//            Debug.Log("moveFlg = false");
        	}
        click = false;
    }
        
	
	public void OnClickMovableArea(){
        //Debug.Log("hit");
        if(drag || doubleClickFlg){
            drag = false;
            doubleClickFlg = false;
        }else{
			if(StatusDataStore.Instance.data.movableStatus){
	            click = true;
			}else{
				StatusDataStore.Instance.data.movableStatus = true;
			}
        }
	}

	public void OnDragMovableArea(){
        //Debug.Log("hit");
        if(!Input.GetMouseButton(1)){
            drag = true;
        }else{
            drag = false;
            doubleClickFlg = true;
        }
        
	}
}
