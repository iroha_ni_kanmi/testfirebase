﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour
{
    
    [SerializeField]
    UnityEngine.Audio.AudioMixer mixer;
    
    public Slider BGMSlider;
    public Slider SESlider;

    // Use this for initialization
    void Start()
    {
        BGMSlider.onValueChanged.AddListener (setBGMVolume);
        SESlider.onValueChanged.AddListener (setSEVolume);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public float masterVolume
    {
        set { mixer.SetFloat("MasterVolume", Mathf.Lerp(-80, 0, value)); }
    }

    public float bgmVolume
    {
        set { mixer.SetFloat("BGMVolume", Mathf.Lerp(-80, 0, value)); }
    }

    public float seVolume
    {
        set { mixer.SetFloat("SEVolume", Mathf.Lerp(-80, 0, value)); }
    }

    public void setBGMVolume(float value){
        SettingDataStore.Instance.data.bgmVolume = value;
        SettingDataStore.Instance.Save();
    }

    public void setSEVolume(float value){
        SettingDataStore.Instance.data.seVolume = value;
        SettingDataStore.Instance.Save();        
    }
}