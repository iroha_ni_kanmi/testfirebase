﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class QuestDataStore : MonoBehaviour {
	
	// シングルトン
    private static QuestDataStore _instance = null;
	// シングルトン
    public static QuestDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<QuestDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class QuestData {
        //クエストID保持用
        [SerializeField]
        public string questId = "";
        //メインクエストDB
        [SerializeField]
        public MainQuestList mainQuestList;
        //サブクエストDB
        [SerializeField]
        public SubQuestList subQuestList;
        //ストーリーDB
        [SerializeField]
        public storyList storyList;
        //なおしクエストDB
        [SerializeField]
        public naoshiQuestList naoshiQuestList;
        //なおし画面依頼人ダイアログDB
        [SerializeField]
        public naoshiDialogList naoshiDialogList;
        //getItemクエストDB
        [SerializeField]
        public getItemList getItemList;
        //meetCharacterクエストDB
        [SerializeField]
        public meetCharacterList meetCharacterList;
        //トロフィーDB
        [SerializeField]
        public TrophyList trophyList;
        //プレイヤーランクDB
        [SerializeField]
        public rankList rankList;
        //プレイヤーランクDB
        [SerializeField]
        public dollList dollList;
        //チュートリアルDB
        [SerializeField]
        public TutorialList tutorialList;
        //チュートリアルDB
        [SerializeField]
        public messageList messageList;

        
        // ダイアログコントローラー付属のGameObject
        [SerializeField]
        public GameObject DialogControllerObj;
        // questObjId
        [SerializeField]
        public string questObjId;

        public void ResetData() {
            questId = "";

            
            var mainQuestList = ScriptableObject.CreateInstance(typeof(MainQuestList));
            var subQuestList = ScriptableObject.CreateInstance(typeof(SubQuestList));
            var storyList = ScriptableObject.CreateInstance(typeof(storyList));
            var naoshiQuestList = ScriptableObject.CreateInstance(typeof(naoshiQuestList));
            var naoshiDialogList = ScriptableObject.CreateInstance(typeof(naoshiDialogList));
            var getItemList = ScriptableObject.CreateInstance(typeof(getItemList));
            var meetCharacterList = ScriptableObject.CreateInstance(typeof(meetCharacterList));
            var trophyList = ScriptableObject.CreateInstance(typeof(TrophyList));
            var rankList = ScriptableObject.CreateInstance(typeof(rankList));
            var dollList = ScriptableObject.CreateInstance(typeof(dollList));
            var tutorialList = ScriptableObject.CreateInstance(typeof(TutorialList));
            var messageList = ScriptableObject.CreateInstance(typeof(messageList));

            DialogControllerObj = new GameObject();
            questObjId = "";

        }

    }

    
	
    // 保存データ
    public QuestData data = new QuestData();

    // 保存キー
    private const string saveKey = "SaveQuestData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<QuestData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}

}
