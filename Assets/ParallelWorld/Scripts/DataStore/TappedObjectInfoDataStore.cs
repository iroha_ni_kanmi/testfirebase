﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TappedObjectInfoDataStore : MonoBehaviour {
	
	// シングルトン
    private static TappedObjectInfoDataStore _instance = null;
	// シングルトン
    public static TappedObjectInfoDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<TappedObjectInfoDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class TappedObjectInfoData {
		// オブジェクトの名前
		public string objName;
		// オブジェクトのモデル
		public GameObject objModel;
        // クエストIDのリスト
        [SerializeField]
        public List<string> listQuestId;
		// クエスト対象オブジェクトか判定するためのクエストid
		public string questObjId;
        
        public void ResetData() {
			objName = "";
			objModel = new GameObject();
			listQuestId = new List<string>();
			questObjId = "";
        }
    }
	
    // 保存データ
    public TappedObjectInfoData data = new TappedObjectInfoData();

    // 保存キー
    private const string saveKey = "SaveTappedObjectInfoData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<TappedObjectInfoData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}

}
