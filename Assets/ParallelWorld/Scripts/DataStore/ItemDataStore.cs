﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ItemDataStore : MonoBehaviour {
	
	// シングルトン
    private static ItemDataStore _instance = null;
	// シングルトン
    public static ItemDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<ItemDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class ItemData {

        //マテリDB
        [SerializeField]
        public materiList materiList;

        //衣服DB
        [SerializeField]
        public clothingList clothingList;

        //キーアイテムDB
        [SerializeField]
        public keyItemList keyItemList;

        //ショップアイテムDB
        [SerializeField]
        public shopItemList shopItemList;
        

        public void ResetData() {

            var materiList = ScriptableObject.CreateInstance(typeof(materiList));
            var clothingList = ScriptableObject.CreateInstance(typeof(clothingList));
            var keyItemList = ScriptableObject.CreateInstance(typeof(keyItemList));
            var shopItemList = ScriptableObject.CreateInstance(typeof(shopItemList));
        }
    }
	
    // 保存データ
    public ItemData data = new ItemData();

    // 保存キー
    private const string saveKey = "SaveItemData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<ItemData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}

}
