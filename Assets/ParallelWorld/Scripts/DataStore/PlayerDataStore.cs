﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using ParallelWorld.DBModel;

public class PlayerDataStore : MonoBehaviour {
	
	// シングルトン
    private static PlayerDataStore _instance = null;
	// シングルトン
    public static PlayerDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<PlayerDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	
    // 保存データ
    public PlayerData data = new PlayerData();

    // 保存キー
    private const string saveKey = "SavePlayerData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<PlayerData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}



	//ゲーム内で判定用に使用する値を、取得する関数

	public List<string> GetQuestIds(){
		//現在進行中のクエストのID一覧を取得する
		List<string> questIdList = new List<string>();
		foreach (QuestInventory quest in data.quests)
		{
			questIdList.Add(quest.id);
		}
		return questIdList;
	}

	public Dictionary<string,bool> GetDicQuest(){
		//現在進行中のクエストのIDと、クエスト情報一覧を取得する
		Dictionary<string,bool> dicQuest = new Dictionary<string,bool>();
		foreach (QuestInventory quest in data.quests)
		{
			dicQuest.Add(quest.id, quest.progress);
		}
		return dicQuest;
	}
	
	public Dictionary<string,bool> GetDicDoll(){
		//なおしたぬいぐるみ一覧と、ぬいぐるみ一覧画面で確認したかのチェックを、辞書型で取得する
		Dictionary<string,bool> dicDoll = new Dictionary<string,bool>();
		foreach (createdDoll createDoll in data.createdDollList){
			dicDoll.Add(createDoll.dollName, createDoll.check);
		}
		return dicDoll;
	}
}
