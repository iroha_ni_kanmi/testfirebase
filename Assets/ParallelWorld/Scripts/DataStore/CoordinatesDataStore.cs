﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using GoShared;

public class CoordinatesDataStore : MonoBehaviour {
	// シングルトン
    private static CoordinatesDataStore _instance = null;
	// シングルトン
    public static CoordinatesDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<CoordinatesDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class CoordinatesData {
        // 最後にいた座標
        [SerializeField]
        public Coordinates lastCoordinates;
        // AreaA座標
        [SerializeField]
        public Coordinates AreaA;
        // AreaB座標
        [SerializeField]
        public Coordinates AreaB;
        // AreaC座標
        [SerializeField]
        public Coordinates AreaC;
        
        //locationManager
        [SerializeField]
        public LocationManager locationManager;        

        public void ResetData() {
            lastCoordinates = new Coordinates(35.6895304, 139.706899);
            AreaA = new Coordinates( 35.6895304, 139.706899 );
            AreaB = new Coordinates( 35.6595807, 139.6986655 );
            AreaC = new Coordinates( 35.6812021, 139.7665688 );
            locationManager = null;
        }
    }
	
    // 保存データ
    public CoordinatesData data = new CoordinatesData();

    // 保存キー
    private const string saveKey = "SaveCoordinatesData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<CoordinatesData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}

}
