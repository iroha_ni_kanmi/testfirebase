﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class StatusDataStore : MonoBehaviour {

	// シングルトン
    private static StatusDataStore _instance = null;
	// シングルトン
    public static StatusDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<StatusDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class StatusData {
        // ゲーム開始時か否か
        [SerializeField]
        public bool gameStartStatus;

        // ローディング画面画像
        [SerializeField]
        public string loadingPic;

        // ロード状況
        [SerializeField]
        public bool loadingStatus;

        // 移動処理判定
        [SerializeField]
        public bool movableStatus;

        //シーンのモード
        [SerializeField]
        public string viewMode;
        
        //シナリオラベルのモード
        [SerializeField]
        public string scenarioLabel;

        //チュートリアルのモード
        [SerializeField]
        public string tutorialId;

        //ネットワークの状態
        [SerializeField]
        public string networkStatus;

        //ネットワークの状態
        [SerializeField]
        public bool getNews;

        public void ResetData() {
        	gameStartStatus = true;
        	loadingPic = "loadingPic01";
        	loadingStatus = false;
            movableStatus = true;
            viewMode = "TitleView";
            scenarioLabel = "";
            tutorialId = "1";
            networkStatus = "ON";
            getNews = false;
        }
    }
	
    // 保存データ
    public StatusData data = new StatusData();

    // 保存キー
    private const string saveKey = "SaveStatusData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<StatusData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}
}
