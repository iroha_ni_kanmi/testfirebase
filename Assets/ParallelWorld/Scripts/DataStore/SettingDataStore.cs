﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SettingDataStore : MonoBehaviour {

	// シングルトン
    private static SettingDataStore _instance = null;
	// シングルトン
    public static SettingDataStore Instance {
		get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<SettingDataStore>();
                _instance.Load();
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class SettingData {
        // BGM音量
        [SerializeField]
        public float bgmVolume;
        // SE音量
        [SerializeField]
        public float seVolume;

        public void ResetData() {
            bgmVolume = 0;
            seVolume = 0;
        }
    }
	
    // 保存データ
    public SettingData data = new SettingData();

    // 保存キー
    private const string saveKey = "SaveSettingData";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<SettingData> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}
}
