﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models;

public class StoryViewModel {

    private string exampleStoryAsJson = "{\"title\":\"門をなおそう\",\"description\":\"ムスビメである門に\\nヒビが入ってしまい、\\n修正しないと危なくて通れない。\\n\\nテツダイグマをなおして、\\n門を修繕してもらおう\"}";

    private ArrayList stories;

    public ArrayList GetStories() {
        if (stories == null) {
            stories = loadStories();
        }

        return stories;
    }

    public Story loadStoryData() {
        Story story = JsonUtility.FromJson<Story>( exampleStoryAsJson );
        return story;
    }

    public ArrayList loadStories() {
        return new ArrayList() {
            new Story( "門をなおす", "ムスビメである門に\nヒビが入ってしまい、\n修正しないと危なくて通れない。\n\nテツダイグマをなおして、\n門を修繕してもらおう", true ),
            new Story( "郵便屋さんの依頼", "ユウビンバトをなおす", false ),
            new Story( "運び屋さんの依頼", "ウンソウウシをなおす", false ),
            new Story( "工事屋さんの依頼", "テツダイクマをなおす", false ),
            new Story( "郵便屋さんの依頼", "ユウビンバトをなおす", false ),
            new Story( "運び屋さんの依頼", "ウンソウウシをなおす", false ),
            new Story( "工事屋さんの依頼", "テツダイクマをなおす", false )
        };
    }

}
