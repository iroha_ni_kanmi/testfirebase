﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class GetFilePathEditor: EditorWindow
{
 // 右クリックMenuに追加.
 [MenuItem("Assets/GetFilePath")]
 private static void GetFilePath()
 {
   GetSelectFile();
 }

 /// <summary>
 /// 選択中のファイルパス取得.
 /// </summary>
 private static void GetSelectFile()
 {
   // ファイルが選択されている時.
   if(Selection.assetGUIDs != null && Selection.assetGUIDs.Length > 0)
   {
     // ファイルリスト作成.
     List<string> fileList = new List<string>();
     
     // 選択されているパスの取得.
     foreach(var files in Selection.assetGUIDs)
     {
       var path = AssetDatabase.GUIDToAssetPath(files);
       fileList.Add(path);
     }

     // 出力.
　   foreach(var list in fileList)
     {
       Debug.Log(list);
     }
   }
 }
}