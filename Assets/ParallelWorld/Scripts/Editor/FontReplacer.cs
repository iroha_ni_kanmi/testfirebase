﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEditor.SceneManagement;

public class FontReplacer : EditorWindow 
{
	public Font font;
	public float lineSpacing = 0.7f;

	[MenuItem("GamePJ/Replace All Fonts")]
	public static void ShowWindow()
	{
		var window = (FontReplacer)EditorWindow.GetWindow(typeof(FontReplacer));
		window.font = AssetDatabase.LoadAssetAtPath<Font>("Assets/Font/FOT-DNPShueiGoGinStd-L.otf");
		window.Show();
	}

	void OnGUI()
	{
		font = EditorGUILayout.ObjectField("Font", font, typeof(Font), true) as Font;
		lineSpacing = EditorGUILayout.FloatField ("Line Spacing",lineSpacing);
		if (font != null) {
			if (GUILayout.Button("Replace All Fonts"))
			{
				Debug.Log("you are trying to replace all fonts to new one");

				var textComponents = Resources.FindObjectsOfTypeAll(typeof(Text)) as Text[];
				foreach (var component in textComponents)
				{
					component.font = font;
					component.lineSpacing = lineSpacing;
				}
				// ※追記 : シーンに変更があることをUnity側に通知しないと、シーンを切り替えたときに変更が破棄されてしまうので、↓が必要
				EditorSceneManager.MarkAllScenesDirty();
			}
		}
	}
}