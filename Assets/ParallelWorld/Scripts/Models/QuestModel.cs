﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
 


[Serializable]
[CreateAssetMenu(fileName = "QuestModel", menuName="CreateQuest")]
public class QuestModel : ScriptableObject {

	[System.Serializable]
	public class QuestItemTable : Serialize.TableBase<string, int, QuestItemPair>{}
	[System.Serializable]
	public class QuestItemPair : Serialize.KeyAndValue<string, int>{
		public QuestItemPair (string id, int quantity) : base (id, quantity) {}
		}

	[System.Serializable]
	public class QuestDollPointTable : Serialize.TableBase<Sprite, int, QuestDollPointPair>{}
	[System.Serializable]
	public class QuestDollPointPair : Serialize.KeyAndValue<Sprite, int>{
		public QuestDollPointPair (Sprite sprite, int point) : base (sprite, point) {}
		}

  
  	public enum Genre {
		Main,
		Sub
	}

  	public enum Purpose {
		getItem,
		goPlace,
		meetCharactor,
		createDoll
	}

	//　クエストのid
	[SerializeField]
	public string id;
	//　クエストの名前
	[SerializeField]
	public string questName;
	//　クエストの名前
	[SerializeField]
	public string questSubName;
	//　クエストのアイコン
	[SerializeField]
	public Sprite questIcon;
	//　クエストのジャンル
	[SerializeField]
	public Genre genre;
	//　クエストの目的
	[SerializeField]
	public Purpose purpose;

	//　[porpose = getItem]
	// 目的のアイテム辞書
	[SerializeField]
	public QuestItemTable questItemTable;

	//　[porpose = goPlace]
	// 目的地
	[SerializeField]
	public GameObject placeObj;

	//　[porpose = meetCharactor]
	// 目的のキャラクター
	[SerializeField]
	public List<GameObject> listCharactor;

	//　[porpose = meetCharactor]
	// なおすアイテム
	[SerializeField]
	public Sprite dollImage;

	//　[porpose = createDoll]
	// なおすためのアイテム
	[SerializeField]
	public QuestDollPointTable questDollPoint;

	//　クエストの情報
	[SerializeField]
	[Multiline(10)] public string description;
	
	//　クエスト達成のヒント
	[SerializeField]
	[Multiline(10)] public string howToClear;

	//　クエストの情報
	[SerializeField]
	private int thankPoint;

	//　クエストの情報
	[SerializeField]
	private bool completeFlg;

	public string GetQuestid() {
		return id;
	}
 
	public Sprite GetIcon() {
		return questIcon;
	}
 
	public string GetQuestName() {
		return questName;
	}
 
	public string GetQuestSubName() {
		return questSubName;
	}

	public Genre GetQuestGenre() {
		return genre;
	}

	public Purpose GetQuestPurpose() {
		return purpose;
	}

	public string GetDescription() {
		return description;
	}

	public string GetHowToClear() {
		return howToClear;
	}

	public QuestItemTable GetQuestItemTable() {
		return questItemTable;
	}

	public Sprite GetDollImage() {
		return dollImage;
	}

	public GameObject GetPlaceObj() {
		return placeObj;
	}

	public List<GameObject> GetListCharactor() {
		return listCharactor;
	}

	public List<string> GetListCharactorName() {
		List<string> listCharactorName = new List<string>();
		foreach (var item in listCharactor)
		{
			listCharactorName.Add(item.name);
		}
		return listCharactorName;
	}

	public QuestDollPointTable GetQuestDollPoint() {
		return questDollPoint;
	}

	public int GetQuestThankPoint() {
		return thankPoint;
	}

	public bool GetCompleteFlg() {
		return completeFlg;
	}
}