﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Info {

    public bool active;

    public string title;

    public string digest;

    public string message;

    public string image_url;
    
    public string date;

    public Info() {
        active = false;
        title = "";
        digest = "";
        message = "";
        image_url = "";
        date = "";
    }
	
}
