﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;


namespace ParallelWorld.DBModel {

    /// DBデータ
    [Serializable]
    public class PlayerData {

        /// ユーザーID
        public string id;

        /// ユーザー名
        public string name;
        
        /// 生年月日
        public string birthday;

        /// 最後の操作エリアID
        public string last_area;

        /// 最後の現在地座標
        public double[] last_position;

        /// 着せ替え設定情報
        public Appearance appearance;

        /// 所持アイテム
        public ItemInventory[] items;

        /// クエスト状態配列
        public QuestInventory[] quests;

        /// meetCharacterクエスト状態配列
        public meetCharacterInventory[] meetCharacter;

        /// フレンド
        public string[] friends;

        /// 広告非表示
        public string no_ad_id;

        /// 設定オブジェクト
        public Settings settings;

        /// DB構造バージョン
        public string version;

        /// 最終更新日時
        public string last_updated;

        ///サンクポイント
        public int thankPoint;

        /// 主人公ランク
        public int playerRank;

        /// ストーリーNo
        public int storyNo;

        /// トロフィーNo
        public string[] trophy;

        /// キャラクター親密度
        public ThanksOfCharacter[] thanksOfCharacter;

        /// 作ったぬいぐるみリスト
        public createdDoll[] createdDollList;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PlayerData() {
            id = "";
            name = "";
            birthday = "20181205";
            last_area = "0000";
            last_position = new double[] { };
            appearance = new Appearance();
            items = new ItemInventory[] { };
            quests = new QuestInventory[] { };
            meetCharacter = new meetCharacterInventory[] { };
            friends = new string[] { };
            no_ad_id = "";
            settings = new Settings();
            version = "0.3.0";
            last_updated = DateTime.Now.ToString();
            thankPoint = 0;
            playerRank = 1;
            storyNo = 1;
            trophy = new string[] { };
            thanksOfCharacter = new ThanksOfCharacter[] { };
            createdDollList = new createdDoll[] { };
        }
        
        public void ResetData() {
            id = "";
            name = "";
            birthday = "20181205";
            last_area = "0000";
            last_position = new double[] { };
            appearance = new Appearance();
            items = new ItemInventory[] { };
            quests = new QuestInventory[] { };
            meetCharacter = new meetCharacterInventory[] { };
            friends = new string[] { };
            no_ad_id = "";
            settings = new Settings();
            version = "0.3.0";
            last_updated = DateTime.Now.ToString();
            thankPoint = 0;
            playerRank = 1;
            storyNo = 1;
            trophy = new string[] { };
            thanksOfCharacter = new ThanksOfCharacter[] { };
            createdDollList = new createdDoll[] { };
        }

        public string ExportPlayerDataAsString(){
            //ローカルデバイスから、AWSへデータベースを更新するために文字列出力する関数
            string exportData = "{";

            //idの追加
            exportData += ("\"id\":\""+ id + "\"");

            //nameの追加
            exportData += (",\"name\":\"" + name + "\"");
            
            //birthdayの追加
            exportData += (",\"birthday\":\"" + birthday + "\"");
            
            //last_areaの追加
            exportData += (",\"last_area\":\"" + last_area + "\"");

            //last_positionの追加
            for(int i =0; i < last_position.Length; i++){
                if(i == 0){
                    exportData += (",\"last_position\":[" + last_position[i].ToString());
                }else{
                    exportData += ("," + last_position[i].ToString());
                }
            }
            exportData += "]";

            //appearanceの追加
            exportData += ",\"appearance\":{}";

            //itemsの追加
            for(int i =0; i < items.Length; i++){
                if(i == 0){
                    exportData += (",\"items\":[{\"id\":\"" + items[i].id + "\"");
                    exportData += (",\"quantity\":\"" + items[i].quantity.ToString() + "\"");
                    exportData += (",\"check\":" + items[i].check.ToString().ToLower() + "}");
                }else{
                    exportData += (",{\"id\":\"" + items[i].id + "\"");
                    exportData += (",\"quantity\":\"" + items[i].quantity.ToString() + "\"");
                    exportData += (",\"check\":" + items[i].check.ToString().ToLower() + "}");
                }
            }
            exportData += "]";

            //questsの追加
            for(int i =0; i < quests.Length; i++){
                if(i == 0){
                    exportData += (",\"quests\":[{\"id\":\"" + quests[i].id + "\"");
                    exportData += (",\"progress\":" + quests[i].progress.ToString().ToLower());
                    exportData += (",\"check\":" + quests[i].check.ToString().ToLower());
                    exportData += (",\"read\":" + quests[i].read.ToString().ToLower() + "}");
                }else{
                    exportData += (",{\"id\":\"" + quests[i].id + "\"");
                    exportData += (",\"progress\":" + quests[i].progress.ToString().ToLower());
                    exportData += (",\"check\":" + quests[i].check.ToString().ToLower());
                    exportData += (",\"read\":" + quests[i].read.ToString().ToLower() + "}");
                }
            }
            exportData += "]";

            //meetCharacterの追加
            for(int i =0; i < meetCharacter.Length; i++){
                if(i == 0){
                    exportData += (",\"meetCharacter\":[{\"nameChara\":\"" + meetCharacter[i].nameChara + "\"");
                    for(int j =0; j < meetCharacter[i].id.Length; j++){
                        exportData += ",\"id\":";
                        
                        if(j == 0){
                            exportData += ("[\"" + meetCharacter[i].id[j] + "\"");
                        }else{
                            exportData += (",\"" + meetCharacter[i].id[j] + "\"");
                        }
                    }
                    exportData += "]}";

                }else{
                    exportData += (",{\"nameChara\":\"" + meetCharacter[i].nameChara + "\"");
                    for(int j =0; j < meetCharacter[i].id.Length; j++){
                        exportData += ",\"id\":";
                        
                        if(j == 0){
                            exportData += ("[\"" + meetCharacter[i].id[j] + "\"");
                        }else{
                            exportData += (",\"" + meetCharacter[i].id[j] + "\"");
                        }
                    }
                    exportData += "]}";
                }
            }
            exportData += "]";

            //friendsの追加
            exportData += ",\"friends\":[]";

            //no_ad_idの追加
            exportData += ",\"no_ad_id\":\"none\"";

            //settingsの追加
            exportData += (",\"settings\":{\"se_volume\":" + settings.se_volume + ",\"bgm_volume\":" + settings.bgm_volume + "}");

            //versionの追加
            exportData += ",\"version\":" + version;

            //last_updatedの追加
            exportData += ",\"last_updated\":\"" + last_updated.ToString() + "\"";

            //thankPointの追加
            exportData += ",\"thankPoint\":" + thankPoint;

            //playerRankの追加
            exportData += ",\"playerRank\":" + playerRank;

            //playerRankの追加
            exportData += ",\"storyNo\":" + storyNo.ToString();

            //trophyの追加
            for(int i =0; i < trophy.Length; i++){
                if(i == 0){
                    exportData += (",\"trophy\":[\"" + trophy[i] + "\"");
                }else{
                    exportData += (",\"" + trophy[i] + "\"");
                }
            }
            exportData += "]";

            //thanksOfCharacterの追加
            for(int i =0; i < thanksOfCharacter.Length; i++){
                if(i == 0){
                    exportData += (",\"thanksOfCharacter\":[{\"nameChara\":\"" + thanksOfCharacter[i].nameChara + "\"");
                    exportData += (",\"thankPoint\":" + thanksOfCharacter[i].thankPoint.ToString() + "}");
                }else{
                    exportData += (",{\"nameChara\":\"" + thanksOfCharacter[i].nameChara + "\"");
                    exportData += (",\"thankPoint\":" + thanksOfCharacter[i].thankPoint.ToString() + "}");
                }
            }
            exportData += "]";

            //createdDollListの追加
            for(int i =0; i < createdDollList.Length; i++){
                if(i == 0){
                    exportData += (",\"createdDollList\":[{\"dollName\":\"" + createdDollList[i].dollName + "\"");
                    exportData += (",\"check\":" + createdDollList[i].check.ToString().ToLower() + "}");
                }else{
                    exportData += (",{\"dollName\":\"" + createdDollList[i].dollName + "\"");
                    exportData += (",\"check\":" + createdDollList[i].check.ToString().ToLower() + "}");
                }
            }
            exportData += "]";



            exportData += "}";
            return exportData;
        }

        public Dictionary<string, bool> GetDicQuestCompleteFlg(){
            Dictionary<string, bool> dicQuestCompleteFlg = new Dictionary<string, bool>();
            
            foreach (QuestInventory quest in quests){
                dicQuestCompleteFlg[quest.id] = quest.progress;
                }
            
            return dicQuestCompleteFlg;
        }

        public Dictionary<string, int> GetDicItemQuantity(){
            Dictionary<string, int> dicItemQuantity = new Dictionary<string, int>();
            
            foreach (ItemInventory item in items){
                dicItemQuantity[item.id] = item.quantity;
                }
            
            return dicItemQuantity;
        }

        public Dictionary<string, string[]> GetDicMeetCharacter(){
            //キャラクター名：meetCharacterクエストIDから構成される辞書を返す
            Dictionary<string, string[]> dicMeetCharacter = new Dictionary<string, string[]>();

            foreach (meetCharacterInventory metchara in meetCharacter){
                dicMeetCharacter[metchara.nameChara] = metchara.id;
            }
            
            return dicMeetCharacter;
        }

        public Dictionary<string, int> GetDicMeetCharacterSortNumber(){
            //キャラクター名：meetCharacterクエストIDから構成される辞書を返す
            Dictionary<string, int> dicMeetCharacterSortNumber = new Dictionary<string, int>();
            int i = 0;
            foreach (meetCharacterInventory metchara in meetCharacter){
                dicMeetCharacterSortNumber[metchara.nameChara] = i;
                i += 1;
            }
            
            return dicMeetCharacterSortNumber;
        }

        public Dictionary<string, string[]> GetDicMeetCharacterIds(){
            //id:必要キャラクターから構成される辞書を返す
            Dictionary<string, string[]> dicMeetCharacterIds = new Dictionary<string, string[]>();
            foreach (meetCharacterInventory metchara in meetCharacter){
                for(int i = 0; i < metchara.id.Length; i ++){
                    if(dicMeetCharacterIds.ContainsKey(metchara.id[i])){
                        //すでに登録されていれば、配列をマージし、辞書を更新
                        var src = dicMeetCharacterIds[metchara.id[i]];
                        Array.Resize(ref src, src.Length + 1);
                        src[src.Length - 1] = metchara.nameChara;                        

                    }else{
                        //辞書にまだ登録されていなければ、idをKeyに
                        //新規で辞書登録
                        dicMeetCharacterIds[metchara.id[i]] = new string[1];
                        dicMeetCharacterIds[metchara.id[i]][0] = metchara.nameChara;
                    }
                }
            }
            return dicMeetCharacterIds;
        }

        public bool chkPlayerCheckedQuest(){
            //クエストにて、未だプレイヤーが未読のものがあるかどうかをチェック
            bool flgCheck = true;
            foreach (QuestInventory quest in quests){
                if(!quest.check){
                    flgCheck = false;
                    return flgCheck;
                }
            }
            return flgCheck;
        }

        public bool chkPlayerCheckedItem(){
            //クエストにて、未だプレイヤーが未読のものがあるかどうかをチェック
            bool flgCheck = true;
            foreach (ItemInventory item in items){
                if(!item.check){
                    flgCheck = false;
                    return flgCheck;
                }
            }
            return flgCheck;
        }

        public bool chkPlayerCheckedLetter(){
            //クエストにて、未だプレイヤーが未読のものがあるかどうかをチェック
            bool flgRead = true;
            foreach (QuestInventory quest in quests){
                if(!quest.read){
                    flgRead = false;
                    return flgRead;
                }
            }
            return flgRead;
        }

    }

    /// 着せ替えobj
    [Serializable]
    public class Appearance {

    }

    /// 所持アイテムarray
    [Serializable]
    public class ItemInventory {
        /// アイテムID
        public string id;

        /// 所持アイテム数
        public int quantity;

        /// アイテム確認状況
        public bool check;

        /// コンストラクタ
        public ItemInventory()
        {
            id = "";
            quantity = 0;
            check = false;
        }
    }

    /// 設定オブジェクト
    [Serializable]
    public class Settings {

        /// SEボリューム
        public float se_volume;

        /// BGMボリューム
        public float bgm_volume;

    }

    /// クエストarray
    [Serializable]
    public class QuestInventory
    {
        /// クエストID
        public string id;

        /// クエスト達成度
        public bool progress;

        /// クエスト確認状況
        public bool check;

        /// クエスト確認状況
        public bool read;

        /// コンストラクタ
        public QuestInventory()
        {
            id = "";
            progress = false;
            check = false;
            read = false;
        }
    }

    /// meetCharacter専用DB
    [Serializable]
    public class meetCharacterInventory
    {   
        //キャラの名前
        public string nameChara;

        //キャラに紐づくクエストid
        public string[] id;

    }

    /// キャラクター親密度
    [Serializable]
    public class ThanksOfCharacter {

        /// SEボリューム
        public string nameChara;

        /// BGMボリューム
        public int thankPoint;

    }

    /// キャラクター親密度
    [Serializable]
    public class createdDoll {

        /// SEボリューム
        public string dollName;

        /// BGMボリューム
        public bool check;

    }
}
