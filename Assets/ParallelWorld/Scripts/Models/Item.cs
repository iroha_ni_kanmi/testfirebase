﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
 
[Serializable]
[CreateAssetMenu(fileName = "Item", menuName="CreateItem")]
public class Item : ScriptableObject {
  
  	public enum Genre {
		Materi,
		Clothing,
		KeyItem,
		etc
	}
  
  	public enum SubGenre {
		Thread,
		Cloth,
		Cotton,
		Needle,
		Etc
	}

	//　アイテムのid
	[SerializeField]
	private string id;
	//　アイテムの名前
	[SerializeField]
	private string itemName;
	//　アイテムのアイコン
	[SerializeField]
	private Sprite icon;
	//　アイテムのジャンル（アイテム画面用）
	[SerializeField]
	private Genre genre;
	//　アイテムのサブジャンル（なおし画面用）
	[SerializeField]
	private SubGenre subGenre;
	//　アイテムの情報
	[SerializeField]
	[Multiline(10)] private string description;
	//　アイテムの取得方法
	[SerializeField]
	[Multiline(10)] private string howToGet;
	//　アイテムの取得方法
	[SerializeField]
	private float sortNumber;

	public string GetItemid() {
		return id;
	}
 
	public Sprite GetIcon() {
		return icon;
	}
 
	public string GetItemName() {
		return itemName;
	}
 
	public Genre GetItemGenre() {
		return genre;
	}

	public SubGenre GetItemSubGenre() {
		return subGenre;
	}

	public string GetItemDescription() {
		return description;
	}

	public string GetItemHowToGet() {
		return howToGet;
	}

	public float GetItemSortNumber() {
		return sortNumber;
	}
}
