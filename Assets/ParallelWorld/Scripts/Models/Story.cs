﻿namespace Models {
    
    public class Story {

        public string title;

        public string description;

        public bool active;

        public Story(string title, string description, bool active) {
            this.title = title;
            this.description = description;
            this.active = active;
        }

    }

}
