﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoShared;

public class WeatherRenderer : MonoBehaviour
{
    public LocationManager GoMapLocationManager;
    public WeatherLoader Loader;
    public Text WeatherText;
    public Text WindForceText;
    public uint ThresholdSeconds = 60 * 60; // 1時間更新しない

    private DateTime lastUpdatedDateTime;

    void Start()
    {
        //GoMapLocationManager.onLocationChanged += LoadWeather;
        LoadWeather(GoMapLocationManager.currentLocation);
    }

    void LoadWeather(Coordinates location)
    {
        if (!CanUpdateWeather()) return;
        StartCoroutine(Loader.Load(location.latitude, location.longitude, weatherEntity =>
        {
            lastUpdatedDateTime = DateTime.UtcNow;
            Render(weatherEntity);
        }));
    }

    void Render(WeatherEntity weatherEntity)
    {
        WeatherText.text = string.Format("weather:{0}", weatherEntity.weather[0].main);
        WindForceText.text = string.Format("wind: {0}m", weatherEntity.wind.speed);
    }

    // ざっくり時間で間引く
    bool CanUpdateWeather()
    {
        return lastUpdatedDateTime == null || ((DateTime.UtcNow - lastUpdatedDateTime).TotalSeconds > ThresholdSeconds);
    }
}