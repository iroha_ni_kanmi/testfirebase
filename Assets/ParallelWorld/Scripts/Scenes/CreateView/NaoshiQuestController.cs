﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class NaoshiQuestController : MonoBehaviour
{
    //データセット用関数
    public MainQuestList mainQuestList;
    public SubQuestList subQuestList;
    public storyList storyList;
    public naoshiQuestList naoshiQuestList;
    public naoshiDialogList naoshiDialogList;
    public materiList materiList;

    //内部判定用関数
    private int phaseNumber;
    private int totalPhaseNumber;
    private float totalPoint;

    //アイテム制限周りのList
    private List<string> listOnlyItems = new List<string>();
    private List<string> listExceptItems = new List<string>();
    //Phaseごとにしか適用できないアイテムのリスト
    private List<string> listPhaseItem = new List<string>();


    //判定用辞書宣言
    //private Dictionary<string, MainQuestList.QuestInfo> dicMainQuestInfo = new Dictionary<string, MainQuestList.QuestInfo>();
    //private Dictionary<string, SubQuestList.QuestInfo> dicSubQuestInfo = new Dictionary<string, SubQuestList.QuestInfo>();
    //private Dictionary<string, storyList.storyInfo> dicStoryListInfo = new Dictionary<string, storyList.storyInfo>();    

    private Dictionary<string, naoshiQuestList.naoshiInfo> dicNaoshiQuestInfo = new Dictionary<string, naoshiQuestList.naoshiInfo>();    
    private Dictionary<string, naoshiDialogList.naoshiDialogInfo> dicNaoshiDialogInfo = new Dictionary<string, naoshiDialogList.naoshiDialogInfo>();    
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_id = new Dictionary<string, materiList.materiInfo>();    
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_itemName = new Dictionary<string, materiList.materiInfo>(); 
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_fileName = new Dictionary<string, materiList.materiInfo>(); 

    //判定用クエストデータセット
    private Dictionary<int, naoshiQuestDataSet> dicPhaseQuest = new Dictionary<int, naoshiQuestDataSet>(); 


    void Awake(){
        //データセット
        naoshiQuestList = QuestDataStore.Instance.data.naoshiQuestList;
        naoshiDialogList = QuestDataStore.Instance.data.naoshiDialogList;
        materiList = ItemDataStore.Instance.data.materiList;

        dicNaoshiQuestInfo = naoshiQuestList.getNaoshiInfo();
        dicNaoshiDialogInfo = naoshiDialogList.getNaoshiDialogInfo();
        dicMateriListInfo_id = materiList.getMateriInfo_id();
        dicMateriListInfo_itemName = materiList.getMateriInfo_itemName();
        dicMateriListInfo_fileName = materiList.getMateriInfo_fileName();

        dicPhaseQuest = new Dictionary<int, naoshiQuestDataSet>(); 

    }


    // Start is called before the first frame update
    void Start()
    {

        //クエストidをセット
        setNaoshiQuest(QuestDataStore.Instance.data.questId);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void setNaoshiQuest(string id){

        if(naoshiQuestList.getIds().Contains(id)){

            //データセット
            //===========================Phase
            //なおしクエストデータを取得
            string[] arrItemGenre = dicNaoshiQuestInfo[id].numberingItemGenre.Split(',');
            string[] arrPoint = dicNaoshiQuestInfo[id].numberingPoint.Split(',');
            string[] arrItemName = dicNaoshiQuestInfo[id].numberingItemName.Split(',');
            totalPhaseNumber = arrItemGenre.Length;

            float totalPhasePoints = 0;

            //Excelのアイテムの順序・上限ポイント・特定アイテムを格納
            for(int i = 0; i < arrItemGenre.Length ;i++){
                naoshiQuestDataSet naoshiQuestData = new naoshiQuestDataSet();
                naoshiQuestData.itemGenre = convertStringToItemGenre(arrItemGenre[i]);
                naoshiQuestData.point = int.Parse(arrPoint[i]);
                naoshiQuestData.itemName = arrItemName[i];

                totalPhasePoints += float.Parse(arrPoint[i]);

                Debug.Log("hit");
                Debug.Log(naoshiQuestData.itemGenre);
                Debug.Log(naoshiQuestData.point);
                Debug.Log(naoshiQuestData.itemName);

                dicPhaseQuest[i] = naoshiQuestData;
                Debug.Log(i);
            }

            NaoshiStatus.Instance.data.totalPhasePoints = totalPhasePoints;

            Debug.Log(NaoshiStatus.Instance.data.totalPhasePoints);

            phaseNumber = 0;

            //===========================listOnlyItems
            //使用アイテムに制限があるか否か
            if(dicNaoshiQuestInfo[id].onlyItems != "-"){

                //存在した場合は、使用可能なアイテムのリストを作成
                listOnlyItems.AddRange(dicNaoshiQuestInfo[id].onlyItems.Split(','));

            }else{
                listOnlyItems = new List<string>();
            }

            //===========================listExceptItems
            //使用アイテムに除外するものがあるか否か
            if(dicNaoshiQuestInfo[id].exceptItems != "-"){

                //存在した場合は、使用不可能なアイテムのリストを作成
                listExceptItems.AddRange(dicNaoshiQuestInfo[id].exceptItems.Split(','));


            }else{
                listExceptItems = new List<string>();
            }

        }
    }

    public float setNaoshiItem(string itemName){

            //適切なアイテムがドロップされたかどうか
            //アイテムの制限周りの判定
            if(listOnlyItems.Any()){
                //特定アイテムでなければ、pass
                if(!listOnlyItems.Contains(itemName)){
                    Debug.Log(itemName);
                    Debug.Log("not特定アイテム0P");
                    NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.bad;
                    return 0;
                }
            }
            //Debug.Log("Pass1");

            if(listExceptItems.Any()){
                //特定アイテムを使用していれば、pass
                if(listExceptItems.Contains(itemName)){
                    NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.bad;
                    Debug.Log("禁止アイテム0P");
                    return 0;
                }
            }
            //Debug.Log("Pass2");

            //Phaseによる制限周りの判定

            foreach(var item in dicPhaseQuest.Keys){
                Debug.Log("dictionary");
                Debug.Log(item);
            }

            //辞書に登録されているkey、かつポイント加算対象となるジャンルかどうか
            if(dicPhaseQuest.ContainsKey(phaseNumber) && dicPhaseQuest[phaseNumber].itemGenre == convertStringToItemGenre(dicMateriListInfo_itemName[itemName].genre)){
                //Debug.Log("Pass3");

                //特定アイテムがあるかどうか
                if(dicPhaseQuest[phaseNumber].itemName != "-"){
                    
                    if(itemName == dicPhaseQuest[phaseNumber].itemName){
                        //特定アイテムであった場合、それに応じたポイントを加算する
                        Debug.Log("特定アイテムP");
                        //喜び(大)
                        NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.great;
                        return getParameterItem(itemName);

                    }else{
                        //特定アイテム出なかった場合、特定アイテムのポイントを100とした時の
                        //アイテムのポイントを返す
                        Debug.Log("not特定but同属性アイテムP");
                        //喜び(小)
                        NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.good;
                        return getParameterItem(dicPhaseQuest[phaseNumber].itemName, itemName);

                    }

                }else{
                    //Debug.Log("Pass4");
                    //喜び(大)
                    Debug.Log("通常マッチアイテムP");
                    NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.great;
                    return getParameterItem(itemName);
                }


            }else{
                //はてな（それなんで適用するの？）
                NaoshiStatus.Instance.data.matching = NaoshiStatus.naoshiStatus.typeMatching.question;
                Debug.Log("フェーズ外");
                Debug.Log(phaseNumber);
                return 0;
            }
    }

    public float getParameterItem(string itemName){
        //アイテムDBから、アイテムのポイントを引っ張ってくる
        if(dicMateriListInfo_itemName.ContainsKey(itemName)){
            //Debug.Log(dicMateriListInfo_itemName[itemName].itemPoint);
            return dicMateriListInfo_itemName[itemName].itemPoint;
        }else{
            Debug.Log("登録されていないアイテムです");
            return 0;
        }
    }

    public float getParameterItem(string correctItemName, string itemName){
        //アイテムDBから、アイテムのポイントを引っ張ってくる
        //同じ属性だが、正解のアイテムでない場合、
        //正解のアイテム:postしたアイテムを100:xとした比率のポイントを返す
        if(dicMateriListInfo_itemName.ContainsKey(itemName)){
            float itemPoint = dicMateriListInfo_itemName[itemName].itemPoint;
            float correctItemPoint = dicMateriListInfo_itemName[correctItemName].itemPoint;
            float returnPoint = (itemPoint/correctItemPoint) * itemPoint; 
            //Debug.Log(dicMateriListInfo_itemName[itemName].itemPoint);
            //Debug.Log(dicMateriListInfo_itemName[correctItemName].itemPoint);
            //Debug.Log(returnPoint);
            return returnPoint;
        }else{
            Debug.Log("登録されていないアイテムです");
            return 0;
        }
    }

    public float OnDropNaoshiItem(string itemName){
        
        //itemNameに入ってくるのは、ファイル名であるため、
        //一度変換する必要がある
        if(dicMateriListInfo_fileName.ContainsKey(itemName)){
            Debug.Log("hit");
            itemName = dicMateriListInfo_fileName[itemName].itemName;
        }else{
            Debug.Log("not hit");
        }
        Debug.Log(itemName);

        totalPoint += setNaoshiItem(itemName);

        if(dicPhaseQuest.ContainsKey(phaseNumber) && totalPoint >= dicPhaseQuest[phaseNumber].point){
            //フェーズの更新
            phaseNumber += 1;
            totalPoint = 0;

        }
        if(phaseNumber >= totalPhaseNumber){
            //フェーズが完了したら、到達のメッセージ
            //終了の処理
            Debug.Log("Finished!");
        }

        return setNaoshiItem(itemName);
    }


    public void OnClickTestButton(string itemName){
        totalPoint += setNaoshiItem(itemName);
        if(dicPhaseQuest.ContainsKey(phaseNumber) && totalPoint >= dicPhaseQuest[phaseNumber].point){
            //フェーズの更新
            phaseNumber += 1;
            totalPoint = 0;

        }
        if(phaseNumber >= totalPhaseNumber){
            //フェーズが完了したら、到達のメッセージ
            //終了の処理
            NaoshiStatus.Instance.data.flgClear = true;
            Debug.Log("Finished!");
        }
        //Debug.Log(phaseNumber);
        //Debug.Log(totalPhaseNumber);
        //Debug.Log(totalPoint);
    }

	public class naoshiQuestDataSet{

        //判定に使用するアイテムに関するデータセット

        [SerializeField]
        public typeItem itemGenre = new typeItem();

        [SerializeField]
        public int point = 0;

        [SerializeField]
        public string itemName = "";


        public void ResetData() {
            itemGenre = new typeItem();
            point = 0;
            itemName = "";
        }

        public enum typeItem{
            hari,
    		ito,
	    	nuno,
	    	wata,
            other,
        }
	}  

    //アイテムの種類を定義
    public naoshiQuestDataSet.typeItem convertStringToItemGenre(string genre){
        switch(genre){
            case "ハリ":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "Hari":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "hari":
            return naoshiQuestDataSet.typeItem.hari;
            break;

            case "イト":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "Ito":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "ito":
            return naoshiQuestDataSet.typeItem.ito;
            break;

            case "ヌノ":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "Nuno":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "nuno":
            return naoshiQuestDataSet.typeItem.nuno;
            break;

            case "ワタ":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            case "Wata":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            case "wata":
            return naoshiQuestDataSet.typeItem.wata;
            break;

            default:
            return naoshiQuestDataSet.typeItem.other;
            break;
        }

    }


}
