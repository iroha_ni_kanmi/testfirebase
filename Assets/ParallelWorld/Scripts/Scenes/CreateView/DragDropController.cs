﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class DragDropController : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    //ぬいぐるみの画像セット
    public Image nuigurumiImage;

    //依頼人の画像セット
    public Image ClientImage;

    //クエストのゲージ
    public Slider QuestGauge;

    //処理の呼び出し用
    public NaoshiQuestController naoshiQuestController;

    //内部処理関数=========================
    //ぬいぐるみの画像パス
    private string dollImagePath;
    private Sprite step01,step02,step03,step04;

    //依頼人の画像パス
    private string ClientImagePath;
    private Sprite faceGood,faceUsual,faceBad,faceQuestion;

    //正解かどうかの変数定義
    //private string DollImageName;
    private string DropItemName;

    //ぬいぐるみの最新の状態画像
    //TODO:これ必要か？
    private Sprite nowSprite;

    //DataStoreから落としてくるDB辞書
    private MainQuestList mainQuestList;
    private naoshiQuestList naoshiQuestList;

    //===================================
    
    void Start()
    {
        //データストアから、データセット
        mainQuestList = QuestDataStore.Instance.data.mainQuestList;
        naoshiQuestList = QuestDataStore.Instance.data.naoshiQuestList;

        dollImagePath = naoshiQuestList.getNaoshiInfo()[TappedObjectInfoDataStore.Instance.data.questObjId].dollImage;
        step01 = Resources.Load(dollImagePath+"_step01", typeof(Sprite)) as Sprite;
        step02 = Resources.Load(dollImagePath+"_step02", typeof(Sprite)) as Sprite;
        step03 = Resources.Load(dollImagePath+"_step03", typeof(Sprite)) as Sprite;
        step04 = Resources.Load(dollImagePath, typeof(Sprite)) as Sprite;

        //依頼人の画像を付与
        ClientImagePath = mainQuestList.getQuestInfo()[TappedObjectInfoDataStore.Instance.data.questObjId].questIconFilePath;
        faceGood = Resources.Load(ClientImagePath+"_good", typeof(Sprite)) as Sprite;
        faceBad = Resources.Load(ClientImagePath+"_bad", typeof(Sprite)) as Sprite;
        faceQuestion = Resources.Load(ClientImagePath+"_question", typeof(Sprite)) as Sprite;
        faceUsual = Resources.Load(ClientImagePath, typeof(Sprite)) as Sprite;

        //TODO:クエストの開始時のポイントを設定する

        //ぬいぐるみの初期画像を付与
        nuigurumiImage.sprite = step01;
        nowSprite = nuigurumiImage.sprite;

        //依頼人の初期画像を付与
        ClientImage.sprite = faceUsual;

    }

    void Update(){

        //クエストのゲージに応じて、ぬいぐるみの状態を変遷させる
        //TODO:UniRxでやりたい。Updateでやりたくない
        if(QuestGauge.value < 50){
            nuigurumiImage.sprite = step01;
        }else if(QuestGauge.value >= 50 && QuestGauge.value < 75){
            nuigurumiImage.sprite = step02;
        }else if(QuestGauge.value >= 75 && QuestGauge.value < 100){
            nuigurumiImage.sprite = step03;
        }else if(QuestGauge.value >= 100){
            nuigurumiImage.sprite = step04;
        }
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        //カーソルがDollObjectの判定範囲に入ってきた時の処理
        //pointerDragがnullの時、また、DragControllerが付与されてない時は何もしない
        if(pointerEventData.pointerDrag == null) return;
        if(pointerEventData.pointerDrag.GetComponent<DragController>() == null) return;
        
        //ドラッグしてきた画像を変数に格納
        Image droppedImage = pointerEventData.pointerDrag.GetComponent<Image>();

        Debug.Log("Enter");
        
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        //カーソルが判定範囲から離れた後の処理
        //pointerDragがnullの時、また、DragControllerが付与されてない時は何もしない
        if(pointerEventData.pointerDrag == null) return;
        if(pointerEventData.pointerDrag.GetComponent<DragController>() == null) return;
        
        //初期画像に戻す
        nuigurumiImage.sprite = nowSprite;
        
        //もしnullであった場合は透明にする
        if(nowSprite == null)
            nuigurumiImage.color = Vector4.zero;
        else
            nuigurumiImage.color = Vector4.one;

        Debug.Log("Exit");

    }
    
    public void OnDrop(PointerEventData pointerEventData)
    {
        //アイテムをドロップした時の処理
        //pointerDragがnullの時、また、DragControllerが付与されてない時は何もしない
        if(pointerEventData.pointerDrag.GetComponent<DragController>() == null) return;        
        
        Image droppedImage = pointerEventData.pointerDrag.GetComponent<Image>();

        //ポイントを加算する
        //どれくらい加算するかはNaoshiQuestControllerのなかで行っている
        float itemScore = GetUpableScore(naoshiQuestController.OnDropNaoshiItem(droppedImage.sprite.name));
        //float itemScore = GetUpableScore(naoshiQuestController.OnDropNaoshiItem("ふつうのワタ"));
        float questGaugeScore = QuestGauge.value;
        StartCoroutine (AddScore(questGaugeScore, itemScore));
	}

    private IEnumerator AddScore(float Score, float AddScore) {
	    while(QuestGauge.value < Score + AddScore){
	        QuestGauge.value += 1.0f;
            
            if(QuestGauge.value >= 100.0f){
                NaoshiStatus.Instance.data.flgClear = true;
            }

    	    //yield return new WaitForSeconds (0.001f);
            yield return new WaitForSeconds (0.01f);
	    }
	}

    private float GetUpableScore(float itemScore){
        //内部的なポイントの加算と、ゲージ上に見える数値の加算は分けて考えている。
        //例えば、フェーズ1で150pt、フェーズ2で250pt貯めるとした時、
        //20のポイントが加算されたとしても、
        //ゲージで上がるのは、
        //(20÷(250+250)) × 100 = 10
        //となる

        float totalPhasePoints = NaoshiStatus.Instance.data.totalPhasePoints;
        float UpableScore = 100 * (itemScore/totalPhasePoints);
        return UpableScore;
    }
}
