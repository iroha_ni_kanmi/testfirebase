﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class NaoshiStatus : MonoBehaviour {

	// シングルトン
    private static NaoshiStatus _instance = null;
	// シングルトン
    public static NaoshiStatus Instance {
        get {
			if (_instance == null ) {
                _instance = MonoBehaviourExtensions.CreateGameObject<NaoshiStatus>();
                _instance.Load();
                //GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
		}
	}

	[Serializable]
    public class naoshiStatus {

        //アイテムがマッチしているか否か
        [SerializeField]
        public typeMatching matching  = new typeMatching();

        //クリアしたかどうか
        [SerializeField]
        public bool flgClear;

        //クエストのアイテムの加算比率はどの程度のものか
        [SerializeField]
        public float totalPhasePoints;

        public enum typeMatching {
            great,
            good,
            normal,
            bad,
            question,
        }


        public void ResetData() {
            matching  = new typeMatching();
            flgClear = false;
            totalPhasePoints = 0;
        }

    }

    // 保存データ
    public naoshiStatus data = new naoshiStatus();

    // 保存キー
    private const string saveKey = "SaveNaoshiStatus";

	public void Load() {
		if( !PlayerPrefs.HasKey( saveKey ) ) {
            data.ResetData();
			return;
		}
		var base64 = PlayerPrefs.GetString (saveKey);
		var json = Base64Encoder.Decode (base64);
		//Debug.Log (json);
        data = JsonUtility.FromJson<naoshiStatus> (json);
	}

	public void Save() {
		var json = JsonUtility.ToJson (data);
		//Debug.Log (json);
		var base64 = Base64Encoder.Encode (json);
		PlayerPrefs.SetString (saveKey, base64);
	}

}
