﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class NaoshiViewController : MonoBehaviour
{

	//なおし画面の画面を構成するためのスクリプト
	//ぬいぐるみの描画は、ゲージの振り幅に応じて、動的に対応するため、
	//DragDropController.csに処理を記載している
	//依頼人の描画は、
	//ClientController.csに処理を記載している

	[Header("QuestDollSetting"), SerializeField]
	public GameObject QuestDoll;
    
    [Header("ItemListSetting"), SerializeField]
	public RectTransform ItemList;
	public Transform rootContent;
	public GameObject OriginalItemObj;

	[Header("ItemDataSetting"), SerializeField]
	public ToggleGroup ItemGenre;
	private ItemDataBase itemDataBase;

        
	[Header("ButtonSetting"), SerializeField]
	Button CloseButton;

	public string SceneName;
	public GameObject DialogButton;
    private Animator anim;

	//内部判定用関数
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_id = new Dictionary<string, materiList.materiInfo>();    


    void Awake () {
        //閉じるボタンに処理を追加
		CloseButton.onClick.AddListener( OnClickCloseButton );

        //クエストコンプリート画面(ボタン)に処理を追加
		DialogButton.GetComponent<Button>().onClick.AddListener( OnClickCompleteDialogButton );

        //アニメーションセット
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
    }
	
	// Use this for initialization
	void Start () {
		//マテリ・衣服・キーアイテム情報の取得
        dicMateriListInfo_id = ItemDataStore.Instance.data.materiList.getMateriInfo_id();

        //アイテムのタグで並び替えに、UniRxで対応する
		string selectedGenre = Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
		RenderMateri(selectedGenre);

		ItemGenre.ObserveEveryValueChanged( _ => _.ActiveToggles())
			.Subscribe (text => {
				if(selectedGenre != Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0]){
					selectedGenre = Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
					//RenderItemData(selectedGenre);
					RenderMateri(selectedGenre);
				}
			});

		//アイテムリストの画面比率調整
		rootContent.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(ItemList.rect.width/3.5f,ItemList.rect.width/3.5f);
		
		//なおしクエストが完了したら、完了画面を表示する
		NaoshiStatus.Instance.data.ObserveEveryValueChanged( _ => _.flgClear)
			.Subscribe (flg => {
				if(NaoshiStatus.Instance.data.flgClear){
					DialogButton.SetActive(true);
				}
			});
    }

	// Update is called once per frame
	void Update () {
		
	}
	
	void OnClickCompleteDialogButton(){
		DialogButton.SetActive(false);

		//クエストのコンプリート
		for(int i = 0; i < PlayerDataStore.Instance.data.quests.Length; i++){
			if(PlayerDataStore.Instance.data.quests[i].id == QuestDataStore.Instance.data.questId){
				PlayerDataStore.Instance.data.quests[i].progress = true;
			}
		}
        //なおしをコンプリートした場合も会話が発生するためメイン画面は読み込まない
        //LoadSceneController.LoadSceneObjects(SceneName);
		string scenarioLabel = "complete-" + QuestDataStore.Instance.data.questId;
		QuestDataStore.Instance.data.DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
        CloseAnim();
    }

	void OnClickCloseButton(){
        //なおしを中断した場合も会話が発生するためメイン画面は読み込まない
		//LoadSceneController.LoadSceneObjects(SceneName);
		string scenarioLabel = "stop-" + QuestDataStore.Instance.data.questId;
		Debug.Log("hit");
		QuestDataStore.Instance.data.DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
        CloseAnim();
    }

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
		//StatusDataStore.Instance.data.viewMode = "MainView";
        //SceneManager.UnloadSceneAsync(SceneName);
    }

	void RenderMateri(string selectedSubGenre){
		
		//itemのデータを取得
		ItemInventory[] ary_items = PlayerDataStore.Instance.data.items;

		//ItemList以下のオブジェクトクリア
		foreach ( Transform obj in rootContent )
		{
			GameObject.Destroy(obj.gameObject);
		}

		for(int i=0; i<ary_items.Length; ++i){
			
			if(selectedSubGenre == "All" || dicMateriListInfo_id[ary_items[i].id].genre == selectedSubGenre){

				//複製処理
        		// OriginalInfoViewから、RateBarを複製
	        	GameObject ItemObj = Instantiate<GameObject>( OriginalItemObj );
            	// オブジェクトを表示
        	    ItemObj.gameObject.SetActive( true );
            	// 親子関係にする
            	ItemObj.transform.SetParent( rootContent, false );
	        	//子要素を取得
    	    	var childTransform = ItemObj.GetComponentsInChildren<Transform>();
				//オブジェクト名をidに変更
				ItemObj.name = ary_items[i].id;

        	    //アイテム名と画像を更新
            	foreach (Transform child in childTransform){
        			//アイテム名の更新
            		if(child.name == "ItemNameText"){
			            child.GetComponent<Text>().text = "× " + ary_items[i].quantity;

						//値を監視
		        	    PlayerDataStore.Instance.data.items[i].ObserveEveryValueChanged( _ => _.quantity)
			        	.Subscribe (text => {
			            	child.GetComponent<Text>().text = "× " + PlayerDataStore.Instance.data.items[i].quantity;
			    	    });			            
			    	//画像の更新
	        		}else if(child.name == "ItemImage"){
		        	    child.GetComponent<Image>().sprite = Resources.Load(dicMateriListInfo_id[ary_items[i].id].filePath, typeof(Sprite)) as Sprite;
	            	}
            	}
			}
		}

		//アイテム順序の並び替え
		foreach (Transform obj in rootContent.transform){
			if(dicMateriListInfo_id.ContainsKey(obj.name)){
				obj.SetSiblingIndex(dicMateriListInfo_id[obj.name].sortNumber);
			}
		}

	}

}
