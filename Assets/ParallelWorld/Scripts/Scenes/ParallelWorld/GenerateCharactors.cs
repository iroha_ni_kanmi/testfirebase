﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using GoMap;

public class GenerateCharactors : MonoBehaviour {

	public List<GameObject> gameCharactors;
	private bool generateFlg;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void generateCharactors(){

		//キャラクター生成処理
		//GoMapで生成されるroadの半数分だけ、キャラクターが生成される
		

		if(generateFlg){
			GameObject goMapRoad = GameObject.FindGameObjectsWithTag("road").Last();
			//生成
			GameObject charactor = Instantiate<GameObject>( gameCharactors.GetAtRandom() );
					
			//道の中心座標を取得
			var goMapRoadRender = goMapRoad.GetComponent<MeshFilter>().mesh;
			goMapRoadRender.RecalculateBounds();

			var bounds = goMapRoadRender.bounds;

			Vector3 center = bounds.center;
			Vector3 size = bounds.size;
			size.y = 20.0f;
					
			//中心点にオブジェクトを配置
			center.y += 2.0f;
			charactor.transform.position = center;
			charactor.transform.SetParent (goMapRoad.transform);

			//charactor.transform.localScale = new Vector3(3,3,3);

			generateFlg = false;
		}else{
			generateFlg = true;
		}

		
	}

}
