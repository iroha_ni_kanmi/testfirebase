﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PW_CameraController_2 : MonoBehaviour
{
    /// <summary>
    /// ターゲット
    /// </summary>
    public GameObject obj;
    /// <summary>
    /// ターゲットの位置
    /// </summary>
    private Vector3 targetPos;
    /// <summary>
    /// アングル変更フラグ（PC用）
    /// </summary>
    private bool angeFlg;
    /// <summary>
    /// カメラ sizs 最小値
    /// </summary>
    float sMin = 30f;
    /// <summary>
    /// カメラ sizs 最大値
    /// </summary>
    float sMax = 60f;
    /// <summary>
    /// 直前の2点間の距離
    /// </summary>
    private float befDist = 0.0f;
    /// <summary>
    /// スクリーンサイズ 横
    /// </summary>
    float wid = Screen.width;
    /// <summary>
    /// スクリーンサイズ 縦
    /// </summary>
    float hei = Screen.height;
    /// <summary>
    /// スクリーンサイズ（対角線の長さ）
    /// </summary>
    float diag;
    /// <summary>
    /// 2点間の距離 最大値
    /// </summary>
    float dMax;
    /// <summary>
    /// 2点間の距離 最小値
    /// </summary>
    float dMin = 0f;
    /// <summary>
    /// 初回時の2点のタッチ位置
    /// </summary>
    Touch beft1;
    /// <summary>
    /// 初回時の2点のタッチ位置
    /// </summary>
    Touch beft2;

    void Start()
    {
        // ターゲットの位置を取得
        targetPos = obj.transform.position;
        // スクリーンサイズ（対角線の長さ）を取得
        diag = Mathf.Sqrt(Mathf.Pow(wid, 2) + Mathf.Pow(hei, 2));
        //2点間の距離の最大値に代入（一旦 diag の8割にする）
        dMax = (diag * 8) / 10;
        dMax = diag;

        // 初回時の2点のタッチ位置
        beft1 = new Touch();
        beft2 = new Touch();
    }

    void Update()
    {
        // targetの移動量分、自分（カメラ）も移動する
        transform.position += obj.transform.position - targetPos;
        targetPos = obj.transform.position;

        float mouseInputX = 0;
        float mouseInputY = 0;

        // マルチタッチかどうか
        if (Input.touchCount >= 2)
        {
            // 実機操作（スマホ・タブレット）
            StatusDataStore.Instance.data.movableStatus = true;

            // 2点のタッチ位置を取得
            Touch t1 = Input.GetTouch(0);
            Touch t2 = Input.GetTouch(1);

            // x,y の初期化 TODO: 2点スワイプとピンチインアウトの切り分け
            mouseInputX = 0;
            mouseInputY = 0;

            // 2点が移動されたとき、移動後の距離を取得
            //float newDist = Vector2.Distance(t1.position, t2.position);
            float newDist = 0f;

            // virtual cameraのカメラsizeを変更するよう改修 コンポーネントCinemachineVirtualCamera
            Cinemachine.CinemachineVirtualCamera vc1 = this.GetComponent<Cinemachine.CinemachineVirtualCamera>();
            GameObject vc2obj = GameObject.Find("CM vcam2");

            // ピンチイン ピンチアウト ------------s
            if (t2.phase == TouchPhase.Began)
            {
                // 初回時は2点タッチが開始されたとき、開始時の距離を取得
                befDist = Vector2.Distance(t1.position, t2.position);

                // 初回時の2点のタッチ位置を取得
                beft1 = Input.GetTouch(0);
                beft2 = Input.GetTouch(1);
            }
            else if ((t1.phase == TouchPhase.Moved || t1.phase == TouchPhase.Stationary) &&
                     (t2.phase == TouchPhase.Moved || t2.phase == TouchPhase.Stationary))
            {
                // 変更後のサイズ
                float newsize = 0f;


                newDist = Vector2.Distance(t1.position, t2.position);

                //// コンポーネント カメラ
                //Camera camera = this.GetComponent<Camera>();

                // 変更後のサイズ
                float size = 0f;

                // 2点間の距離を正規化する
                size = (newDist - dMin) / (dMax - dMin) * 2;

                if (befDist < newDist)
                {
                    // ピンチイン時 カメラの距離サイズを小さくする（寄る）
                    //newsize = camera.orthographicSize - size;
                    newsize = vc1.m_Lens.OrthographicSize - size;

                }
                else if (befDist > newDist)
                {
                    // ピンチアウト時 カメラの距離サイズを大きくする（引く）
                    //newsize = camera.orthographicSize + size;
                    newsize = vc1.m_Lens.OrthographicSize + size;
                }
                else
                {
                    // 変更なし
                    //newsize = camera.orthographicSize;
                    newsize = vc1.m_Lens.OrthographicSize;
                }

                // カメラsizeの変域を越えないようにする
                if (newsize > sMax)
                {
                    newsize = sMax;
                }
                else if (newsize < sMin)
                {
                    newsize = sMin;
                }

                //camera.orthographicSize = newsize;
                vc1.m_Lens.OrthographicSize = newsize;

            }
            // ピンチイン ピンチアウト ------------e

            // 2点スワイプ ------------s
            // 移動量
            //mouseInputX = Input.GetAxis("Mouse X");
            mouseInputX = beft1.position.x - t1.position.x;
            // 2点間の距離を正規化する 最大値は画面対角線サイズにしてみる
            mouseInputX = (mouseInputX - dMin) / (diag - dMin) * 2;
            //mouseInputY = Input.GetAxis("Mouse Y");

            // targetの位置のY軸を中心に、回転（公転）する
            transform.RotateAround(targetPos, Vector3.up, mouseInputX * Time.deltaTime * 150f);
            // 会話シーン前のズーム用カメラも回転（公転）する
            vc2obj.transform.RotateAround(targetPos, Vector3.up, mouseInputX * Time.deltaTime * 150f);

            // カメラの垂直移動（※角度制限なし、必要が無ければコメントアウト）
            //transform.RotateAround(targetPos, transform.right, mouseInputY * Time.deltaTime * 200f);
            // 2点スワイプ ------------e
        }
        else
        {
            // PC操作 現状は視点の回転のみ動作
            // マウスの右クリックを押している間
            if (Input.GetMouseButton(1))
            {
                StatusDataStore.Instance.data.movableStatus = false;
                if (angeFlg)
                {
                    // マウスの移動量
                    mouseInputX = Input.GetAxis("Mouse X");
                    mouseInputY = Input.GetAxis("Mouse Y");
                }
                else
                {
                    angeFlg = true;

                }
                // targetの位置のY軸を中心に、回転（公転）する
                transform.RotateAround(targetPos, Vector3.up, mouseInputX * Time.deltaTime * 150f);
                // カメラの垂直移動（※角度制限なし、必要が無ければコメントアウト）
                //transform.RotateAround(targetPos, transform.right, mouseInputY * Time.deltaTime * 200f);
            }
            else
            {
                angeFlg = false;
            }
        }
    }
}