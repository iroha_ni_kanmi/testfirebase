﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Text.RegularExpressions;

public class PW_NavMeshController : MonoBehaviour
{
    static Vector3 startPosition = new Vector3();
    static NavMeshAgent agent;
    public static bool GoNavMesh = false;
    // デバッグ用
    public static　GameObject obj;    

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        startPosition = this.transform.position;
        // デバッグ用
        obj = GameObject.Find("uematsusan");


    }

    // Update is called once per frame
    void Update()
    {
        if (GoNavMesh)
        {
            // エージェントが現在の巡回地点に到達したら
            if (!agent.pathPending && agent.remainingDistance < 0.5f)
            {
                // 次の巡回地点を設定する処理を実行
                GotoNextPoint();
            }
        }        
    }

    public static void GotoNextPoint()
    {
        RaycastHit[] hits = new RaycastHit[0];

        // ランダムな点を取得（一旦半径10以内）
        var randDestination = Random.insideUnitCircle * 100;

        // NavMesh上の座標を取得
        int layerMask = 1 << LayerMask.NameToLayer("Roads"); // "Roads"レイヤー以外を無視
        // うまく当たらないので保留
        //hits = Physics.RaycastAll(startPosition + new Vector3(randDestination.x, 5, randDestination.y), Vector3.down, 10, layerMask);
        hits = Physics.RaycastAll(startPosition + new Vector3(randDestination.x, 5, randDestination.y), Vector3.down, 10);

        foreach (var hit in hits)
        {
            bool isGoMapNavMesh = Regex.IsMatch(hit.collider.ToString(), @"^[0-9]{5}-[0-9]{5}-[0-9]{2}");

            if (isGoMapNavMesh)
            {
                ////　デバッグ用 うえまつさん配置
                //Instantiate(obj, hit.point, Quaternion.identity);
                //obj.name = hit.collider.ToString();

                // agentの次の目的地に設定
                agent.destination = hit.point;
                break;
            }            
        }
    }
}






































////using System.Collections;
////using System.Collections.Generic;
////using UnityEngine;
////// "NavMeshAgent"関連クラスを使用できるようにする
////using UnityEngine.AI;

////public class Patrol : MonoBehaviour
////{
////    // 巡回地点オブジェクトを格納する配列
////    public Transform[] points;
////    // 巡回地点のオブジェクト数（初期値=0）
////    private int destPoint = 0;
////    // NavMesh Agent コンポーネントを格納する変数
////    private NavMeshAgent agent;

////    // ゲームスタート時の処理
////    void Start()
////    {
////        // 変数"agent"に NavMesh Agent コンポーネントを格納
////        agent = GetComponent<NavMeshAgent>();
////        // 巡回地点間の移動を継続させるために自動ブレーキを無効化
////        //（エージェントは目的地点に近づいても減速しない)
////        agent.autoBraking = false;
////        // 次の巡回地点の処理を実行
////        GotoNextPoint();
////    }

////    // 次の巡回地点を設定する処理
////    void GotoNextPoint()
////    {
////        // 巡回地点が設定されていなければ
////        if (points.Length == 0)
////            // 処理を返します
////            return;
////        // 現在選択されている配列の座標を巡回地点の座標に代入
////        agent.destination = points[destPoint].position;
////        // 配列の中から次の巡回地点を選択（必要に応じて繰り返し）
////        destPoint = (destPoint + 1) % points.Length;
////    }

////    // ゲーム実行中の繰り返し処理
////    void Update()
////    {
////        // エージェントが現在の巡回地点に到達したら
////        if (!agent.pathPending && agent.remainingDistance < 0.5f)
////            // 次の巡回地点を設定する処理を実行
////            GotoNextPoint();
////    }
////}