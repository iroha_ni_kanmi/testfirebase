﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class CameraMoveController : MonoBehaviour {
	
	public Transform CameraPos;
	public Camera MainCamera;
	private Vector3 velocity = Vector3.zero;
	public float moveSpeed,rotationSpeed;
	private float step;

	// Use this for initialization
	void Start () {
		CameraPos.ObserveEveryValueChanged( _ => _.position)
		.Subscribe (pos => {
			//CameraPosの位置が変わった時の処理をここに記載
			StartCoroutine (MoveCamera());
			
			});
	}
	
	void FixedUpdate () {
		/*MainCamera.transform.position = Vector3.SmoothDamp
			(MainCamera.transform.position,
			CameraPos.position,
			ref velocity,
			moveSpeed* Time.deltaTime);
		step = rotationSpeed * Time.deltaTime;
        Quaternion rotation = Quaternion.RotateTowards 
        (MainCamera.transform.rotation, 
        CameraPos.rotation, 
        step);
        MainCamera.transform.rotation = rotation;*/
    }
	
	float GetFloat(float baseFloat, float OutFloat){
		if(baseFloat > OutFloat){
			return OutFloat + 0.1f;
		}else if(baseFloat < OutFloat){
			return OutFloat - 0.1f;
		}else{
			return OutFloat;
		}
	}

    private IEnumerator MoveCamera() {
    	
    	MainCamera.transform.position = Vector3.Lerp(CameraPos.position, MainCamera.transform.position, 5.0f * Time.deltaTime);

    	MainCamera.transform.rotation = Quaternion.Lerp(CameraPos.rotation, MainCamera.transform.rotation, 5.0f * Time.deltaTime);

		//yield return new WaitForSeconds (0.001f);
	    yield return null; // 
	}

}
