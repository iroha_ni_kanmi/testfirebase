﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using GoMap;

public class GenerateTown : MonoBehaviour {

	//public GameObject targetPlace;
	//public GameObject mapObj;

	public float max_pos_value;		//
	public float move_pos_value;	//２進数にすること

	// Use this for initialization
	void Start () {
		
		/*
		//targetPlaceに子要素でGameObjectを作成
		//x軸のTownGroupの作成
		for(float x=max_pos_value; x > -(max_pos_value); x = x - move_pos_value){
			GameObject townGroup = new GameObject();
			townGroup.name = "TownGroup";
			townGroup.transform.SetParent(targetPlace.transform);
			townGroup.transform.localPosition = new Vector3(x,0,0);
			townGroup.transform.localScale = new Vector3(1,1,1);

			//z軸に沿って、TownObjの作成
			for(float z=max_pos_value; z > -(max_pos_value); z = z - move_pos_value){
				GameObject townObj = new GameObject();
				townObj.name = "TownObj";
				townObj.transform.SetParent(townGroup.transform);
				townObj.transform.localPosition = new Vector3(0,0,z);
				townObj.transform.localScale = new Vector3(1,1,1);
				townObj.AddComponent<BoxCollider>().size = new Vector3(move_pos_value,move_pos_value,move_pos_value);
				townObj.GetComponent<BoxCollider>().isTrigger = true;
				townObj.AddComponent<BuildingController>();


			}
		}
		 */


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GenerateTownPatterns (GameObject mapObj) {

		foreach (Transform mapchild in mapObj.transform){
			bool generateFlg = false;
			foreach (Transform grandchild in mapchild.transform){
				if(grandchild.tag == "buildingGroup" && mapchild.name != "Feature prototype" && mapchild.name != "Street prototype"){
					generateFlg = true;
				}
			}

			if(!generateFlg){
				
				GameObject buildingGroup = new GameObject();
				buildingGroup.transform.SetParent(mapchild.transform);
				buildingGroup.name = "buildingGroup";
				buildingGroup.tag = "buildingGroup";
				buildingGroup.transform.localPosition = new Vector3(0,0,0);
				buildingGroup.transform.localScale = new Vector3(50,50,50);
				buildingGroup.AddComponent<Rigidbody>();
				buildingGroup.GetComponent<Rigidbody>().useGravity = false;
				buildingGroup.GetComponent<Rigidbody>().isKinematic = true;


				//targetPlaceに子要素でGameObjectを作成
				//x軸のTownGroupの作成
				for(float x=max_pos_value; x > -(max_pos_value); x = x - move_pos_value){
					GameObject townGroup = new GameObject();
					townGroup.name = "TownGroup";
					townGroup.tag = "TownGroup";
					townGroup.transform.SetParent(buildingGroup.transform);
					townGroup.transform.localPosition = new Vector3(x,0,0);
					townGroup.transform.localScale = new Vector3(1,1,1);

					//z軸に沿って、TownObjの作成
					for(float z=max_pos_value; z > -(max_pos_value); z = z - move_pos_value){
						GameObject townObj = new GameObject();
						townObj.name = "TownObj";
						townObj.tag = "TownObj";
						townObj.transform.SetParent(townGroup.transform);
						townObj.transform.localPosition = new Vector3(0,0,z);
						townObj.transform.localScale = new Vector3(1,1,1);
						//townObj.AddComponent<BoxCollider>().size = new Vector3(move_pos_value,move_pos_value,move_pos_value);
						townObj.AddComponent<BoxCollider>().size = new Vector3(move_pos_value,move_pos_value,move_pos_value);
						townObj.GetComponent<BoxCollider>().isTrigger = true;
						townObj.AddComponent<BuildingController>();
					}
				}
			}
		}
	}

}
