﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using GoMap;


public class ParallelWorldController : UIBehaviour {

	[Header("GoMapSetting"), SerializeField]
	public LocationManager _locationManager;
	public GOMap gomap;
	private bool tilesFlg;
	private int tilesCount;

	[Header("MoveAvaterSetting"), SerializeField]
	private NavMeshWalkerController _navMeshWalkerController;

	[Header("dialogSetting"), SerializeField]
	public DialogViewController dialogViewController;
	private string scenarioLabel;

	//内部処理関数
	private GameObject MapTile;


	void Awake () {
		//Area移動するためのlocationManagerをセット
		CoordinatesDataStore.Instance.data.locationManager = _locationManager;
		tilesFlg = true;
	}


	void Start () {
		
		//Tileを読み込んだ回数をカウント
		tilesCount = 0;

		this.ObserveEveryValueChanged( _ => _.tilesCount)
			.Subscribe (count => {
		    	//if(tilesCount == Math.Pow(2*gomap.tileBuffer+1, 2)){
		    	if(tilesCount == 1){
		    		StatusDataStore.Instance.data.loadingStatus = false;
					foreach(GameObject obj in GameObject.FindGameObjectsWithTag("buildingPW")){
						obj.AddComponent<MeshCollider>().enabled = true;
						obj.GetComponent<MeshCollider>().convex = true;
						obj.GetComponent<MeshCollider>().isTrigger = true;
						obj.AddComponent<BuildingDeleteController>();
					}
                    //TODO:チュートリアル処理が組み込み終わったら、以下の動作確認用処理を削除
                    //LoadSceneController.LoadSceneObjects("TutorialView");
                    //StatusDataStore.Instance.data.viewMode = "TutorialView";
                    
					//テスト実装
					//string scenarioLabel = "storyNo-01-00";
					//dialogViewController.GetQuestDialog(scenarioLabel);
                }
		    });

    }
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnLoadedTiles(Transform MapObj){
		if (tilesFlg){
			tilesCount += 1;
		}
		Debug.Log(tilesCount);
		/*
		foreach(Transform child in MapObj){
			if(child.name != "Feature prototype" &&child.name != "Street prototype"){
				MapTile = child.gameObject;
				if(!MapTile.HasComponent<EventTrigger>()){
					//EventTriggerの付与
					AddEventTrigger_ForMove(MapTile);
					//AddNavMesh(MapTile);
				}
			}
		} */
    }

	public void AddNavMesh(GameObject MapTile){
		//NavMeshSurfaceを付与。同時にベイク。
		MapTile.AddComponent<NavMeshSurface>().BuildNavMesh();
	}

	public void AddEventTrigger_ForMove(GameObject MapTile){
		//EventTriggerを付与する
		//※スクリプトでAddした場合、インスペクターには表示されないので注意(2019.03.27)

        EventTrigger currentTrigger = MapTile.AddComponent<EventTrigger>();
        currentTrigger.triggers = new List<EventTrigger.Entry>();
        //↑ここでAddComponentしているので一応、初期化しています。
           
		//付与するイベントは以下の３つ
		//01.Drag=============================
        EventTrigger.Entry entryDrag = new EventTrigger.Entry();
        entryDrag.eventID = EventTriggerType.Drag; //PointerClickの部分は追加したいEventによって変更してね
        entryDrag.callback.AddListener( (x) => _navMeshWalkerController.OnDragArea() );  //ラムダ式の右側は追加するメソッドです。

        currentTrigger.triggers.Add(entryDrag);

		//02.PointerUp=======================
        EventTrigger.Entry entryPU = new EventTrigger.Entry();
        entryPU.eventID = EventTriggerType.PointerUp; //PointerClickの部分は追加したいEventによって変更してね
        entryPU.callback.AddListener( (x) => _navMeshWalkerController.OnClickArea() );  //ラムダ式の右側は追加するメソッドです。

        currentTrigger.triggers.Add(entryPU);

		//03.PointerExit=====================
        EventTrigger.Entry entryPE = new EventTrigger.Entry();
        entryPE.eventID = EventTriggerType.PointerExit; //PointerClickの部分は追加したいEventによって変更してね
        entryPE.callback.AddListener( (x) => _navMeshWalkerController.OnPointerExitArea() );  //ラムダ式の右側は追加するメソッドです。

        currentTrigger.triggers.Add(entryPE);

	}

}
