﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GetItemController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime, Space.World);
	}
	
	public void GetItem (){
        
		Debug.Log("Item Get! ItemName is" + this.name);
		GameObject _parent = this.gameObject.transform.parent.gameObject;
		Debug.Log(_parent.name);
		Destroy(this.gameObject);
	}

	public void ChangeParent(Transform parentObj){
		//EditorApplication.isPlaying = false;	//どのタイミングでpoiが生成されているか確認 ＝> 生成される前にこのOnPoiLoadは実行されていた

		string[] ary_energy = new string[]{"Energy_hari","Energy_ito","Energy_nuno","Energy_wata"};
		GameObject[] list_poi = GameObject.FindGameObjectsWithTag("poi");
		
		string i;
		
		foreach (GameObject poi in list_poi){
			//go mapの不具合で、pivotがずれて生成されてしまうため、
			//一度poiの情報を取得したprefabを生成し、poiを消すことを行う
			i = ary_energy[UnityEngine.Random.Range(0,ary_energy.Length)];
			GameObject prefab = (GameObject)Resources.Load ("Prefabs/WorldObjects/ClothWorld/Materials/"+i);
			var obj = Instantiate (prefab, poi.transform.position, Quaternion.identity);
			obj.name = poi.name;
			obj.transform.SetParent (parentObj);
			Destroy(poi);
		}
	}

}
