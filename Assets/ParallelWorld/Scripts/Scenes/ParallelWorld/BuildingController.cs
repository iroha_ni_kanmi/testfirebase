﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider obj){

		if(!StatusDataStore.Instance.data.loadingStatus){

			if(obj.tag == "road"){
				Destroy(this.gameObject);
			}else{

				if(!this.gameObject.HasChild()){
					//Debug.Log("obj NAME = " + obj.name);

					GameObject[] buildings = Resources.LoadAll<GameObject>("Prefabs/TownPatterns");
			
					var building = Instantiate(buildings[Random.Range(0, buildings.Length)]);

					building.transform.SetParent(this.transform);
					building.tag = "building";
					building.transform.localPosition = new Vector3(0,0,0);
					building.transform.localScale = new Vector3(0.15f,0.15f,0.15f);
					building.transform.Rotate(0.0f, 0.0f, 0.0f);

					Vector3 colSize = GetComponent<BoxCollider>().size;
					colSize.y = 1;
					GetComponent<BoxCollider>().size = colSize;

					//無駄なboxColliderを削除
					//Destroy( GetComponent<BoxCollider>());
				}
			}


		}
	}

}


public static class RandomUtils
{
    /// <summary>
    /// 指定された配列の中からランダムに要素を返します
    /// </summary>
    public static T Random<T>( params T[] values )
    {
        return values[ UnityEngine.Random.Range( 0, values.Length ) ];
    }
}