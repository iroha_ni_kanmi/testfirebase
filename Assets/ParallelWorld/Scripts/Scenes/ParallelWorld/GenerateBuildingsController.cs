﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using GoMap;

public class GenerateBuildingsController : MonoBehaviour {
	
	[Header("BuildingSetting"), SerializeField]
	public string worldMode;
	public List<GameObject> buildingObjs;
	private bool replaceFlg;
	
	public GameObject mapObj;
	public GameObject testBuildingGroup01;
	
	void Awake () {
		
		switch (worldMode)
		{
			case "cloth":
			/*
				//設定されていればreplace処理を行う
				replaceFlg = true;
				
				//Clothワールドに付随するオブジェクトを全部取得
				UnityEngine.Object[] objs = Resources.LoadAll("Prefabs/WorldObjects/ClothWorld/Buildings") ;
				
				buildingObjs = new List<GameObject>();
				foreach (GameObject building in objs){
					buildingObjs.Add(building as GameObject);
				}
            	break;
			 */

			default:
				replaceFlg = false;
            	break;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	public void ReplaceBuilding(){
		GameObject goMapBuilding = GameObject.FindGameObjectsWithTag("building").Last();
		//生成
		GameObject buildingObj = Instantiate<GameObject>( buildingObjs.GetAtRandom() );
					
		//建物の中心座標を取得
		var goMapBuildingRender = goMapBuilding.GetComponent<MeshFilter>().mesh;
		goMapBuildingRender.RecalculateBounds();

		var bounds = goMapBuildingRender.bounds;

		Vector3 center = bounds.center;
		Vector3 size = bounds.size;
		size.y = 20.0f;
					
		//中心点にオブジェクトを配置
		buildingObj.transform.SetParent( goMapBuilding.transform.parent, false );
		//center.y += 10.0f;
		buildingObj.transform.position = center;
		//buildingObj.transform.localScale = size;
		goMapBuilding.SetActive(false);
	}

	public void addRigidbody(string tagName){
		GameObject[] list_building= GameObject.FindGameObjectsWithTag(tagName);

		foreach(var building in list_building){
			if(!building.GetComponent<Rigidbody>()){
				//if Ball does not exist
				building.AddComponent<Rigidbody>();
				building.GetComponent<Rigidbody>().useGravity = false;
				building.GetComponent<Rigidbody>().isKinematic = true;
			}
		}
	}

	public void GenerateBuildingGroup(){

		foreach (Transform mapchild in mapObj.transform){
			bool generateFlg = false;
			foreach (Transform grandchild in mapchild.transform){
				if(grandchild.tag == "buildingGroup" && mapchild.name != "Feature prototype" && mapchild.name != "Street prototype"){
					generateFlg = true;
				}
			}

			if(!generateFlg){
				GameObject buildingGroup = Instantiate<GameObject>( testBuildingGroup01 );
				buildingGroup.transform.SetParent( mapchild.transform, false );
			}

		}


	}

}
