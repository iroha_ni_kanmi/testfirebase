﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCController : MonoBehaviour {
	
	public GameObject Player;
	public GameObject NPC;
	public GameObject NPCCanvas;
	public GameObject ConversionSignButton;

	public string SceneName;

	public string QuestName;
	public int QuestProgress;
	public string QuestDollName;

	private float distance;
	private bool CSBFlg = false;
	private bool ScaleFlg = true;
	private float Interval = 2f;

	// Use this for initialization
	void Start () {
		//Dialog
		ConversionSignButton.GetComponent<Button>().onClick.AddListener( OnClickConversionSignButton );
	}
	
	// Update is called once per frame
	void Update () {
		//ButtonObjの向き調整
		SetUIObjToCamera();

		Vector3 PlayerPos = Player.transform.position;
        Vector3 NPCPos = this.transform.position;
		distance = Vector3.Distance(PlayerPos, NPCPos);
		
		if(distance < 50){
			//近づいたら会話アイコンが表示されるにする
			CSBFlg = true;
			StartCoroutine (GetConversionSign(ConversionSignButton));
		}else{
			Interval = 2f;
			CSBFlg = false;
		}
	}

	void SetUIObjToCamera(){
		
		NPCCanvas.transform.rotation = Camera.main.transform.rotation;
		NPC.transform.LookAt(Player.transform);
	}
	
	
	private IEnumerator GetConversionSign(GameObject ButtonObj) {
		while(CSBFlg){
	    	if(ScaleFlg){
		    	ButtonObj.transform.localScale += new Vector3(0.01f,0.01f, 0);
		    	if(ButtonObj.transform.localScale.x >= 2.0f){
		    		ScaleFlg = false;
		    	}
    		}else{
	    		ButtonObj.transform.localScale -= new Vector3(0.01f,0.01f, 0);
		    	if(ButtonObj.transform.localScale.x <= 1.5f){
		    		ScaleFlg = true;
		    	}
	    	}
	    	Interval += 2f;
        	yield return new WaitForSeconds (Interval);  
        }
   }
   
   public void OnClickConversionSignButton(){
   		
		//クエスト情報を更新
		//QuestDataStore.Instance.data.title = QuestName;
		//QuestDataStore.Instance.data.progress = QuestProgress;

		//クエストでなおすぬいぐるみの名前と素材名を取得
		//ItemDataStore.Instance.data.questDoll = QuestDollName;	
		//ItemDataStore.Instance.data.materialsForQuestDoll = MaterialsForQuestDoll;	
		
	   	LoadSceneController.LoadSceneObjects(SceneName);
   }
	
}
