﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChaseTargetIcon_2 : MonoBehaviour
{


    [SerializeField]
    Transform target;
    [SerializeField]
    Camera targetCamera;
    [SerializeField]
    Image icon;
    [SerializeField]
    Image childicon;
    /// <summary>
    /// クエストキャラクター
    /// </summary>
    [SerializeField]
    Transform character;


    private Rect rect = new Rect(0, 0, 1, 1);

    private Rect canvasRect;

    void Start()
    {
        // UIがはみ出ないようにする
        canvasRect = ((RectTransform)icon.canvas.transform).rect;
        canvasRect.Set(
            canvasRect.x + icon.rectTransform.rect.width * 0.5f,
            canvasRect.y + icon.rectTransform.rect.height * 0.5f,
            canvasRect.width - icon.rectTransform.rect.width,
            canvasRect.height - icon.rectTransform.rect.height
        );        
    }

    void Update()
    {
        var viewport = targetCamera.WorldToViewportPoint(target.position);
        var characterviewport = targetCamera.WorldToViewportPoint(character.position);
        //if (rect.Contains(viewport))
        if (rect.Contains(characterviewport))
            {
            icon.enabled = true;
            childicon.enabled = true;

            //アイコンのRectを変更する
            icon.rectTransform.anchoredPosition = Rect.NormalizedToPoint(canvasRect, viewport);
        }
        else
        {
            icon.enabled = false;
            childicon.enabled = false;

            //// 画面内で対象を追跡
            //viewport.x = Mathf.Clamp01(viewport.x);
            //viewport.y = Mathf.Clamp01(viewport.y);
            //icon.rectTransform.anchoredPosition = Rect.NormalizedToPoint(canvasRect, viewport);
            //var diff = (target.position - icon.transform.position).normalized;
        }

        
    }
}
