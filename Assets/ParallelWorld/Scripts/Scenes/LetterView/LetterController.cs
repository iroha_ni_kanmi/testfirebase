﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class LetterController : MonoBehaviour {

	[Header("LetterListSetting"), SerializeField]
	public RectTransform LetterList;
	public Transform rootContent;
	public GameObject OriginalLetterObj;

	[Header("LetterDetailSetting"), SerializeField]
	public GameObject LetterDetailDialog;
	public GameObject LetterBar;
	public GameObject buttonClose;

	//内部判定用関数
	private Dictionary<string, messageList.messageInfo> dicMessageInfo = new Dictionary<string, messageList.messageInfo>();


	// Use this for initialization
	void Start () {

		//データのセット
        dicMessageInfo = QuestDataStore.Instance.data.messageList.getDicMessageInfo();

		RenderLetter();		
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void RenderLetter(){
		//レターデータをメニューに描画する

		//完了したクエスト一覧を取得
		QuestInventory[] ary_quests = PlayerDataStore.Instance.data.quests;

		//手紙のタイトル、説明などの情報を格納する一時的な辞書を宣言
		Dictionary<string, string> dicLetterInfoValue = new Dictionary<string, string>();
		
		//クエスト一覧をリセット
		foreach ( Transform obj in rootContent )
		{
			GameObject.Destroy(obj.gameObject);
		}


		//現在進行中のクエストを描画していく
		for(int i=0; i<ary_quests.Length; ++i){
			//クエストをクリアしており、messageListにidがあれば、オブジェクトをセット
			if(ary_quests[i].progress && dicMessageInfo.ContainsKey(ary_quests[i].id)){

                dicLetterInfoValue["LetterTitleText" + ary_quests[i].id] = dicMessageInfo[ary_quests[i].id].messageTitle;
                dicLetterInfoValue["LetterFromName" + ary_quests[i].id] = dicMessageInfo[ary_quests[i].id].characterName;
                dicLetterInfoValue["LetterMessageText" + ary_quests[i].id] = dicMessageInfo[ary_quests[i].id].messageText;
                dicLetterInfoValue["LetterImage" + ary_quests[i].id] = dicMessageInfo[ary_quests[i].id].imageFilePath;

				Debug.Log("done");
				Debug.Log(ary_quests[i].id);

			    //手紙を描画
				SetLetterData(ary_quests[i], dicLetterInfoValue, i);
			}
		}
	}

	public void SetLetterData(QuestInventory quest, Dictionary<string, string> dicLetterInfoValue,int index){

		//複製処理
    	// OriginalInfoViewから、RateBarを複製
        GameObject LetterObj = Instantiate<GameObject>( OriginalLetterObj );
    	// オブジェクトを表示
        LetterObj.gameObject.SetActive( true );
    	// 親子関係にする
        LetterObj.transform.SetParent( rootContent, false );
    	//子要素を取得
        var childTransform = LetterObj.GetComponentsInChildren<Transform>();
		//オブジェクト名をidに変更
		LetterObj.gameObject.name = quest.id;
		//ボタンを押した際の挙動をadd
		LetterObj.GetComponent<Button>().onClick.AddListener( () => GetLetterDetailView(LetterObj.gameObject.name, dicLetterInfoValue, index) );
		//Debug.Log("ボタン付与完了");
		//Debug.Log(item,.id);

        //クエスト名と説明、画像を更新
    	foreach (Transform child in childTransform){
            //クエスト名の更新
        	if(child.name == "LetterTitleText"){
	            child.GetComponent<Text>().text = dicLetterInfoValue["LetterTitleText"+quest.id];
		    //差出人名の更新
        	}else if(child.name == "LetterSubTitleArea"){
	        	child.GetComponentsInChildren<Transform>()[2].GetComponent<Text>().text = dicLetterInfoValue["LetterFromName"+quest.id];
		    //画像の更新
        	}else if(child.name == "LetterCharaLayer"){
	            child.GetComponentsInChildren<Transform>()[1].GetComponent<Image>().sprite = Resources.Load(dicLetterInfoValue["LetterImage"+quest.id], typeof(Sprite)) as Sprite;

    		//クエストの進行状況の更新
        	}else if(child.name == "LetterNotReadImage"){
                checkPlayerView(quest.read, child.gameObject, index);
    		}
		}
	}

	public void checkPlayerView(bool read, GameObject imageObj, int index){
		//プレイヤーがクエストの詳細を参照済か否かを確認
		//未参照の場合、アウトラインで囲い、
		//参照済の場合、アウトラインを消す

		if(read){
			//チェック済の場合、未読マークを非表示にする
			imageObj.SetActive(false);
		}else{
			//チェック済の場合、未読マークを表示する
			imageObj.SetActive(true);

			//UniRxにて、プレイヤーデータの値を監視
			//チェックが更新されたら、アウトラインも消す
		}
		PlayerDataStore.Instance.data.quests[index].ObserveEveryValueChanged( _ => _.read)
		.Subscribe (flg => {
			if(PlayerDataStore.Instance.data.quests[index].read){
				if(imageObj != null){
					imageObj.SetActive(true);
				}
			}
		});


	}

	public void GetLetterDetailView(string letterId, Dictionary<string, string> dicLetterDetail,int index){
		//子要素を取得

        var childTransform = LetterBar.GetComponentsInChildren<Transform>();
		foreach (Transform child in childTransform){
			//クエスト名の更新
    	    if(child.name == "LetterTitleText"){
	        	child.GetComponent<Text>().text = dicLetterDetail["LetterTitleText"+letterId];
		    //説明の更新
			}else if(child.name == "LetterFromName"){
				child.GetComponent<Text>().text = dicLetterDetail["LetterFromName"+letterId];
			//画像の更新
			}else if(child.name == "LetterImage"){
	    	    child.GetComponent<Image>().sprite = Resources.Load(dicLetterDetail["LetterImage"+letterId], typeof(Sprite)) as Sprite;
			//内容の更新
			}else if(child.name == "LetterMessageText"){
				child.GetComponent<Text>().text = dicLetterDetail["LetterMessageText"+letterId];
			}
		}

		buttonClose.GetComponent<Button>().onClick.AddListener( CloseLetterDetail );

		LetterDetailDialog.SetActive(true);
		if(!PlayerDataStore.Instance.data.quests[index].check){
			PlayerDataStore.Instance.data.quests[index].check = true;
		}

	}

	public void CloseLetterDetail(){
		LetterDetailDialog.SetActive(false);
	}


}
