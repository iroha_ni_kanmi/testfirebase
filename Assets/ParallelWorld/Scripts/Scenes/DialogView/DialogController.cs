﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utage;
using ParallelWorld.DBModel;

public class DialogController : ModuleQuest {

    AdvEngine Engine { get { return engine ?? (engine = FindObjectOfType<AdvEngine>()); } }
    public AdvEngine engine;

    private string scenarioLabel;
 
	public void GetQuestDialog(string scenarioLabel){
        
		StartCoroutine( GetDialog(scenarioLabel) );
	}
 
    IEnumerator GetDialog(string scenarioLabel)
    {
        Debug.Log("関数実行");
        //メイン画面のアンロード
        StatusDataStore.Instance.data.viewMode = "DialogView";
        //「宴」のシナリオを呼び出す
        Engine.JumpScenario( scenarioLabel );
        //LoadSceneController.UnLoadScene("MainView");
 
        //「宴」のシナリオ終了待ち
        while(!Engine.IsEndScenario)
        {
            yield return 0;
        }
		/*
        if(StatusDataStore.Instance.data.viewMode == "DialogView")
        {
            // 会話シーン ズーム用のvirtual camera vc2
            Cinemachine.CinemachineVirtualCamera vc2 = GameObject.Find("CM vcam2").GetComponent<Cinemachine.CinemachineVirtualCamera>();
            // コンポーネントを無効にする
            vc2.enabled = false;

            //なおし画面に入らなかったら、会話終了の段階でメイン画面を読み込む
            StatusDataStore.Instance.data.viewMode = "MainView";
        }   
		 */     

  //      bool isLoaded = false;
		//for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount ; i++) {
		//	//読み込まれているシーンを取得し、
		//	//これから生成しようとしているシーンが
		//	//すでに生成されているシーンかどうかを判定する
		//	string ExistSceneName = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name; 

		//	//重複したシーンを読み込まないようにする
		//	if("CreateView" == ExistSceneName || "DialogView" == ExistSceneName){
  //              isLoaded = true;
		//	}
  //      }

  //      if (!isLoaded)
  //      {
  //          LoadSceneController.LoadSceneObjects("MainView");
  //      }

    }

	// Use this for initialization
	void Start () {
		//QuestDataStore.Instance.data.DialogControllerObj = this.gameObject;
		GetQuestDialog(StatusDataStore.Instance.data.scenarioLabel);		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
 

}
