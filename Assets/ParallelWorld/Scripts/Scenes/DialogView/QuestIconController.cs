﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utage;
using ParallelWorld.DBModel;

public class QuestIconController : ModuleQuest
{

    [Header("QuestIconControllerSetting"), SerializeField]
    public GameObject Player;   //プレイヤー
    [SerializeField]
    public List<string> questIds;   //クエスト一覧
    public string questObjId;   //クエスト対象物か判定するためのid
    private string scenarioLabel;
    private string charactorName;

    //内部判定用関数
    private string playerRank,storyNo;
    private int chkPlayerRank,chkStoryNo;
    private GameObject DialogControllerObj;

    private const float INVAILD_BASE_SCALE = float.MinValue;
    /// <summary>
    /// カメラからの距離が1のときのスケール値
    /// </summary>
    [SerializeField]
    private float _baseScale = INVAILD_BASE_SCALE;

    // Use this for initialization
    void Start()
    {
        if (_baseScale != INVAILD_BASE_SCALE) return;
        // カメラからの距離が1のときのスケール値を算出
        _baseScale = transform.localScale.x / GetDistance();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(Player.transform);
    }

    private void LateUpdate()
    {
        //transform.localScale = Vector3.one * _baseScale * GetDistance();
    }

    // クエストアイコンをタップした時
    public void OnClickCharactorIcon()
    {
        //タッチされたオブジェクトの情報を格納
        TappedObjectInfoDataStore.Instance.data.objName = this.name;
        TappedObjectInfoDataStore.Instance.data.objModel = this.gameObject;
        TappedObjectInfoDataStore.Instance.data.listQuestId = questIds;
        TappedObjectInfoDataStore.Instance.data.questObjId = questObjId;

        //メイン画面のアンロード処理を待つ
        StartCoroutine(PlayCloseAnim());
                
    }

    void SetScenarioLabel()
    {
        /*
        //TODO:カメラアングルの調整
        // 会話シーン ズーム用のvirtual camera vc2 （クエストキャラとプレイヤー）の移動点を取得する
        Vector3 qcpos = this.gameObject.transform.position;
        Vector3 pcpos = GameObject.Find("Avatar").transform.position;

        // camera vc2 の look at に設定しているオブジェクトの位置を2キャラの中間点に更新する
        GameObject.Find("CenterPos").transform.position = new Vector3((qcpos.x + pcpos.x) / 2, (qcpos.y + pcpos.y) / 2, (qcpos.z + pcpos.z) / 2);

        // 会話シーン ズーム用のvirtual camera vc2
        Cinemachine.CinemachineVirtualCamera vc2 = GameObject.Find("CM vcam2").GetComponent<Cinemachine.CinemachineVirtualCamera>();
        // コンポーネントを有効にする（vc1より Priority を高く設定しているため、有効にすれば vc2 が優先される）
        vc2.enabled = true;

        //DialogControllerObj = QuestDataStore.Instance.data.DialogControllerObj;
         */
        charactorName = this.transform.parent.gameObject.name;



        //【Phase01】meetCharacterの対象キャラかどうかを判定
        Dictionary<string, string[]> dicMeetCharacter = PlayerDataStore.Instance.data.GetDicMeetCharacter();
        if(dicMeetCharacter.ContainsKey(charactorName)){
            //現在進行中のクエストが存在し、話しかけたキャラがクエスト対象だった場合
            //クエスト進捗を更新し、シナリオラベルを発行する

            if(dicMeetCharacter[charactorName].Contains(questObjId)){
                //まだ会っていないキャラクターであれば
                //クエストIDをplayerDataから削除し、
                //会った時のシナリオラベルを発行
                //(配列の状態だと削除できないため、一旦リストに変換して削除している)
                var listIds = new List<string>();
                listIds.AddRange(dicMeetCharacter[charactorName]);

                listIds.Remove(questObjId);

                string[] aryMeetCharacterIds;
                aryMeetCharacterIds = listIds.ToArray();

                Dictionary<string, int> dicMeetCharacterSortNumber = PlayerDataStore.Instance.data.GetDicMeetCharacterSortNumber();

                //クエスト進捗を更新
                PlayerDataStore.Instance.data.meetCharacter[dicMeetCharacterSortNumber[charactorName]].id = aryMeetCharacterIds;

                //シナリオラベルを発行
                //scenarioLabel = charactorName + "-metCharacter-" + questObjId;
                scenarioLabel = "metCharacter-" + questObjId;
                StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
                StatusDataStore.Instance.data.viewMode = "DialogView";

                Debug.Log("metChara!");

            }else{
                //すでに会っているキャラクターであれば、通常会話を、storyの段階に応じて出し分け
                //scenarioLabel = charactorName + "-metCharacter-normal-" + PlayerDataStore.Instance.data.storyNo;
                scenarioLabel = "metCharacter-normal-" + PlayerDataStore.Instance.data.storyNo;
                StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
                StatusDataStore.Instance.data.viewMode = "DialogView";

                Debug.Log("metChara!");
            }
        }
        

        //【Phase02】クエストID保持キャラかどうかを判定
        //クエストIDと現在のPlayerDBとの照合を行い、
        //登録状況・達成状況に応じて、シナリオラベルを発行

        Debug.Log("questObjId : " + questObjId);

        scenarioLabel = CheckQuestProgress(questObjId);
        StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
        StatusDataStore.Instance.data.viewMode = "DialogView";

    }

    /// <summary>
    /// アイコンとカメラの距離を取得
    /// </summary>
    /// <returns></returns>
    private float GetDistance()
    {
        return (transform.position - Player.transform.position).magnitude;
    }

    /// <summary>
    /// メイン画面のアンロード完了を待つコルーチン
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlayCloseAnim()
    {
        MainViewController.CloseAnim();

        // メイン画面がアニメーション後にアンロードされるまで待つ
        while (!MainViewController.isClosed)
        {
            yield return new WaitForEndOfFrame();
        }

        // シナリオラベルを発行する
        SetScenarioLabel();

        MainViewController.isClosed = false;
    }

    private string CheckQuestProgress(string id){
        //【Phase02】クエストID保持キャラかどうかを判定
        //クエストIDと現在のPlayerDBとの照合を行い、
        //登録状況・達成状況に応じて、シナリオラベルを発行

        //questIdがセットされているか否か
        if (id != "")
        {
            //idが空でない場合（クエストキャラである場合）
            //クエストIDをセット（なおしクエスト読み込みに使用する）
            QuestDataStore.Instance.data.questId = id;

            //同じIDのクエストが登録されているかどうかを確認
            if (PlayerDataStore.Instance.GetQuestIds().Contains(id))
            {
                Debug.Log("have questId in PlayerDB");
                //すでにクエストIDがplayerDBにある場合
                //questProgress(クリアしているか否か)の確認
                if (PlayerDataStore.Instance.GetDicQuest()[id])
                {
                    //すでにクリア済みの場合
                    //完了のシナリオラベルを発行
                    scenarioLabel = "completed-" + id;
                    //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
                    Debug.Log("questCompleted");
                    return scenarioLabel;
                }
                else
                {
                    //未クリア
                    //クリア条件を満たしているか否かの判定
                    if (searchQuestId(id))
                    {
                        //クエスト達成のシナリオラベルを発行
                        //scenarioLabel = charactorName + "-complete-" + id;
                        scenarioLabel = "complete-" + id;
                        //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
                        Debug.Log("questComplete");
                        return scenarioLabel;
                    }
                    else
                    {
                        //クリア条件を満たしていない場合 or 該当クエストがなおしクエストであった場合
                        //クエスト未達成のシナリオラベルを発行
                        scenarioLabel = "doing-" + id;
                        //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
                        Debug.Log("questNotComplete or naoshiQuestStart");
                        return scenarioLabel;
                    }
                }
            }
            else
            {
                //クエストが未だ存在しない場合、
                //クエストを引き受けられる状態かどうかを判定

                if(CheckCertification(id)){
                    //クエストを引き受けられる場合

                    AddQuestToPlayerDB(id);

                    scenarioLabel = "start-" + id;
                    StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
                    StatusDataStore.Instance.data.viewMode = "DialogView";
                    //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
                    Debug.Log("クエストが追加されました");
                    return scenarioLabel;

                }else{
                    //クエストを引き受けられない場合
                    scenarioLabel = "cannotstart-" + id;
                    StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
                    StatusDataStore.Instance.data.viewMode = "DialogView";
                    //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
                    Debug.Log("クエストを開始できるレベル・ストーリー進行状況にありません");
                    return scenarioLabel;

                }
            }
        }
        else
        {
            //クエストIdがキャラクターに付属されてない（モブキャラである）場合
            //主人公のランクとストーリーの進捗によって、発行するシナリオラベルを変える
            //シナリオラベル：「キャラクター名-playerRank-storyNo」
            string playerRank = PlayerDataStore.Instance.data.playerRank.ToString();
            string storyNo = PlayerDataStore.Instance.data.storyNo.ToString();

            scenarioLabel = charactorName + "-" + playerRank + "-" + storyNo;
            StatusDataStore.Instance.data.scenarioLabel = scenarioLabel;
            StatusDataStore.Instance.data.viewMode = "DialogView";
            //DialogControllerObj.GetComponent<DialogViewController>().GetQuestDialog(scenarioLabel);
            Debug.Log("not found questIds");
            return scenarioLabel;
        }        
    }

    private bool CheckCertification(string id){
        //主人公のランクとストーリーの進捗から
        //クエストを引き受けられるかどうかを確認
        
        bool flgCertification = true;
        //チェック用の関数セット
		if(QuestDataStore.Instance.data.mainQuestList.getIds().Contains(id)){
			Debug.Log("subQuestです");
			chkPlayerRank = QuestDataStore.Instance.data.mainQuestList.getQuestInfo()[questObjId].playerRank;
            chkStoryNo = QuestDataStore.Instance.data.mainQuestList.getQuestInfo()[questObjId].storyNo;
		}else if(QuestDataStore.Instance.data.subQuestList.getIds().Contains(id)){
			Debug.Log("subQuestです");
			chkPlayerRank = QuestDataStore.Instance.data.subQuestList.getQuestInfo()[questObjId].playerRank;
            chkStoryNo = QuestDataStore.Instance.data.subQuestList.getQuestInfo()[questObjId].storyNo;
		}else{
			//どちらのクエストリストにも登録されていなかった時
			Debug.Log("不正なクエストIDです");
            return false;
		}
        //主人公のランクを判定
        if(chkPlayerRank > PlayerDataStore.Instance.data.playerRank){
            flgCertification = false;
        }

        //ストーリーの進捗
        if(chkStoryNo > PlayerDataStore.Instance.data.storyNo){
            flgCertification = false;
        }
        return flgCertification;

    }

	public void AddQuestToPlayerDB(string questId){
			//新しいクエストの登録
			QuestInventory newQuest = new QuestInventory();
			newQuest.id = questId;
			newQuest.progress = false;

			//要素の追加
            Array.Resize(ref PlayerDataStore.Instance.data.quests, PlayerDataStore.Instance.data.quests.Length + 1);
            PlayerDataStore.Instance.data.quests[PlayerDataStore.Instance.data.quests.Length  - 1] = newQuest;
	}
}
