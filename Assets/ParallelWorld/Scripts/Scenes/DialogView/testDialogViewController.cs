﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testDialogViewController : MonoBehaviour {
	
	public Text DialogText;
	public Text ButtonText;
	public Button SceneButton;
    public Action onClickButtonAction = () => { };

	// Use this for initialization
	void Start () {
		/*		if(QuestDataStore.Instance.data.progress < 100){
			DialogText.text = "はよなおさんかい";
			ButtonText.text = "なおす";
			onClickButtonAction = () => 
			{
				LoadSceneController.LoadSceneObjects("CreateView");
			};
			
		}else{
			DialogText.text = "やるじゃないの...";
			ButtonText.text = "もどる";
			onClickButtonAction = () => 
			{
				LoadSceneController.UnLoadScene("DialogView");
			};
		}
		 */

		
		SceneButton.onClick.AddListener( OnClickButton );
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnClickButton(){
		onClickButtonAction();
	}
}
