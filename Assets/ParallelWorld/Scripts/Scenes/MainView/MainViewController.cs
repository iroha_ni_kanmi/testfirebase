﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class MainViewController : MonoBehaviour {

	[Header("ButtonSetting"), SerializeField]
	public GameObject AreaViewButton;
	public GameObject QuestViewButton;
	public GameObject InfoViewButton;
	public GameObject ItemViewButton;
	public GameObject SettingViewButton;
	public GameObject StoryViewButton;
	public GameObject RankViewButton;
	
	[Header("SlideSetting"), SerializeField]
	public GameObject SlideArea;
	public GameObject OpenMenuButton;
	public GameObject CloseMenuButton;
	public AnimationCurve animCurve = AnimationCurve.Linear(0, 0, 1, 1);
	public float duration = 0.5f;
	public Vector3 OutPosition;

	[Header("MenuSetting"), SerializeField]
	private bool MenuFlg;
	public float menuPosY,menuHeight;

    private static Animator anim;

    public static bool isClosed;

	[Header("CheckMarkSetting"), SerializeField]
	public GameObject playerChkQuest;
	public GameObject playerChkItem;
	public GameObject playerChkInfo;

	[Header("InfoSetting"), SerializeField]
	public GameObject InfoButton;


    // Use this for initialization
    void Start () {
		//Sceneを読み込むButton一式へ、OnClickLoadSceneButtonを設定
		AreaViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(AreaViewButton) );
		QuestViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(QuestViewButton) );
		InfoViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(InfoViewButton) );
		ItemViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(ItemViewButton) );
		SettingViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(SettingViewButton) );
		StoryViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(StoryViewButton) );
		RankViewButton.GetComponent<Button>().onClick.AddListener( () => OnClickLoadSceneButton(RankViewButton) );
		
		//Menu開閉処理設定
		// OpenMenuButton.GetComponent<Button>().onClick.AddListener( OnClickMenuButton );
		// CloseMenuButton.GetComponent<Button>().onClick.AddListener( OnClickMenuButton );
		GetSlideAnimationParam(SlideArea, animCurve, OutPosition, duration);		
		MenuFlg = false;

        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
        isClosed = false;

		//詳細未読マークの表示

		//=================クエストの判定=================
		//未読のクエストがあるかどうか
		if(!PlayerDataStore.Instance.data.chkPlayerCheckedQuest()){
			//クエストアイコンの上に未読バッチを表示させる
			playerChkQuest.SetActive(true);
		}
		//状態の変化に応じて、変えていく
		PlayerDataStore.Instance.data.quests.ObserveEveryValueChanged( _ => _.Length)
			.Subscribe (flg => {
				if(!PlayerDataStore.Instance.data.chkPlayerCheckedQuest()){
					//クエストアイコンの上に未読バッチを表示させる
					playerChkQuest.SetActive(true);
				}else{
					playerChkQuest.SetActive(false);
				}
			});
		//Debug.Log("クエスト");
		//Debug.Log(PlayerDataStore.Instance.data.chkPlayerCheckedQuest());


		//=================アイテムの判定=================
		//未読のアイテムがあるかどうか
		if(!PlayerDataStore.Instance.data.chkPlayerCheckedItem()){
			//持ち物アイコンの上に未読バッチを表示させる
			playerChkItem.SetActive(true);			
		}
		//状態の変化に応じて、変えていく
		PlayerDataStore.Instance.data.items.ObserveEveryValueChanged( _ => _.Length)
			.Subscribe (flg => {
				if(!PlayerDataStore.Instance.data.chkPlayerCheckedItem()){
					//持ち物アイコンの上に未読バッチを表示させる
					playerChkItem.SetActive(true);
				}else{
					playerChkItem.SetActive(false);
				}
			});
		//Debug.Log("アイテム");
		//Debug.Log(PlayerDataStore.Instance.data.chkPlayerCheckedItem());


		//=================お知らせの判定=================
		//未読のお便りがあるかどうか
		if(!PlayerDataStore.Instance.data.chkPlayerCheckedQuest()){
			//クエストアイコンの上に未読バッチを表示させる
			playerChkInfo.SetActive(true);
		}else{
			playerChkInfo.SetActive(false);
		}

		//状態の変化に応じて、変えていく
		PlayerDataStore.Instance.data.quests.ObserveEveryValueChanged( _ => _.Length)
			.Subscribe (flg => {
				if(!PlayerDataStore.Instance.data.chkPlayerCheckedLetter()){
					//クエストアイコンの上に未読バッチを表示させる
					playerChkQuest.SetActive(true);
				}else{
					playerChkQuest.SetActive(false);
				}
			});

		//未読のお知らせがあるかどうか
		if(StatusDataStore.Instance.data.getNews){
			playerChkInfo.SetActive(true);
			InfoButton.name = "InfoViewButton";
		}else{
			InfoButton.name = "LetterViewButton";
		}
    }
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnClickLoadSceneButton(GameObject ButtonObj){
		//LoadSceneController.LoadSceneObjects(ButtonObj.name);
		CloseAnim();
		StatusDataStore.Instance.data.viewMode = ButtonObj.name;
		if(ButtonObj.name.Contains("Info") || ButtonObj.name.Contains("Item") || ButtonObj.name.Contains("Setting")){
            //SlideArea.GetComponent<CanvasSlider>().SlideOut();
        }
	}

	void OnClickMenuButton(){
		
		//Menu開閉処理
		TapMenu();
		
		if(!MenuFlg){
			MenuFlg = true;
		}else{
			MenuFlg = false;
		}
	}

	
	private void GetSlideAnimationParam(GameObject ViewObject, AnimationCurve animCurve, Vector3 OutPosition, float duration){
		if(ViewObject.GetComponent<CanvasSlider>() == null){
			ViewObject.AddComponent<CanvasSlider>();
		}
		ViewObject.GetComponent<CanvasSlider>().animCurve = animCurve;
		ViewObject.GetComponent<CanvasSlider>().outPosition = OutPosition;
		ViewObject.GetComponent<CanvasSlider>().duration = duration;
	}
	
	private void TapMenu(){
		StartCoroutine (GetButtonMenu(MenuFlg,OpenMenuButton,SlideArea,menuPosY,menuHeight)); 
	}
	
	private IEnumerator GetButtonMenu(bool MenuFlg,GameObject OpenMenuButton,GameObject SlideArea,float menuPosY,float menuHeight){

        float startTime = Time.time;    // 開始時間
        
        //SlideAreaの定義
        float startHeight = SlideArea.GetComponent<RectTransform>().rect.height;
        float startPosY = SlideArea.GetComponent<RectTransform>().localPosition.y;
        float totalHeight,totalPosY;
        
        Vector3 startScale = OpenMenuButton.transform.localScale;
        Vector3 totalScale;

		if(MenuFlg){
			//メニューを閉じる処理
			totalHeight = -menuHeight;
			totalPosY = -menuPosY;
			totalScale = new Vector3(1,1,1) - startScale;
		}else{
			//メニューを開く処理
			totalHeight = menuHeight;
			totalPosY = menuPosY;
			totalScale = new Vector3(0,0,0) - startScale;
		}
				
        while((Time.time - startTime) < duration){
            //メニューの拡大・縮小処理
            //スライドエリア
            SlideArea.GetComponent<RectTransform>().sizeDelta = 
            new Vector2(
            SlideArea.GetComponent<RectTransform>().rect.width,
            startHeight + totalHeight * animCurve.Evaluate((Time.time - startTime) / duration)
            );
            //メニューアイコン
            //OpenMenuButton.transform.localScale = startScale + totalScale * animCurve.Evaluate((Time.time - startTime) / duration);
            
                        
            //メニュー位置の移動
            SlideArea.GetComponent<RectTransform>().localPosition = 
            new Vector3(
            SlideArea.GetComponent<RectTransform>().localPosition.x,
            startPosY + totalPosY * animCurve.Evaluate((Time.time - startTime) / duration),
            SlideArea.GetComponent<RectTransform>().localPosition.z);
            
            yield return 0;        // 1フレーム後、再開
        }


        //微妙なズレを修正するための計算
        SlideArea.GetComponent<RectTransform>().sizeDelta = 
        new Vector2(
        SlideArea.GetComponent<RectTransform>().rect.width,
        startHeight + totalHeight
        );
        
        SlideArea.GetComponent<RectTransform>().localPosition = 
        new Vector3(
        SlideArea.GetComponent<RectTransform>().localPosition.x,
        startPosY + totalPosY,
        SlideArea.GetComponent<RectTransform>().localPosition.z
        );

	}

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    public static void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
        //SceneManager.UnloadSceneAsync(SceneName);
        isClosed = true;
    }
}
