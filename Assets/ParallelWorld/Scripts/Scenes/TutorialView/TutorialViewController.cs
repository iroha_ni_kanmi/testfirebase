﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
public class TutorialViewController : MonoBehaviour {

	[Header("TutorialSetting"), SerializeField]
	public Text TutorialTitleText;
	public Image TutorialImage;
	public Text ExplainText;
	[Header("TutorialPageButtonSetting"), SerializeField]
	public GameObject backButton;
	public GameObject nextButton;
	public Image nextButtonImage;
	public Sprite nextImage;
	public Sprite okImage;

	[Header("TutorialPageElementsSetting"), SerializeField]
	public Transform rootPageElements;
	public GameObject PageTogle;
	public GameObject DataSetController;

	private int pageTotal;
	private List<GameObject> listPageToggle = new List<GameObject>();
	//private int pageStatus;
	public static ReactiveProperty<int> pageStatus = new ReactiveProperty<int>(0);

    private Animator anim;

    public static bool isClosed;

	//内部判定用関数
	private Dictionary<string, TutorialList.TutorialInfo> dicTutorialInfo = new Dictionary<string, TutorialList.TutorialInfo>();
	private string tutorialId;

	private List<string> descriptions;
	private List<string> tutorialImage;
	private string pageTitle;

    // Use this for initialization
    void Awake () {
		GameObject _dataSetController = Instantiate<GameObject>( DataSetController );
	}
    void Start () {
		
		//データセット
		dicTutorialInfo = QuestDataStore.Instance.data.tutorialList.getDicTutorialInfo();
		tutorialId = StatusDataStore.Instance.data.tutorialId;
		pageTitle = dicTutorialInfo[tutorialId].tutorialName;
		descriptions = dicTutorialInfo[tutorialId].tutorialDescription;
		tutorialImage = dicTutorialInfo[tutorialId].tutorialImage;

		//ページ数を取得
		pageTotal = descriptions.Count();

		//Toggleを生成
		for(int i=0; i<pageTotal; ++i){
			GameObject PageObj = Instantiate<GameObject>( PageTogle );
        	PageObj.transform.SetParent( rootPageElements, false );
			PageObj.gameObject.SetActive( true );
			if(pageTotal != 0){
				PageObj.GetComponent<Toggle>().isOn = false;
			}
			listPageToggle.Add(PageObj);
		}

		//表示ページを初期化
		pageStatus.Value = 0;

		var childTransform = rootPageElements.transform.GetComponentsInChildren<Transform>();

		//ボタンに処理を追加する
		backButton.GetComponent<Button>().onClick.AddListener( OnClickBackButton );
		nextButton.GetComponent<Button>().onClick.AddListener( OnClickNextButton );
		nextButtonImage.sprite = nextImage;

		//ページの各項目に初期値を反映していく
		TutorialTitleText.text = pageTitle;
		ExplainText.text = descriptions[pageStatus.Value];
		TutorialImage.sprite = Resources.Load(tutorialImage[pageStatus.Value], typeof(Sprite)) as Sprite;
		backButton.SetActive(false);


		//現在ページ(pageStatus)に変化があったら、ページ各項目を更新する
		pageStatus.ObserveEveryValueChanged( _ => _.Value)
			.Subscribe (count => {
				TutorialImage.sprite = TutorialImage.sprite = Resources.Load(tutorialImage[pageStatus.Value], typeof(Sprite)) as Sprite;
				ExplainText.text = descriptions[pageStatus.Value];

				listPageToggle[pageStatus.Value].GetComponent<Toggle>().isOn = true;

				//ページが最初のページの時の処理
				if(pageStatus.Value == 0){
					backButton.SetActive(false);
				}else{
					backButton.SetActive(true);
				}
				//ページが最後のページの時の処理
				if(pageStatus.Value == pageTotal-1){
					nextButtonImage.sprite = okImage;		
				}else{
					nextButtonImage.sprite = nextImage;
				}
			});

        // アニメーターを取得、トリガーをOpenにする
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
        isClosed = false;
    }

    // Update is called once per frame
    void Update () {
		
	}

	void OnClickBackButton(){
		if(pageStatus.Value != 0){
			pageStatus.Value -= 1;
		}
	}

	void OnClickNextButton(){
		if(pageStatus.Value < pageTotal-1){
			pageStatus.Value += 1;
		}else if(pageStatus.Value == pageTotal-1){
            // チュートリアル画面のアンロード処理を待つ
            StartCoroutine(PlayCloseAnim());

        }
	}

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {        
        //SceneManager.UnloadSceneAsync(SceneName);
        isClosed = true;
    }

    /// <summary>
    /// チュートリアル画面のアンロード完了を待つコルーチン
    /// </summary>
    /// <returns></returns>
    private IEnumerator PlayCloseAnim()
    {
        CloseAnim();

        // チュートリアル画面がアニメーション後にアンロードされるまで待つ
        while (!TutorialViewController.isClosed)
        {
            yield return new WaitForEndOfFrame();
        }

        // メイン画面の呼び出し
        StatusDataStore.Instance.data.viewMode = "MainView";

        TutorialViewController.isClosed = false;
    }
}
