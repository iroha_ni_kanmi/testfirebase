﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GoShared;

public class AreaController : MonoBehaviour {

	[Header("ButtonSetting"), SerializeField]
	public Button areaAButton;
	public Button areaBButton;
	public Button areaCButton;

	[Header("CoordinatesSetting"), SerializeField]
	public Coordinates AreaA;
	public Coordinates AreaB;
	public Coordinates AreaC;

	public static LocationManager locationManager;

	void Awake() {
		Coordinates AreaA = CoordinatesDataStore.Instance.data.AreaA;
		Coordinates AreaB = CoordinatesDataStore.Instance.data.AreaB;
		Coordinates AreaC   = CoordinatesDataStore.Instance.data.AreaC;
		locationManager = CoordinatesDataStore.Instance.data.locationManager;		
	}

	// Use this for initialization
	void Start () {
		areaAButton.onClick.AddListener( OnClickareaAButton );
		areaBButton.onClick.AddListener( OnClickareaBButton );
		areaCButton.onClick.AddListener( OnClickareaCButton );
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnClickareaAButton() {
		locationManager.SetLocation( AreaA );
		StatusDataStore.Instance.data.viewMode = "MainView";
		//LoadSceneController.UnLoadScene("AreaView");
	}

	public void OnClickareaBButton() {
		locationManager.SetLocation( AreaB );
		StatusDataStore.Instance.data.viewMode = "MainView";
		//LoadSceneController.UnLoadScene("AreaView");
	}

	public void OnClickareaCButton() {
		locationManager.SetLocation( AreaC );
		StatusDataStore.Instance.data.viewMode = "MainView";
		//LoadSceneController.UnLoadScene("AreaView");
	}
}
