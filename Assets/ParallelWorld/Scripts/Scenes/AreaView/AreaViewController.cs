﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class AreaViewController : MonoBehaviour {
	
	[Header("ButtonSetting"), SerializeField]
	Button CloseButton;
	public string SceneName;
    private Animator anim;

    void Awake () {
        CloseButton.onClick.AddListener(() => CloseAnim());
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
    }
	
	// Use this for initialization
	void Start () {

    }

	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
        StatusDataStore.Instance.data.viewMode = "MainView";
        //SceneManager.UnloadSceneAsync(SceneName);
    }
}
