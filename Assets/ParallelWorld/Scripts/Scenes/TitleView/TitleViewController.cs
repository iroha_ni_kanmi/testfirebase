﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using GoMap;

public class TitleViewController : MonoBehaviour {

	public List<string> DefaultLoadScene;
	public GameObject ButtonObj;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TapStartButton (){
		CanvasFader.Begin (target:ButtonObj, isFadeOut:true, duration:0.2f, ignoreTimeScale:true, onFinished:OnFinished);
        
	}

	private void OnFinished(){

		/*
		//デフォルトで読み込むSceneを読み込み
		string[] ExceptScenes = DefaultLoadScene.ToArray();
		for(int i = 0; i < DefaultLoadScene.Count(); i++){
			LoadSceneController.LoadSceneObjects(DefaultLoadScene[i]);
		}
		*/
		//LoadSceneController.UnLoadScene("TitleView");
		StatusDataStore.Instance.data.viewMode = "MainView";

	}

}
