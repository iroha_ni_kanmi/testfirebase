﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class RankController : MonoBehaviour
{
	[Header("RankViewSetting"), SerializeField]
    private Transform RankDetailViewArea;
    [SerializeField]
	public Transform AllRankViewArea;
    [SerializeField]
	public GameObject OriginalBadgeButton;
    [SerializeField]
	public GameObject HideBadgeButton;

	//内部判定用関数
    private List<int> listRankNoes = new List<int>();
	private Dictionary<int, rankList.rankInfo> dicRankInfo = new Dictionary<int, rankList.rankInfo>();
    private int playerRank, selectNumber;

    // Start is called before the first frame update
    void Start()
    {
        //データセット
        //PlayerDataからセット
        playerRank = PlayerDataStore.Instance.data.playerRank;

        //ScriptableObjectからセット
        listRankNoes = QuestDataStore.Instance.data.rankList.getRankNoes();
        dicRankInfo = QuestDataStore.Instance.data.rankList.getRankInfo();

        //現在選択中のバッジに合わせ、背景を帰る
        selectNumber = playerRank;

        RenderRankInfo(listRankNoes);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void RenderRankInfo(List<int> listRankNoes){
		//バッジ表示欄を全てクリア
		foreach ( Transform obj in AllRankViewArea ){
			GameObject.Destroy(obj.gameObject);
		}

        //バッジを追加していく
        //ランクのバッジ一覧を表示

        //獲得バッジの状況に応じて、バッジを表示するか、非表示にするかを決める
        for(int i=0; i<listRankNoes.Count(); ++i){
            
            if(playerRank >= dicRankInfo[listRankNoes[i]].rankNo){
                //バッジを表示した状態でセットしていく
            	//インスタンスを生成
	            GameObject BadgeButtonObj = Instantiate<GameObject>( OriginalBadgeButton );
                // オブジェクトを表示
                BadgeButtonObj.gameObject.SetActive( true );
                // 親子関係にする
                BadgeButtonObj.transform.SetParent( AllRankViewArea, false );
                //子要素を取得
                //バッジの名前を変更
                var childTransform = BadgeButtonObj.GetComponentsInChildren<Transform>();
                foreach (Transform child in childTransform){
                    if(child.name == "badgeNameText"){
                        child.GetComponent<Text>().text = dicRankInfo[listRankNoes[i]].rankName;
                    }else if(child.name == "badgeButtonImage"){
                        child.GetComponent<Image>().sprite = Resources.Load(dicRankInfo[listRankNoes[i]].badgeFilePath, typeof(Sprite)) as Sprite;
                    }else if(child.name == "NowLevelImage"){
                        if(playerRank == dicRankInfo[listRankNoes[i]].rankNo){
                            child.gameObject.SetActive(true);
                        }else{
                            child.gameObject.SetActive(false);
                        }
                    }
                    
                }
	        	//名前をidにする
    		    BadgeButtonObj.gameObject.name = dicRankInfo[listRankNoes[i]].rankNo.ToString();
                //ボタンに処理を付与する
    		    BadgeButtonObj.GetComponent<Button>().onClick.AddListener( () => GetRankView(int.Parse(BadgeButtonObj.gameObject.name)) );

            }else{
                //バッジを隠した状態でセットしていく
            	//インスタンスを生成
	            GameObject BadgeButtonObj = Instantiate<GameObject>( HideBadgeButton );
                // オブジェクトを表示
                BadgeButtonObj.gameObject.SetActive( true );
                // 親子関係にする
                BadgeButtonObj.transform.SetParent( AllRankViewArea, false );
                //子要素を取得
                var childTransform = BadgeButtonObj.GetComponentsInChildren<Transform>();
	        	//名前をidにする
    		    BadgeButtonObj.gameObject.name = dicRankInfo[listRankNoes[i]].rankNo.ToString();
                //ボタンに処理を付与する
    		    BadgeButtonObj.GetComponent<Button>().onClick.AddListener( () => HideRankView(int.Parse(BadgeButtonObj.gameObject.name)) );
            }
            //プレイヤーランクと等しいバッジであった場合、背景を選択状態にする
            //詳細は、現在のプレイヤーのサンクポイント獲得状況に応じて、
            //最新のバッジを表示させる
            GetRankView(playerRank);

        }

    }

    void GetRankView(int rankNo){
        //子要素を取得
        var childTransform = RankDetailViewArea.GetComponentsInChildren<Transform>();
        foreach (Transform child in childTransform){

            //詳細画面の処理
            if(child.name == "RankNameText"){
                child.GetComponent<Text>().text = dicRankInfo[rankNo].rankName;
            }else if(child.name == "RankDescriptionText"){
                child.GetComponent<Text>().text = dicRankInfo[rankNo].rankDescription;
            }else if(child.name == "BadgeImageMask"){
                child.GetComponent<Image>().color = new Color(0,0,0,0);
            }else if(child.name == "BadgeImage"){
                child.GetComponent<Image>().sprite = Resources.Load(dicRankInfo[rankNo].badgeFilePath, typeof(Sprite)) as Sprite;
            }else if(child.name == "HowToGetText"){
                child.GetComponent<Text>().text = dicRankInfo[rankNo].needThankPoints.ToString() + "pt";
    	    }
        }

        SetButtonSelected(rankNo);

    }

    void HideRankView(int rankNo){
        //子要素を取得
        var childTransform = RankDetailViewArea.GetComponentsInChildren<Transform>();
        foreach (Transform child in childTransform){

            //詳細画面の処理
            if(child.name == "RankNameText"){
                child.GetComponent<Text>().text = "???";
            }else if(child.name == "RankDescriptionText"){
                child.GetComponent<Text>().text = "???";
            }else if(child.name == "BadgeImageMask"){
                child.GetComponent<Image>().color = new Color(255,255,255,255);
            }else if(child.name == "HowToGetText"){
                child.GetComponent<Text>().text = dicRankInfo[rankNo].needThankPoints.ToString() + "pt";
    	    }
        }

        SetButtonSelected(rankNo);

    }

    void SetButtonSelected(int rankNo){
        foreach (Transform child in AllRankViewArea){
            if(dicRankInfo.ContainsKey(int.Parse(child.name))){
                if(int.Parse(child.name) == rankNo){
                    child.GetComponent<Image>().color = new Color(255,255,255,255);
                }else{
                    child.GetComponent<Image>().color = new Color(0,0,0,0);
                }
            }
        }
    }
}
