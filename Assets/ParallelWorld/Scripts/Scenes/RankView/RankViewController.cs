﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class RankViewController : MonoBehaviour
{
	[Header("ButtonSetting"), SerializeField]
	Button CloseButton;
	[SerializeField]
	Button TrophyViewButton;
//	public string SceneName;
//  private Animator anim;
    
    //TODO:画面デザインが決まったら、アニメーションの作成

    // Start is called before the first frame update
    void Start()
    {
        /*
        CloseButton.onClick.AddListener(() => CloseAnim());
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
         */
        CloseButton.onClick.AddListener(() => CloseScene(""));
        TrophyViewButton.onClick.AddListener(() => LoadScene("TrophyView"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
/*
    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }
 */
    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
		StatusDataStore.Instance.data.viewMode = "MainView";
        //SceneManager.UnloadSceneAsync(SceneName);
    }

    void LoadScene(string SceneName)
    {
		StatusDataStore.Instance.data.viewMode = SceneName;
        //SceneManager.UnloadSceneAsync(SceneName);
    }
}
