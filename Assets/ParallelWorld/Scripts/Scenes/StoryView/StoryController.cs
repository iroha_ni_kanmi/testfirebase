﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class StoryController : MonoBehaviour
{
    [Header("StoryViewSetting"), SerializeField]
    private Text TitleText;
	[SerializeField]
    private Text DescriptionText;
	[SerializeField]
    private Image StoryImage;
    [SerializeField]
    private GameObject NowHereMark;

    [Header("ButtonSetting"), SerializeField]
    private GameObject ReturnStoryButton;
	[SerializeField]
    private GameObject NextStoryButton;    

    //内部判定用関数
    private Dictionary<int, storyList.storyInfo> dicStoryInfo = new Dictionary<int, storyList.storyInfo>();
    private int playerStoryNo;
    private storyList storyList;

    // Start is called before the first frame update
    void Start()
    {   
        //データセット
        storyList = QuestDataStore.Instance.data.storyList;

        dicStoryInfo = storyList.getDicStoryInfo();
        //TODO:2ではなく、PlayerDataStoreの値と紐づける
        playerStoryNo = PlayerDataStore.Instance.data.storyNo;

        //ストーリーを描画
        RenderStoryView(playerStoryNo);

        ReturnStoryButton.GetComponent<Button>().onClick.AddListener( () => OnClickReturnStoryButton() );
        NextStoryButton.GetComponent<Button>().onClick.AddListener( () => OnClickNextStoryButton() );
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnClickNextStoryButton(){
        //次のストーリーのあらすじを描画
        playerStoryNo += 1;

        //ストーリーを描画
        RenderStoryView(playerStoryNo);
    }

    void OnClickReturnStoryButton(){
        //次のストーリーのあらすじを描画
        playerStoryNo -= 1;

        //ストーリーを描画
        RenderStoryView(playerStoryNo);
    }

    void RenderStoryView(int storyNo){

        //StoryViewの描画処理
        //画像とタイトル、説明文を更新する
        if(dicStoryInfo.ContainsKey(storyNo)){
            TitleText.text = dicStoryInfo[storyNo].storyName;
            DescriptionText.text = dicStoryInfo[storyNo].storyDescription;
            StoryImage.sprite = Resources.Load(dicStoryInfo[storyNo].storyImageFilePath, typeof(Sprite)) as Sprite;
        }
        //ボタンを表示するか非表示にするかをチェック
        CheckActiveOrNot(playerStoryNo);

        //イマココ画像を表示するかをチェック
        CheckNowStoryNo(playerStoryNo);
    }

    void CheckActiveOrNot(int storyNo){
        //1を足した時、プレイヤーのランクを上回るなら、ボタンを非表示にする
        if((storyNo+1) > PlayerDataStore.Instance.data.storyNo){
            NextStoryButton.SetActive(false);
        }else{
            NextStoryButton.SetActive(true);
        }
        //1を引いた時、0を下回るなら、ボタンを非表示にする
        if((storyNo-1) < 0){
            ReturnStoryButton.SetActive(false);
        }else{
            ReturnStoryButton.SetActive(true);
        }
    }

    void CheckNowStoryNo(int storyNo){
        //プレイヤーのランクであれば、イマココ画像を表示
        if(storyNo == PlayerDataStore.Instance.data.storyNo){
            NowHereMark.SetActive(true);
        }else{
            NowHereMark.SetActive(false);
        }
    }


}
