﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Models;
using UnityEngine.SceneManagement;

public class StoryViewController : MonoBehaviour {
    /*    

	[Header("StoryViewSetting"), SerializeField]
    public Text title;

    public Text description;

    public GameObject listPanel;

    public GameObject StoryListItemPrefab;
     */


	[Header("LoadSceneSetting"), SerializeField]
	Button CloseButton;

	public string SceneName;

    private Animator anim;

    void Awake () {
        CloseButton.onClick.AddListener(() => CloseAnim());
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
    }

	private void OnEnable() {
        Debug.Log( "We are now officially active." );
	}

    /*
    public void setStories(ArrayList stories) {
        foreach (Story story in stories) {
            if (story.active) {
                title.text = story.title;
                description.text = story.description;
            }
            else {
                GameObject storyItem = Instantiate(StoryListItemPrefab) as GameObject;
                StoryListItemController item = storyItem.GetComponent<StoryListItemController>();

                item.Title.text = story.title;
                item.Action.text = story.description;

                item.transform.SetParent(listPanel.transform, false);
            }
        }
    }
     */

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
		StatusDataStore.Instance.data.viewMode = "MainView";		
        //SceneManager.UnloadSceneAsync(SceneName);
    }

}
