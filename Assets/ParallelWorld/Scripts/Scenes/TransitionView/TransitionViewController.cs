﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class TransitionViewController : MonoBehaviour {
	
	public GameObject BGPanel;
	
	void Awake () {
		
		StatusDataStore.Instance.data.loadingStatus = true;
		
		if(StatusDataStore.Instance.data.gameStartStatus){
			//ゲーム起動時であればカットイン表示
			BGPanel.SetActive(true);
			StatusDataStore.Instance.data.gameStartStatus = false;
		}else{
			//ゲームプレイ中であれば、フェードイン表示
			CanvasFader.Begin (target:BGPanel, isFadeOut:false, duration:0.2f);
		}
		
		
		StatusDataStore.Instance.data.ObserveEveryValueChanged( _ => _.loadingStatus)
			.Subscribe (flg => {
				if(!StatusDataStore.Instance.data.loadingStatus){
			    	CanvasFader.Begin (target:BGPanel, isFadeOut:true, duration:0.2f, ignoreTimeScale:true, onFinished:OnFinished);
			    }
		        });
		
	}
	
	void Update () {
		
	}
	
	public void OnLoadTiles(){
//		Debug.Log("Complete!");
//		Debug.Log(Math.Pow(2*gomap.tileBuffer+1, 2));
	}
	private void OnFinished(){
		LoadSceneController.UnLoadScene("TransitionView");
	}
}
