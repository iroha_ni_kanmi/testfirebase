﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class QuestItemController : MonoBehaviour {

	[Header("QuestListSetting"), SerializeField]
	public RectTransform QuestList;
	public Transform rootContent;
	public GameObject OriginalQuestObj;
	public GameObject QuestDetailDialog;

	[Header("ItemDataSetting"), SerializeField]
	private QuestModelDataBase questDataBase;
	public ToggleGroup QuestGenre;

	//内部判定用関数
	private Dictionary<string, MainQuestList.QuestInfo> dicMainQuestInfo = new Dictionary<string, MainQuestList.QuestInfo>();
    private Dictionary<string, SubQuestList.QuestInfo> dicSubQuestInfo = new Dictionary<string, SubQuestList.QuestInfo>();


	// Use this for initialization
	void Start () {

		//データのセット
        dicMainQuestInfo = QuestDataStore.Instance.data.mainQuestList.getQuestInfo();
        dicSubQuestInfo = QuestDataStore.Instance.data.subQuestList.getQuestInfo();


		//クエストのジャンルに応じて、作成するビューを切り替える
		string selectedGenre = Regex.Split(QuestGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
		//RenderQuestData(selectedGenre);
		RenderQuest(selectedGenre);
		
		QuestGenre.ObserveEveryValueChanged( _ => _.ActiveToggles())
			.Subscribe (text => {
				if(selectedGenre != Regex.Split(QuestGenre.ActiveToggles().First().gameObject.name, "Tag")[0]){
					selectedGenre = Regex.Split(QuestGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
					//RenderQuestData(selectedGenre);
					RenderQuest(selectedGenre);
		
				}
			});
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void RenderQuest(string selectedGenre){
		//クエストデータをメニューに描画する

		//現在進行中のクエスト一覧を取得
		QuestInventory[] ary_quests = PlayerDataStore.Instance.data.quests;
		//クエストのタイトル、説明などの情報を格納する一時的な辞書を宣言
		Dictionary<string, string> dicQuestInfoValue = new Dictionary<string, string>();
		
		//クエスト一覧をリセット
		foreach ( Transform obj in rootContent )
		{
			GameObject.Destroy(obj.gameObject);
		}


		//現在進行中のクエストを描画していく
		for(int i=0; i<ary_quests.Length; ++i){
			//選択中のアイテムのジャンルであれば、そのアイテムを描画
			switch(selectedGenre){
				case "Main":
				//メインクエストのジャンルを選択中かつ
				//メインクエスト情報にidがあれば、オブジェクトをセット
				if(dicMainQuestInfo.ContainsKey(ary_quests[i].id)){
					//内部で使うクエスト情報の値を更新
					dicQuestInfoValue["QuestTitle"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].questName;
					dicQuestInfoValue["QuestSubTitle"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].questSubName;
					dicQuestInfoValue["QuestCharaLayer"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].questIconFilePath;
					dicQuestInfoValue["ExplainText"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].description;
					dicQuestInfoValue["PurposeText"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].howToClear;
					dicQuestInfoValue["GiftText"+ary_quests[i].id] = dicMainQuestInfo[ary_quests[i].id].thankPoint.ToString();

					//クエストを描画
					SetQuestData("Main", ary_quests[i], dicQuestInfoValue, i);
				}else{
					//クエストのidがなかったらログ出力
					Debug.Log("データベースにクエストが存在しません");
					Debug.Log("Genre : MainQuest");
					Debug.Log("id : " + ary_quests[i].id);
				}
				break;

				case "Sub":
				//サブクエストのジャンルを選択中かつ
				//サブクエスト情報にidがあれば、オブジェクトをセット
				if(dicSubQuestInfo.ContainsKey(ary_quests[i].id)){
					//内部で使うクエスト情報の値を更新
					dicQuestInfoValue["QuestTitle"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].questName;
					dicQuestInfoValue["QuestSubTitle"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].questSubName;
					dicQuestInfoValue["QuestCharaLayer"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].questIconFilePath;
					dicQuestInfoValue["ExplainText"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].description;
					dicQuestInfoValue["PurposeText"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].howToClear;
					dicQuestInfoValue["GiftText"+ary_quests[i].id] = dicSubQuestInfo[ary_quests[i].id].thankPoint.ToString();

					//クエストを描画
					SetQuestData("Sub", ary_quests[i], dicQuestInfoValue, i);
				}else{
					//クエストのidがなかったらログ出力
					Debug.Log("データベースにクエストが存在しません");
					Debug.Log("Genre : SubQuest");
					Debug.Log("id : " + ary_quests[i].id);
				}
				break;
			}
		}
	}

	public void SetQuestData(string mode, QuestInventory item, Dictionary<string, string> dicQuestInfoValue, int index){

		//複製処理
    	// OriginalInfoViewから、RateBarを複製
        GameObject QuestObj = Instantiate<GameObject>( OriginalQuestObj );
    	// オブジェクトを表示
        QuestObj.gameObject.SetActive( true );
    	// 親子関係にする
        QuestObj.transform.SetParent( rootContent, false );
    	//子要素を取得
        var childTransform = QuestObj.GetComponentsInChildren<Transform>();
		//オブジェクト名をidに変更
		QuestObj.gameObject.name = item.id;
		//ボタンを押した際の挙動をadd
		Debug.Log("詳細処理");
		Debug.Log(QuestObj.gameObject.name);
		QuestObj.GetComponent<Button>().onClick.AddListener( () => GetQuestDetailView(QuestObj.gameObject.name, item, dicQuestInfoValue, index) );
		//Debug.Log("ボタン付与完了");
		//Debug.Log(item,.id);

        //クエスト名と説明、画像を更新
    	foreach (Transform child in childTransform){
        //クエスト名の更新
    	if(child.name == "QuestTitle"){
	        child.GetComponent<Text>().text = dicQuestInfoValue["QuestTitle"+item.id];
		    //サブタイトルの更新
    		}else if(child.name == "QuestSubTitle"){
	    	    child.GetComponent<Text>().text = dicQuestInfoValue["QuestSubTitle"+item.id];
			//画像の更新
    		}else if(child.name == "QuestCharaLayer"){
				Debug.Log(dicQuestInfoValue["QuestCharaLayer"+item.id]);
		        child.GetComponentsInChildren<Transform>()[1].GetComponent<Image>().sprite = Resources.Load(dicQuestInfoValue["QuestCharaLayer"+item.id], typeof(Sprite)) as Sprite;

			//クエストの進行状況の更新
    		}else if(child.name == "QuestSuccessImage"){

				if(item.progress){
					//すでにクリア済なら
					//クリアマークを出現させ
					//オブジェクトを半透明に、枠があったら消去する
					child.gameObject.SetActive(true);
					QuestObj.GetComponent<CanvasGroup>().alpha = 0.8f;

					if(QuestObj.GetComponent<Outline>() != null){
						QuestObj.GetComponent<Outline>().enabled = false;
					}

				}else{
					//未クリアなら
					//クリアマークは非表示に
					//オブジェクトを表示し、枠をつける
					child.gameObject.SetActive(false);
					QuestObj.GetComponent<CanvasGroup>().alpha = 1f;
					if(QuestObj.GetComponent<Outline>() != null){
						QuestObj.GetComponent<Outline>().enabled = true;
					}else{
						QuestObj.AddComponent<Outline>();
					}
				}
			}

			//プレイヤーがクエストをチェックしているか否か判定
			checkPlayerView(item.check, QuestObj, mode, index);
		}
	}

	public void checkPlayerView(bool check, GameObject QuestObj, string mode, int index){
		//プレイヤーがクエストの詳細を参照済か否かを確認
		//未参照の場合、アウトラインで囲い、
		//参照済の場合、アウトラインを消す

		//アウトラインのセット
		if(QuestObj.GetComponent<Outline>() == null){
			QuestObj.AddComponent<Outline>();
		}

		if(check){
			//チェック済の場合、アウトラインを非表示にする
			QuestObj.GetComponent<Outline>().enabled = false;
		}else{
			//未チェックの場合、アウトラインをつける
			QuestObj.GetComponent<Outline>().enabled = true;
			//枠の色は、Mainなら赤、Subなら青にする
			if(mode == "Main"){
				QuestObj.GetComponent<Outline>().effectColor = Color.red;
			}else if(mode == "Sub"){
				QuestObj.GetComponent<Outline>().effectColor = Color.blue;
			}

			//UniRxにて、プレイヤーデータの値を監視
			//チェックが更新されたら、アウトラインも消す
		}
		PlayerDataStore.Instance.data.quests[index].ObserveEveryValueChanged( _ => _.check)
		.Subscribe (flg => {
			if(PlayerDataStore.Instance.data.quests[index].check){
				if(QuestObj != null){
					if(QuestObj.GetComponent<Outline>() == null){
						QuestObj.AddComponent<Outline>();
					}
					QuestObj.GetComponent<Outline>().enabled = false;
				}
			}
		});


	}

	public void GetQuestDetailView(string questId, QuestInventory item, Dictionary<string, string> dicQuestDetail, int index){
		//子要素を取得

        var childTransform = QuestDetailDialog.GetComponentsInChildren<Transform>();
		foreach (Transform child in childTransform){

			if(child.name == "QuestButton"){
				var grandChildTransform = child.GetComponentsInChildren<Transform>();
				foreach (Transform grandChild in grandChildTransform){
					//クエスト名の更新
        		    if(grandChild.name == "QuestTitle"){
		        		grandChild.GetComponent<Text>().text = dicQuestDetail["QuestTitle"+item.id];
				    //説明の更新
    				}else if(grandChild.name == "QuestSubTitle"){
						grandChild.GetComponent<Text>().text = dicQuestDetail["QuestSubTitle"+item.id];
					//画像の更新
    				}else if(grandChild.name == "QuestCharaLayer"){
	    	    	grandChild.GetComponentsInChildren<Transform>()[1].GetComponent<Image>().sprite = Resources.Load(dicQuestDetail["QuestCharaLayer"+item.id], typeof(Sprite)) as Sprite;

					//クエストをクリアしているかどうかの判定で処理を分ける
					if(item.progress){
						//クリアしていたら以下の処理

						if(child.name == "PurposeTitleText"){
							//クリアしていたら目的テキストはなし
							child.GetComponent<Text>().text = "";
						}else if(child.name == "PurposeText"){
							//目的を空白に
							child.GetComponent<Text>().text = "";						
						}else if(child.name == "GiftText"){
							//報酬を空白にする
							child.gameObject.SetActive(false);
						}else if(child.name == "QuestStampClearImage"){
							//クリアのスタンプを表示
							child.GetComponent<Image>().enabled = true;
						}
					}else{
						//未クリアなら以下の処理

						if(child.name == "PurposeTitleText"){
							//未クリアなら次にやることを記載
							child.GetComponent<Text>().text = "次にやること";
						}else if(child.name == "PurposeText"){
							//やることを記載
							child.GetComponent<Text>().text = dicQuestDetail["PurposeText"+item.id];					
						}else if(child.name == "GiftText"){
							//報酬を記載
							child.gameObject.SetActive(true);
							child.GetComponent<Text>().text = "<size=50>"+ dicQuestDetail["GiftText"+item.id] + "</size> サンク\nもらえる";
						}else if(child.name == "QuestStampClearImage"){
							//クリアのスタンプを非表示にする
							child.GetComponent<Image>().enabled = false;
							}
						}
					}
				}
			}else if(child.name == "buttonClose"){
				child.GetComponent<Button>().onClick.AddListener( CloseQuestDetail );
			}
		}
		QuestDetailDialog.SetActive(true);
		if(!PlayerDataStore.Instance.data.quests[index].check){
			PlayerDataStore.Instance.data.quests[index].check = true;
		}

	}

	public void CloseQuestDetail(){
		QuestDetailDialog.SetActive(false);
	}


}
