﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkErrorController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch ( Application.internetReachability ) {

        case NetworkReachability.NotReachable:
            break;

        case NetworkReachability.ReachableViaCarrierDataNetwork:
            Debug.Log("キャリアデータネットワーク経由で到達可能");
            StatusDataStore.Instance.data.networkStatus = "ON";
            LoadSceneController.SetScenes();
            break;

        case NetworkReachability.ReachableViaLocalAreaNetwork:
            Debug.Log("Wifiまたはケーブル経由で到達可能");
            StatusDataStore.Instance.data.networkStatus = "ON";
            LoadSceneController.SetScenes();
            break;
        }       
    }
}
