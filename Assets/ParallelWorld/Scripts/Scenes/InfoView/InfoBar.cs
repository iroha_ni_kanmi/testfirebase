﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;

public class InfoBar : UIBehaviour {

	public Text InfoTitleText;		//お知らせのタイトル
	public Text InfoDigestText;	//お知らせ内容の説明文(Digest)
	public Image InfoImage;	//ヘッダー画像のパス

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
