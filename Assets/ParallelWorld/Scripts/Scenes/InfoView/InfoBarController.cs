﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Models;
using Utils;

public class InfoBarController : MonoBehaviour {

    //お知らせ情報を取得し、
    //InfoBarを複製するための関数
    private static string infoUrl = "https://gjbxefm3s7.execute-api.ap-northeast-1.amazonaws.com/pre_alpha/infos";

    private List<Info> infoList;         // Infoデータを格納するリスト
	
	public InfoBar OriginalInfoBar;	    // お知らせ情報の大本となるGameObject
	public Transform rootContent;		// InfoBarの親要素を指定
	
	public string testInfoTitleText;	// 【TEST：】お知らせのタイトル
	public string testInfoDigestText;	// 【TEST：】お知らせ内容の説明文(Digest)
	public string testInfoImagePath;	// 【TEST：】ヘッダー画像のパス
	
	public int InfoCount;			    // お知らせの数

//	public void GenerateInfoBar(){
	public void Start() {
        StartCoroutine( LoadInfoData() );
        
        //お知らせ画面を開いた時点で、チェックしたことにする
        //TODO:ここの仕様は要相談
        if(StatusDataStore.Instance.data.getNews){
            StatusDataStore.Instance.data.getNews = false;
        }
	}

    private ArrayList GetInfos() {
        return new ArrayList() {
            new Info()
        };
    }

    private IEnumerator LoadInfoData() {
        UnityWebRequest request = UnityWebRequest.Get( infoUrl );

        yield return request.SendWebRequest();

        if (request.isNetworkError) {
            Debug.Log( request.error );
        }
        else {
            if (request.responseCode == 200) {
                Debug.Log( "Received data: " + request.downloadHandler.text );
                //test============
                string jsonString = fixJson( Resources.Load<TextAsset>("DB/test/infoDB").ToString() );
                Info[] infos = JsonHelper.FromJson<Info>( jsonString );
                //ここまで================

                //テスト解除の際は、下2行のコメントアウトの処理を復活させる
                //string jsonString = fixJson( request.downloadHandler.text );
                //Info[] infos = JsonHelper.FromJson<Info>( jsonString );
                infoList = infos.ToList();

                RenderData();
            }
        }
    }

    private void RenderData() {
        // お知らせがない場合
        if (infoList == null) {
            // OriginalRateBarを変更
            InfoBar original = OriginalInfoBar.GetComponent<InfoBar>();
            // TODO( 雛形をテキストに置き換える )
            original.InfoDigestText.text = "お知らせはありません。";
            return;
        }

        // 雛形のOriginalRateBarを破棄
        OriginalInfoBar.gameObject.SetActive( false );

        // 取得したお知らせの数だけ、InfoBarを生成
        foreach (Info info in infoList) {
            // OriginalInfoViewから、RateBarを複製
            InfoBar infoBar = Instantiate<InfoBar>( OriginalInfoBar );
            // オブジェクトを表示
            infoBar.gameObject.SetActive( true );
            // 親子関係にする
            infoBar.transform.SetParent( rootContent, false );

            // タイトルの更新
            infoBar.GetComponent<InfoBar>().InfoTitleText.text = info.title;
            // 説明文(Digest)の更新
            infoBar.GetComponent<InfoBar>().InfoDigestText.text = info.digest;
            // ヘッダー画像の更新
            infoBar.GetComponent<InfoBar>().InfoImage.sprite = Resources.Load<Sprite>( "Splites/InfoImage/InfoImage01" );
            
        }
    }

    private string fixJson(string value) {
        value = "{\"Items\":" + value + "}";
        return value;
    }

}
