﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class TrophyController : MonoBehaviour
{

	[Header("TrophyViewSetting"), SerializeField]
    private Transform TrophyViewArea;
	[SerializeField]
    private RectTransform TrophyListArea;
    [SerializeField]
	public GameObject OriginalTrophyPiece;
    [SerializeField]
	public GameObject HideTrophyPiece;
	[Header("TrophyDetailViewSetting"), SerializeField]
	public GameObject TrophyDialogPanelBG;
    [SerializeField]
	public GameObject TrophyDialogPanel;

	//内部判定用関数
	private List<string> listTrophyGenre = new List<string>();
    private Dictionary<int, string> dicStoryNoAndTrophyGenre = new Dictionary<int, string>();
    private Dictionary<int, string> dicStoryNoAndTrophyGenreCode = new Dictionary<int, string>();
	private Dictionary<string, TrophyList.TrophyInfo> dicTrophyInfo = new Dictionary<string, TrophyList.TrophyInfo>();
    private List<TrophyList.TrophyInfo> listTrophyInfo = new List<TrophyList.TrophyInfo>();
    private Dictionary<string, string> dicItemInfoValue = new Dictionary<string, string>();

    private Dictionary<string, bool> dicQuestCompleteFlg = new Dictionary<string, bool>();

    // Start is called before the first frame update
    void Start()
    {
        //データセット
        //プレイヤーの進行状況をセット
        dicQuestCompleteFlg = PlayerDataStore.Instance.data.GetDicQuestCompleteFlg();

        //トロフィーを取得
        TrophyList trophyList = QuestDataStore.Instance.data.trophyList;
        int storyNo = PlayerDataStore.Instance.data.storyNo;

        //storyNoに合わせて、最初に表示するトロフィー画面を決める
        //トロフィージャンルを取得
        dicStoryNoAndTrophyGenre = trophyList.getDicStoryNoAndTrophyGenre();
        dicStoryNoAndTrophyGenreCode = trophyList.getDicStoryNoAndTrophyGenreCode();
        string TrophyGenre = dicStoryNoAndTrophyGenre[storyNo];
        string TrophyGenreCode = dicStoryNoAndTrophyGenreCode[storyNo];

        //図形数を求める
        listTrophyInfo = trophyList.getDicGenreAndTrophyInfo(TrophyGenre);
        int FigurateNumber = GetFigurateNumbers(listTrophyInfo.Count());
        

        

        //画面の構成数を、トロフィー画面に応じて決定する
        //それぞれのポイントをクリックすることで、詳細のクエストが表示されるようにする
        RenderTrophyPieces(TrophyGenreCode, listTrophyInfo, FigurateNumber);

        //クエストを取得し、該当するidが含まれているかを確認。
        //含まれていた場合、達成しているか否かで画面パズルに色をつけるか否かを決める
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RenderTrophyPieces(string TrophyGenreCode, List<TrophyList.TrophyInfo> listTrophyInfo, int FigurateNumber){
		//トロフィー表示欄を全てクリア
		foreach ( Transform obj in TrophyViewArea ){
			GameObject.Destroy(obj.gameObject);
		}

        //トロフィーを全て達成しているか否か
        //PlayerDataStoreにトロフィー獲得の実績があるかどうかをチェック
        string[] aryTrophy = PlayerDataStore.Instance.data.trophy;
        if (0 <= Array.IndexOf(aryTrophy, TrophyGenreCode)){
            //クリアしている時、一枚絵を表示する。
            //TODO:また、絵をクリックしたとき、
            //トロフィーを獲得するためのクエスト一覧を表示する
            //→新しい画面を作る意味あまりない。
            // 実装予定の詳細画面一覧が出来上がってから着手

            TrophyViewArea.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(TrophyListArea.rect.width,TrophyListArea.rect.height);
        }else{
            //グリッドサイズを図形数に応じて設定
            TrophyViewArea.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(TrophyListArea.rect.width/FigurateNumber,TrophyListArea.rect.height/FigurateNumber);
            
            //トロフィーの情報をボタンに付与し、追加していく
            //クリアフラグはtrueにしておき、
            //トロフィーを全て取得していないかつ、最後までtrueだった場合は、
            //トロフィー獲得のアニメーションを入れる
            bool flgQuestClear;

            for(int i=0; i<listTrophyInfo.Count(); ++i){
        		//インスタンスを生成
	        	GameObject TrophyButtonObj = Instantiate<GameObject>( OriginalTrophyPiece );
                // オブジェクトを表示
                TrophyButtonObj.gameObject.SetActive( true );
                // 親子関係にする
                TrophyButtonObj.transform.SetParent( TrophyViewArea, false );
                //子要素を取得
                var childTransform = TrophyButtonObj.GetComponentsInChildren<Transform>();
	    	    //名前をidにする
		        TrophyButtonObj.gameObject.name = listTrophyInfo[i].id;

                //クエストをクリアしているか否か判定
                //獲得済のクエストであれば、ボタンを透明にする
                if(dicQuestCompleteFlg.ContainsKey(listTrophyInfo[i].id) && dicQuestCompleteFlg[listTrophyInfo[i].id]){
                    flgQuestClear = true;
                    TrophyButtonObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
                }else{
                    flgQuestClear = false;
                }

	    	    //ボタンに処理をadd
                dicItemInfoValue["TrophyText" + listTrophyInfo[i].id] = listTrophyInfo[i].trophyName;
                dicItemInfoValue["HowToReleaseText" + listTrophyInfo[i].id] = listTrophyInfo[i].trophyName;
                dicItemInfoValue["GiftTitle" + listTrophyInfo[i].id] = listTrophyInfo[i].clearItemName;
                dicItemInfoValue["GiftDescription" + listTrophyInfo[i].id] = listTrophyInfo[i].clearItemDescription;
                dicItemInfoValue["GiftImage" + listTrophyInfo[i].id] = listTrophyInfo[i].clearItemFilePath;

    		    TrophyButtonObj.GetComponent<Button>().onClick.AddListener( () => GetTrophyDetailView(TrophyButtonObj.gameObject.name, dicItemInfoValue, flgQuestClear) );
            }

            //TODO:トロフィー制覇のアニメーションを入れる


            //枠数に対し、トロフィーの要素が少なかったら、黒く塗りつぶす枠を表示
            //(押しても反応はしない)
            //TODO:デフォルトで絵を表示するかどうかは、後藤さんと相談。 
            if(listTrophyInfo.Count() < (FigurateNumber*FigurateNumber)){
                bool flgThoroughly = false;
                int countRest = (FigurateNumber*FigurateNumber) - listTrophyInfo.Count();

                for(int i=0; i < countRest; ++i){
            		//インスタンスを生成
	            	GameObject HideTrophyObj = Instantiate<GameObject>( HideTrophyPiece );
                    // オブジェクトを表示
                    HideTrophyObj.gameObject.SetActive( true );
                    // 親子関係にする
                    HideTrophyObj.transform.SetParent( TrophyViewArea, false );
    		        //名前をidにする
	    	        HideTrophyObj.gameObject.name = listTrophyInfo[i].id;
    		        //ボタンに処理をadd
                    //→TODO:ボタン処理は必要ない？一旦コメントアウト
		            //HideTrophyObj.GetComponent<Button>().onClick.AddListener( () => GetTrophyDetailView() );
                    if(flgThoroughly){
                        HideTrophyObj.transform.SetSiblingIndex(0);
                    }else{
                        HideTrophyObj.transform.SetSiblingIndex(TrophyViewArea.GetComponentsInChildren<Transform>().Length);
                    }
                    flgThoroughly = !flgThoroughly;
                }
            }
        }
    }

    public void GetTrophyDetailView(string trophyId, Dictionary<string, string> dicItemInfoValue, bool flgClear){

		//子要素を取得
        var childTransform = TrophyDialogPanel.GetComponentsInChildren<Transform>();
		foreach (Transform child in childTransform){

            //共通処理
            //トロフィーのタイトル、解放条件、ページクローズ処理を付与
            if(child.name == "TrophyText"){
                child.GetComponent<Text>().text = dicItemInfoValue["TrophyText" + trophyId];
            }else if(child.name == "HowToReleaseText"){
                child.GetComponent<Text>().text = "クエスト「" + dicItemInfoValue["HowToReleaseText" + trophyId] + "」\nをクリアする";
            }else if(child.name == "buttonClose"){
    			child.GetComponent<Button>().onClick.AddListener( CloseTrophyDetail );
		    }

            if(flgClear){
                //クリアしていれば、報酬のところを表示する
                if(child.name == "GiftTitle"){
                    child.GetComponent<Text>().text = dicItemInfoValue["GiftTitle" + trophyId];
                }else if(child.name == "GiftDescription"){
                    child.GetComponent<Text>().text = dicItemInfoValue["GiftDescription" + trophyId];
                }else if(child.name == "GiftImage"){
                    child.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["GiftImage" + trophyId], typeof(Sprite)) as Sprite;
                }else if(child.name == "HideImage"){
                    child.gameObject.SetActive(false);
                }else if(child.name == "ClearStampImage"){
                    child.gameObject.SetActive(true);
                }

            }else{
                //クリアしていなければ、報酬のところはhideする
                if(child.name == "GiftTitle"){
                    child.GetComponent<Text>().text = "???";
                }else if(child.name == "GiftDescription"){
                    child.GetComponent<Text>().text = "???";
                }else if(child.name == "GiftImage"){
                    child.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["GiftImage" + trophyId], typeof(Sprite)) as Sprite;
                }else if(child.name == "HideImage"){
                    child.gameObject.SetActive(true);
                }else if(child.name == "ClearStampImage"){
                    child.gameObject.SetActive(false);
                }
            }
		}
		TrophyDialogPanelBG.SetActive(true);
		TrophyDialogPanel.SetActive(true);
    }

	public void CloseTrophyDetail(){
		TrophyDialogPanelBG.SetActive(false);
		TrophyDialogPanel.SetActive(false);
	}


    int GetFigurateNumbers(int number){
        int n = 2;
        while((n*n) < number){
            n = n + 1;
        }
        return n;
    }
}
