﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class ShopViewController : MonoBehaviour
{
	[Header("ItemListSetting"), SerializeField]
	public RectTransform ShopItemList;
	public Transform rootContent;
	public GameObject OriginalShopItemObj;

	[Header("TradeItemListSetting"), SerializeField]
	public GameObject OriginalTradeItemObj;
	public GameObject TradeItemDetailDialog;
	public GameObject TradeItemDetailDialogBG;
	public Text TradeItemNumberText;	//交換アイテムの監視用
	public Transform TradeItemListArea;

	[Header("AddReduceButtonSetting"), SerializeField]
	public GameObject AddTradeItemButton;
	public GameObject ReduceTradeItemButton;

	[Header("shopItemDataSetting"), SerializeField]
    public string shopGenre;

	[Header("testSetting"), SerializeField]
	public shopItemList shopItemList;
	public materiList materiList;

	//内部判定用関数
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_itemName = new Dictionary<string, materiList.materiInfo>();    
    private Dictionary<string, shopItemList.shopItemInfo> dicShopItemListInfo_id = new Dictionary<string, shopItemList.shopItemInfo>();    
	
	// Use this for initialization
	void Start () {

		//アイテムリストの画面比率調整
		rootContent.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(ShopItemList.rect.width/3.5f,ShopItemList.rect.width/3.5f);

		//ショップに応じたショップアイテム取得
        //dicShopItemListInfo_id = ItemDataStore.Instance.data.shopItemList.getShopItemInfo_genre(shopGenre);
        dicShopItemListInfo_id = shopItemList.getShopItemInfo_genre(shopGenre);

		//マテリ情報の取得
        //dicMateriListInfo_itemName = ItemDataStore.Instance.data.materiList.getMateriInfo_itemName();
        dicMateriListInfo_itemName = materiList.getMateriInfo_itemName();

		//ショップアイテムの描画
		RenderItem(shopGenre);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void RenderItem(string shopGenre){
		//ショップアイテムを描画する関数
		//アイテム欄を削除したのち、PlayerDataから所持アイテム情報を獲得
		//選択されているジャンルに応じて、処理を分ける

		//アイテムの名前、説明などの情報を格納する一時的な辞書を宣言
		Dictionary<string, string> dicShopItemInfoValue = new Dictionary<string, string>();
		
		//アイテム欄を全てクリア
		foreach ( Transform obj in rootContent ){
			GameObject.Destroy(obj.gameObject);
		}		

        //アイテム辞書を参照し、アイテムを描画していく
		foreach (string dicShopItemKey in dicShopItemListInfo_id.Keys){
            //01. 描画条件を満たしているか確認
            //ストーリーの進捗具合を見て、一定ストーリーに進んでいたら、描画してもOK
            //int playerStoryNo = PlayerDataStore.Instance.data.storyNo;
            int playerStoryNo = 1;
            if(playerStoryNo >= dicShopItemListInfo_id[dicShopItemKey].minStoryNo){

        		//02. アイテムの描画
                //ショップのジャンルとアイテムのジャンルが一致していれば
                //アイテムを描画していく
                if(dicShopItemListInfo_id[dicShopItemKey].genre == shopGenre){

                    //詳細画面作成用の辞書作成
					dicShopItemInfoValue["TitleText"+dicShopItemListInfo_id[dicShopItemKey].id] = dicShopItemListInfo_id[dicShopItemKey].itemName;
					dicShopItemInfoValue["TradeItemImage"+dicShopItemListInfo_id[dicShopItemKey].id] = dicShopItemListInfo_id[dicShopItemKey].filePath;
					dicShopItemInfoValue["ExplainText"+dicShopItemListInfo_id[dicShopItemKey].id] = dicShopItemListInfo_id[dicShopItemKey].description;
					dicShopItemInfoValue["TradeItemText"+dicShopItemListInfo_id[dicShopItemKey].id] = dicShopItemListInfo_id[dicShopItemKey].tradeItem;

					SetShopItemData(dicShopItemListInfo_id[dicShopItemKey],dicShopItemInfoValue);
                }
            }
		}
		
	}

	public void SetShopItemData(shopItemList.shopItemInfo shopItem, Dictionary<string,string> dicItemInfoValue){

		//表示するアイテムのジャンルと、アイテムのIDを読み取り、
		//アイテムをセットしていく

		//インスタンスを生成
		GameObject ShopItemObj = Instantiate<GameObject>( OriginalShopItemObj );
        // オブジェクトを表示
        ShopItemObj.gameObject.SetActive( true );
        // 親子関係にする
        ShopItemObj.transform.SetParent( rootContent, false );
        //子要素を取得
        var childTransform = ShopItemObj.GetComponentsInChildren<Transform>();
		//名前をidにする
		ShopItemObj.gameObject.name = shopItem.id;

        //アイテム名と画像を更新
    	foreach (Transform child in childTransform){
            //アイテム名の更新
    		if(child.name == "TitleText"){
	    	    child.GetComponent<Text>().text = dicItemInfoValue["TitleText" + shopItem.id];
		    //画像の更新
    		}else if(child.name == "TradeItemImage"){
				child.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["TradeItemImage" + shopItem.id], typeof(Sprite)) as Sprite;
            }
        }

        //ボタンに処理をadd
		ShopItemObj.GetComponent<Button>().onClick.AddListener( () => GetTradeItemDetailView(shopItem, dicItemInfoValue) );
	}
	
	public void GetTradeItemDetailView(shopItemList.shopItemInfo shopItem, Dictionary<string, string> dicItemInfoValue){
        
        //交換の詳細画面を表示する
        //詳細画面を作成
        var childTransform = TradeItemDetailDialog.GetComponentsInChildren<Transform>();

		//交換アイテム数を初期値にする
		TradeItemNumberText.text = "1";
        
        //トレードアイテムのリストを作成（実際のトレード処理に使用）
        List<string> listTradeItem = new List<string>();

        //トレードアイテムのリストを作成（トレードアイテム描画処理に使用）
        List<string> tradeItemList = new List<string>();

		//トレードアイテムの数を更新するための辞書を作成
		Dictionary<string, int> dicNeedTradeItemNumber = new Dictionary<string, int>();
        
    	foreach (Transform child in childTransform){
            //アイテム名の更新
    		if(child.name == "TitleText"){
	    	    child.GetComponent<Text>().text = dicItemInfoValue["TitleText" + shopItem.id];
            //説明の更新
    		}else if(child.name == "ExplainText"){
				child.GetComponent<Text>().text = "\n" + dicItemInfoValue["ExplainText" + shopItem.id];
            //画像の更新
    		}else if(child.name == "TradeItemImage"){
				child.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["TradeItemImage" + shopItem.id], typeof(Sprite)) as Sprite;
            //トレードアイテムの更新
    		}else if(child.name == "TradeItemList"){

				//トレードアイテム欄を全てクリア
				foreach ( Transform obj in child ){
					GameObject.Destroy(obj.gameObject);
				}

                //トレードアイテムの数だけ要素を作成し、更新していく
                //トレードアイテム数を取得
                if(dicItemInfoValue["TradeItemText" + shopItem.id].Contains(",")){
                    string[] aryTradeItem = dicItemInfoValue["TradeItemText" + shopItem.id].Split(',');
                    tradeItemList.AddRange(aryTradeItem);
                }else{
                    tradeItemList.Add(dicItemInfoValue["TradeItemText" + shopItem.id]);
                }

                //トレードアイテムだけ要素を追加
                for(int i = 0; i < tradeItemList.Count();i ++){
            		//インスタンスを生成
		            GameObject TradeItemObj = Instantiate<GameObject>( OriginalTradeItemObj );
                    // オブジェクトを表示
                    TradeItemObj.gameObject.SetActive( true );
                    // 親子関係にする
                    TradeItemObj.transform.SetParent( child, false );
            		//名前をidにする
		            TradeItemObj.gameObject.name = shopItem.id;
                    //子要素を取得
                    var grandChildTransform = TradeItemObj.GetComponentsInChildren<Transform>();
                    foreach (Transform grandChild in grandChildTransform){
                        //アイテム名の更新
                        if(grandChild.name == "TradeItemNameText"){
                            grandChild.GetComponent<Text>().text = tradeItemList[i].Split('*')[0];
                            listTradeItem.Add(tradeItemList[i].Split('*')[0]);
                        //必要数の更新
                        }else if(grandChild.name == "TradeItemNumber"){
                            grandChild.GetComponent<Text>().text = "×" + tradeItemList[i].Split('*')[1];
							
							//トレードアイテムの数を計算する大元の辞書を作成する
							dicNeedTradeItemNumber[shopItem.id] = int.Parse(tradeItemList[i].Split('*')[1]);

							TradeItemNumberText.ObserveEveryValueChanged( _ => _.text)
								.Subscribe (count => {

									//TODO:foreach回しすぎなので、もっとうまいやり方があれば、それにしたい...

									//トレードアイテムのあるエリアの要素を全て参照
									foreach(Transform tradeItem in TradeItemListArea){

										//トレードアイテムを見つけたら、数を更新するためにforeachでまた探す
										if(dicNeedTradeItemNumber.ContainsKey(tradeItem.name)){
											foreach(Transform tradeItemChild in tradeItem){

												//トレードアイテム数を見つけたら、数を更新
												if(tradeItemChild.name == "TradeItemNumber"){
													int needItemnumber = dicNeedTradeItemNumber[tradeItem.name] * int.Parse(TradeItemNumberText.text);
													tradeItemChild.GetComponent<Text>().text = "×" + needItemnumber.ToString();

													//ReduceButton非表示の処理
													//1以下にならないようにする
													if(int.Parse(TradeItemNumberText.text) <= 1){
														ReduceTradeItemButton.SetActive(false);
													}else{
														ReduceTradeItemButton.SetActive(true);
													}

													//AddButton非表示の処理
													//もし、次に倍した時、所持アイテムを上回っていた場合、ボタンをSetFalseにする
													int nextNeedItemNumber = dicNeedTradeItemNumber[tradeItem.name] * (int.Parse(TradeItemNumberText.text)+1);
													//TODO:ここをPlayerDataStoreに問い合わせて判定する形にする
													if(nextNeedItemNumber >= 30){
														AddTradeItemButton.SetActive(false);
													}else{
														AddTradeItemButton.SetActive(true);
													}

												}
											}
										}
									}

									//Debug.Log(grandChild.name);
									//int needItemnumber = int.Parse(TradeItemNumberText.text) * int.Parse(tradeItemList[i].Split('*')[1]);
									//grandChild.GetComponent<Text>().text = "×" + needItemnumber.ToString();
			    				});

                        //画像の更新
                        }else if(grandChild.name == "TradeItemImage"){
                            grandChild.GetComponent<Image>().sprite = Resources.Load(dicMateriListInfo_itemName[tradeItemList[i].Split('*')[0]].filePath, typeof(Sprite)) as Sprite;
                        }
                    }
                }
            
    		}else if(child.name == "AddTradeItemButton"){
                //実際に交換する処理をボタンに追加
        		child.GetComponent<Button>().onClick.AddListener( () => GetTradeItemSetting("Add") );
    		}else if(child.name == "ReduceTradeItemButton"){
                //実際に交換する処理をボタンに追加
        		child.GetComponent<Button>().onClick.AddListener( () => GetTradeItemSetting("Reduce") );
    		}else if(child.name == "TradeItemButton"){
                //実際に交換する処理をボタンに追加
        		child.GetComponent<Button>().onClick.AddListener( () => TradeItem(listTradeItem) );
    		}else if(child.name == "buttonClose"){
                //画面を閉じる処理をボタンに追加
				child.GetComponent<Button>().onClick.AddListener( CloseTradeItemDetail );
			}
        }
		TradeItemDetailDialogBG.SetActive(true);
		TradeItemDetailDialog.SetActive(true);
			
	}

	public void GetTradeItemSetting(string mode){
		//交換するアイテムの数に合わせて、必要なアイテム数を更新する
		int needItemNumber = int.Parse(TradeItemNumberText.text);

		//加算ボタンが押された場合
		if(mode == "Add"){
			needItemNumber += 1;
			TradeItemNumberText.text = needItemNumber.ToString();
		//減算ボタンが押された場合
		}else if(mode == "Reduce"){
			needItemNumber -= 1;
			TradeItemNumberText.text = needItemNumber.ToString();
		}

	}

    public void TradeItem(List<string> listTradeItem){

    }

    public void CloseTradeItemDetail(){
		TradeItemDetailDialogBG.SetActive(false);
		TradeItemDetailDialog.SetActive(false);
	}

}