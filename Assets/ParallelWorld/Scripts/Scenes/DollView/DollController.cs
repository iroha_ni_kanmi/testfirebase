﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class DollController : MonoBehaviour
{
	[Header("RankViewSetting"), SerializeField]
    private Transform DollDetailViewArea;
    [SerializeField]
	public Transform AllDollViewArea;
    [SerializeField]
	public RectTransform AllDollViewRect;
    [SerializeField]
	public GameObject OriginalDollButton;
    [SerializeField]
	public GameObject HideDollButton;
    [SerializeField]
	public dollList dollList;

	//内部判定用関数
	private Dictionary<string, dollList.dollInfo> dicDollInfo = new Dictionary<string, dollList.dollInfo>();
    private Dictionary<string,bool> dicDoll = new Dictionary<string,bool>();
    private createdDoll[] createdDollList;
    
    public int columnViewArea;

    // Start is called before the first frame update
    void Start()
    {
        //データセット
        //PlayerDataから作成したぬいぐるみ一覧をセット
        dicDoll = PlayerDataStore.Instance.GetDicDoll();

        createdDollList = PlayerDataStore.Instance.data.createdDollList;

        //ScriptableObjectから、ぬいぐるみ一覧情報をセット
        dicDollInfo = QuestDataStore.Instance.data.dollList.getDollInfo();

        //画面比率を調整する
		AllDollViewArea.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(AllDollViewRect.rect.width/columnViewArea,AllDollViewRect.rect.width/columnViewArea);

        RenderDollInfo(createdDollList);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void RenderDollInfo(createdDoll[] createdDollList){
		//ぬいぐるみ表示欄を全てクリア
		foreach ( Transform obj in AllDollViewArea ){
			GameObject.Destroy(obj.gameObject);
		}

        //ぬいぐるみを追加していく

        //ぬいぐるみの作成状況に応じて、ぬいぐるみを表示するか、非表示にするかを決める
        foreach(string dollInfoKey in dicDollInfo.Keys){
            
            if(dicDoll.ContainsKey(dicDollInfo[dollInfoKey].dollKeyName)){
                //バッジを表示した状態でセットしていく
            	//インスタンスを生成
	            GameObject DollButtonObj = Instantiate<GameObject>( OriginalDollButton );
                // オブジェクトを表示
                DollButtonObj.gameObject.SetActive( true );
                // 親子関係にする
                DollButtonObj.transform.SetParent( AllDollViewArea, false );
                //子要素を取得
                //バッジの名前を変更
                var childTransform = DollButtonObj.GetComponentsInChildren<Transform>();
                foreach (Transform child in childTransform){
                    if(child.name == "dollNameText"){
                        child.GetComponent<Text>().text = dicDollInfo[dicDollInfo[dollInfoKey].dollKeyName].dollName;
                    }else if(child.name == "dollButtonImage"){
                        child.GetComponent<Image>().sprite = Resources.Load(dicDollInfo[dicDollInfo[dollInfoKey].dollKeyName].dollImageFilePath, typeof(Sprite)) as Sprite;
                    }
                    
                }
	        	//名前をidにする
    		    DollButtonObj.gameObject.name = dicDollInfo[dollInfoKey].dollKeyName;
                //ボタンに処理を付与する
    		    DollButtonObj.GetComponent<Button>().onClick.AddListener( () => GetRankView(dicDollInfo[dollInfoKey].dollKeyName) );

            }else{
                //バッジを隠した状態でセットしていく
            	//インスタンスを生成
	            GameObject DollButtonObj = Instantiate<GameObject>( HideDollButton );
                // オブジェクトを表示
                DollButtonObj.gameObject.SetActive( true );
                // 親子関係にする
                DollButtonObj.transform.SetParent( AllDollViewArea, false );
                //子要素を取得
                var childTransform = DollButtonObj.GetComponentsInChildren<Transform>();
	        	//名前をidにする
    		    DollButtonObj.gameObject.name = dicDollInfo[dollInfoKey].dollKeyName;
                //ボタンに処理を付与する
    		    DollButtonObj.GetComponent<Button>().onClick.AddListener( () => HideRankView(dicDollInfo[dollInfoKey].dollKeyName) );
            }
            //詳細は、一番最初のぬいぐるみ情報 or 最後の情報
            GetRankView(createdDollList[0].dollName);
        }

        //リストの並べ替え
        sortItemsData();

    }

    void GetRankView(string dollName){

        //子要素を取得
        var childTransform = AllDollViewArea.GetComponentsInChildren<Transform>();
        foreach (Transform child in childTransform){

            //詳細画面の処理
            if(child.name == "dollNameText"){
                child.GetComponent<Text>().text = dicDollInfo[dollName].dollName;
            }else if(child.name == "dollDescriptionText"){
                child.GetComponent<Text>().text = dicDollInfo[dollName].dollDescription;
            }else if(child.name == "dollImageMask"){
                child.GetComponent<Image>().color = new Color(0,0,0,0);
            }else if(child.name == "dollImage"){
                child.GetComponent<Image>().sprite = Resources.Load(dicDollInfo[dollName].dollImageFilePath, typeof(Sprite)) as Sprite;
            }
        }

        SetButtonSelected(dollName);

    }

    void HideRankView(string dollName){
        //子要素を取得
        var childTransform = AllDollViewArea.GetComponentsInChildren<Transform>();
        foreach (Transform child in childTransform){

            //詳細画面の処理
            if(child.name == "dollNameText"){
                child.GetComponent<Text>().text = "???";
            }else if(child.name == "dollDescriptionText"){
                child.GetComponent<Text>().text = "???";
            }else if(child.name == "dollImageMask"){
                child.GetComponent<Image>().color = new Color(255,255,255,255);
    	    }
        }

        SetButtonSelected(dollName);

    }

    void SetButtonSelected(string dollName){
        foreach (Transform child in AllDollViewArea){
            if(dicDollInfo.ContainsKey(dollName)){
                if(child.name == dollName){
                    child.GetComponent<Image>().color = new Color(255,255,255,255);
                }else{
                    child.GetComponent<Image>().color = new Color(0,0,0,0);
                }
            }
        }
    }

	public void sortItemsData(){
		//リストの中のぬいぐるみをならべかえる
		foreach (Transform obj in AllDollViewArea.transform){
            if(dicDollInfo.ContainsKey(obj.name)){
                obj.SetSiblingIndex(dicDollInfo[obj.name].sortNumber);
            }
        }
    }
}
