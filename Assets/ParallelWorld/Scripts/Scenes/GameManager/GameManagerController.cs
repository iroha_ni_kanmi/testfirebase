﻿using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using GoShared;
using GoMap;


public class GameManagerController : MonoBehaviour {

	[Header("LoadSceneSetting"), SerializeField]
	//public AudioSource BGMVolume;
	//public AudioSource SEVolume;
	public List<string> DefaultLoadScene;
	public Material skybox;

	[Header("DataSetSetting"), SerializeField]
	public GameObject DataSetController;

	//test
	//public Text testText01;
	//public Text testText02;
	//public Text testText03;
	//public MainQuestList mainQuestList;

	void Awake () {
		//GameObject _dataSetController = Instantiate<GameObject>( DataSetController );

	
		StatusDataStore.Instance.data.loadingStatus = true;

		StatusDataStore.Instance.data.ObserveEveryValueChanged( _ => _.viewMode)
			.Subscribe (mode => {

				Debug.Log("Changed");
				Debug.Log(StatusDataStore.Instance.data.viewMode);

				if(StatusDataStore.Instance.data.networkStatus != "OFF"){

					LoadSceneController.SetScenes();

				}else{
					LoadSceneController.LoadSceneObjects("NetworkErrorView");
				}

				RenderSettings.skybox = skybox;
		    });
		
		
				
	}


	void Start () {
        //デフォルトで読み込むSceneを読み込み
		/*
        string[] ExceptScenes = DefaultLoadScene.ToArray();
		for(int i = 0; i < DefaultLoadScene.Count(); i++){
			LoadSceneController.LoadSceneObjects(DefaultLoadScene[i]);
			}
		 */
		//BGM・効果音の設定を反映
		//BGMVolume.volume = SettingDataStore.Instance.data.bgmVolume;
		//SEVolume.volume = SettingDataStore.Instance.data.seVolume;
		Firebase.FirebaseApp.Create();

	}
	
	// Update is called once per frame
	void Update () {
		//testText01.text = PlayerDataStore.Instance.data.thankPoint.ToString();
		//testText02.text = mainQuestList.getIds()[0];
		//testText03.text = QuestDataStore.Instance.data.storyList.getStoryNoes()[0].ToString();

	}
	
}
