﻿using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using GoShared;
using ParallelWorld.DBModel;
using UnityEngine.Networking;
using Models;
using Utils;

public class DataSetController : MonoBehaviour {
	
	[Header("PlayerDataSetting"), SerializeField]
	private string userID;

	[Header("QuestDataSetting"), SerializeField]
	private MainQuestList mainQuestList;
	[SerializeField]
	private SubQuestList subQuestList;
	[SerializeField]
	private storyList storyList;
	[SerializeField]
	private naoshiQuestList naoshiQuestList;
	[SerializeField]
	private naoshiDialogList naoshiDialogList;
	[SerializeField]
	private getItemList getItemList;
	[SerializeField]
	private meetCharacterList meetCharacterList;
	[SerializeField]
	private TrophyList trophyList;
	[SerializeField]
	private rankList rankList;
	[SerializeField]
	private dollList dollList;
	[SerializeField]
	private messageList messageList;

	[Header("ItemDataSetting"), SerializeField]
	private materiList materiList;
	[SerializeField]
	private clothingList clothingList;
	[SerializeField]
	private keyItemList keyItemList;
	[SerializeField]
	private shopItemList shopItemList;

	[Header("EtcSetting"), SerializeField]
	private TutorialList tutorialList;
	[SerializeField]
	private SettingList settingList;

	private static string infoUrl = "https://gjbxefm3s7.execute-api.ap-northeast-1.amazonaws.com/pre_alpha/infos";


	[Header("AudioSetting"), SerializeField]
	public AudioSource BGMVolume;
	public AudioSource SEVolume;


	//test
	//public Text text04;


    private string url = "https://dyhveoorm9.execute-api.ap-northeast-1.amazonaws.com/pre_alpha/players/";


	void Awake () {

		//testDataの取得（AWS反映前に、データベース周りの確認をする場合は、以下のファイルを読み込み、テストする）
		string json = Resources.Load<TextAsset>("DB/test/playerDB").ToString();
		//text04.text = json;
		PlayerDataSet(json);

		//本番Dataの取得
		//GetPlayer();

		//DataStoreセット
		GetDataStore();

		//InfoDataチェック
		CheckInfo();

		//Debug.Log(PlayerDataStore.Instance.data.ExportPlayerDataAsString());

	}

    public void GetPlayer() {
		//プレイヤーデータを取得する
    	StartCoroutine( GetPlayerData( userID ) );
    }

    public void GetDataStore() {
		//データストアを登録する
    	StartCoroutine( DataStoreSet() );
    }

    public void CheckInfo() {
		//運営からの新着情報があるか田舎を確認する
    	StartCoroutine( InfoDataSet() );
    }

	public void PlayerDataSet(string json){
		//PlayerDataをSet
		PlayerData playerData = new PlayerData();
		playerData = JsonUtility.FromJson<PlayerData>(json);

		//PlayerDataStoreへ読み込んだplayerDataを格納
		PlayerDataStore.Instance.data = playerData;

		//PlayerDataが取得できたかの確認
		//string serialisedItemJson = JsonUtility.ToJson(playerData);
        //Debug.Log("serialisedItemJson " + serialisedItemJson);

		//TODO：ここにデータをセットする処理と完了したらローディングを解除する処理を追加

		//SettingDataStoreにデータをセット
		/*
		SettingDataStore.Instance.data.bgmVolume = PlayerDataStore.Instance.data.settings.bgm_volume;
		SettingDataStore.Instance.data.seVolume = PlayerDataStore.Instance.data.settings.se_volume;
		 */

		//データローディング完了後の処理

		//TODO:BGM系のセットは分けたほうがいいかも

		/*

		//BGM・効果音の設定を反映
		BGMVolume.volume = SettingDataStore.Instance.data.bgmVolume;
		SEVolume.volume = SettingDataStore.Instance.data.seVolume;

		//BGMの再生
		BGMVolume.Play();
		 */

	}

    IEnumerator GetPlayerData(string id)
    {
		//AWSへ問い合わせるurlを発行
        string idUrl = url + id;

        UnityWebRequest request = UnityWebRequest.Get( idUrl );

        yield return request.SendWebRequest();

        if (request.isNetworkError) {
			//TODO:ネットワークエラーの文言を出す
            Debug.Log( request.error );
        }
        else {
            if (request.responseCode == 200) {
				//リクエストに成功したかどうかのログ出力
				//Debug.Log(request.downloadHandler.text);

				//PlayerDataStoreの作成・データの登録
				PlayerDataSet(request.downloadHandler.text);
				}
        }
    }

    IEnumerator DataStoreSet(){
		//クエストDBから必要な情報一式をQuestDataStoreへ登録
		QuestDataStore.Instance.data.mainQuestList = mainQuestList;
		QuestDataStore.Instance.data.subQuestList = subQuestList;
		QuestDataStore.Instance.data.storyList = storyList;
		QuestDataStore.Instance.data.naoshiQuestList = naoshiQuestList;
		QuestDataStore.Instance.data.naoshiDialogList = naoshiDialogList;
		QuestDataStore.Instance.data.getItemList = getItemList;
		QuestDataStore.Instance.data.meetCharacterList = meetCharacterList;
		QuestDataStore.Instance.data.trophyList = trophyList;
		QuestDataStore.Instance.data.rankList = rankList;
		QuestDataStore.Instance.data.tutorialList = tutorialList;
		QuestDataStore.Instance.data.dollList = dollList;
		QuestDataStore.Instance.data.messageList = messageList;

		ItemDataStore.Instance.data.materiList = materiList;
		ItemDataStore.Instance.data.clothingList = clothingList;
		ItemDataStore.Instance.data.keyItemList = keyItemList;
		ItemDataStore.Instance.data.shopItemList = shopItemList;

		//SettingDataStore.Instance.data.settingList = settingList;

		yield return null;
	}

    IEnumerator InfoDataSet(){

		//情報の更新があったかどうかを確認する
        UnityWebRequest request = UnityWebRequest.Get( infoUrl );

        yield return request.SendWebRequest();

        if (request.isNetworkError) {
            Debug.Log( request.error );
        }
        else {
            if (request.responseCode == 200) {
                Debug.Log( "Received data: " + request.downloadHandler.text );
                //test============
                string jsonString = fixJson( Resources.Load<TextAsset>("DB/test/infoDB").ToString() );
                Info[] infos = JsonHelper.FromJson<Info>( jsonString );
                //ここまで================

                //テスト解除の際は、下2行のコメントアウトの処理を復活させる
                //string jsonString = fixJson( request.downloadHandler.text );
                //Info[] infos = JsonHelper.FromJson<Info>( jsonString );

				foreach(Info info in infos){
					if(DateTime.Parse(info.date) > DateTime.Parse(PlayerDataStore.Instance.data.last_updated)){
						//ここにチェックしたか否かの結果を保持する
						//Debug.Log("新着情報があります");
						StatusDataStore.Instance.data.getNews = true;
					}else{
						//Debug.Log("新着情報はありません");
						StatusDataStore.Instance.data.getNews = false;
					}
				}
				

            }
        }
	}

    private string fixJson(string value) {
        value = "{\"Items\":" + value + "}";
        return value;
    }
}
