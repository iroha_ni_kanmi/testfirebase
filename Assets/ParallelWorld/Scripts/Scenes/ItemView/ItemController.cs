﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;
using ParallelWorld.DBModel;

public class ItemController : MonoBehaviour {
	
	[Header("ItemListSetting"), SerializeField]
	public RectTransform ItemList;
	public Transform rootContent;
	public GameObject OriginalItemObj;
	public GameObject ItemDetailDialog;

	public GameObject ItemDetailDialogBG;


	[Header("ItemDataSetting"), SerializeField]
	public ToggleGroup ItemGenre;

	//内部判定用関数
    private Dictionary<string, materiList.materiInfo> dicMateriListInfo_id = new Dictionary<string, materiList.materiInfo>();    
    private Dictionary<string, clothingList.clothingInfo> dicClothingListInfo_id = new Dictionary<string, clothingList.clothingInfo>();    
    private Dictionary<string, keyItemList.keyItemInfo> dicKeyItemListInfo_id = new Dictionary<string, keyItemList.keyItemInfo>();    

	// Use this for initialization
	void Start () {

		//アイテムリストの画面比率調整
		rootContent.gameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(ItemList.rect.width/3.5f,ItemList.rect.width/3.5f);

		//マテリ・衣服・キーアイテム情報の取得
        dicMateriListInfo_id = ItemDataStore.Instance.data.materiList.getMateriInfo_id();
        dicClothingListInfo_id = ItemDataStore.Instance.data.clothingList.getClothingInfo_id();
        dicKeyItemListInfo_id = ItemDataStore.Instance.data.keyItemList.getKeyItemInfo_id();

		//アイテムの描画
		string selectedGenre = Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
		RenderItem(selectedGenre);


		//アイテムのジャンルが変更された時、
		//それをトリガーに再描画を行う
		ItemGenre.ObserveEveryValueChanged( _ => _.ActiveToggles())
			.Subscribe (text => {
				if(selectedGenre != Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0]){
					selectedGenre = Regex.Split(ItemGenre.ActiveToggles().First().gameObject.name, "Tag")[0];
					//RenderItemData(selectedGenre);
					RenderItem(selectedGenre);
				}
			});
	}
	
	// Update is called once per frame
	void Update () {
	}

	void RenderItem(string selectedGenre){
		//アイテムを描画する関数
		//アイテム欄を削除したのち、PlayerDataから所持アイテム情報を獲得
		//選択されているジャンルに応じて、処理を分ける

		//アイテムの名前、説明などの情報を格納する一時的な辞書を宣言
		Dictionary<string, string> dicItemInfoValue = new Dictionary<string, string>();
		
		//アイテム欄を全てクリア
		foreach ( Transform obj in rootContent ){
			GameObject.Destroy(obj.gameObject);
		}

		//アイテムを描画していく
		//01. 所持アイテムのデータを取得
		ItemInventory[] ary_items = PlayerDataStore.Instance.data.items;

		//02. アイテムの描画
		for(int i=0; i<ary_items.Length; ++i){

			//選択中のアイテムのジャンルであれば、そのアイテムを描画
			switch(selectedGenre){
				case "Materi":
				//マテリのジャンルを選択中かつ
				//マテリ情報にidがあれば、オブジェクトをセット
				if(dicMateriListInfo_id.ContainsKey(ary_items[i].id)){

					dicItemInfoValue["TitleText"+ary_items[i].id] = dicMateriListInfo_id[ary_items[i].id].itemName;
					dicItemInfoValue["ItemImage"+ary_items[i].id] = dicMateriListInfo_id[ary_items[i].id].filePath;
					dicItemInfoValue["ExplainText"+ary_items[i].id] = dicMateriListInfo_id[ary_items[i].id].description;
					Debug.Log(dicMateriListInfo_id[ary_items[i].id].howToGet);
					dicItemInfoValue["HowToGetText"+ary_items[i].id] = dicMateriListInfo_id[ary_items[i].id].howToGet;

					SetItemData("Materi", ary_items[i], i, dicItemInfoValue);
				}else{
					//アイテムのidがなかったらログ出力
					Debug.Log("データベースにアイテムが存在しません");
					Debug.Log("Genre : Materi");
					Debug.Log("id : " + ary_items[i].id);
				}
				break;

				case "Clothing":
				//衣服のジャンルを選択中かつ
				//衣服情報にidがあれば、オブジェクトをセット
				if(dicClothingListInfo_id.ContainsKey(ary_items[i].id)){

					dicItemInfoValue["TitleText"+ary_items[i].id] = dicClothingListInfo_id[ary_items[i].id].clothingName;
					dicItemInfoValue["ItemImage"+ary_items[i].id] = dicClothingListInfo_id[ary_items[i].id].filePath;
					dicItemInfoValue["ExplainText"+ary_items[i].id] = dicClothingListInfo_id[ary_items[i].id].description;
					Debug.Log(dicClothingListInfo_id[ary_items[i].id].howToGet);
					dicItemInfoValue["HowToGetText"+ary_items[i].id] = dicClothingListInfo_id[ary_items[i].id].howToGet;

					SetItemData("Clothing", ary_items[i], i, dicItemInfoValue);
				}else{
					//アイテムのidがなかったらログ出力
					Debug.Log("データベースにアイテムが存在しません");
					Debug.Log("Genre : Clothing");
					Debug.Log("id : " + ary_items[i].id);
				}
				break;

				case "KeyItem":
				//キーアイテムのジャンルを選択中かつ
				//キーアイテム情報にidがあれば、オブジェクトをセット
				if(dicKeyItemListInfo_id.ContainsKey(ary_items[i].id)){

					dicItemInfoValue["TitleText"+ary_items[i].id] = dicKeyItemListInfo_id[ary_items[i].id].keyItemName;
					dicItemInfoValue["ItemImage"+ary_items[i].id] = dicKeyItemListInfo_id[ary_items[i].id].filePath;
					dicItemInfoValue["ExplainText"+ary_items[i].id] = dicKeyItemListInfo_id[ary_items[i].id].description;
					Debug.Log(dicKeyItemListInfo_id[ary_items[i].id].howToGet);
					dicItemInfoValue["HowToGetText"+ary_items[i].id] = dicKeyItemListInfo_id[ary_items[i].id].howToGet;

					SetItemData("KeyItem", ary_items[i], i, dicItemInfoValue);
				}else{
					//アイテムのidがなかったらログ出力
					Debug.Log("データベースにアイテムが存在しません");
					Debug.Log("Genre : KeyItem");
					Debug.Log("id : " + ary_items[i].id);
				}
				break;
			}
		}
		
	}

	public void SetItemData(string mode, ItemInventory item, int itemIndex, Dictionary<string,string> dicItemInfoValue){

		//表示するアイテムのジャンルと、アイテムのIDを読み取り、
		//アイテムをセットしていく

		//インスタンスを生成
		GameObject ItemObj = Instantiate<GameObject>( OriginalItemObj );
        // オブジェクトを表示
        ItemObj.gameObject.SetActive( true );
        // 親子関係にする
        ItemObj.transform.SetParent( rootContent, false );
        //子要素を取得
        var childTransform = ItemObj.GetComponentsInChildren<Transform>();
		//名前をidにする
		ItemObj.gameObject.name = item.id;
		//ボタンに処理をadd
		ItemObj.GetComponent<Button>().onClick.AddListener( () => GetItemDetailView(ItemObj.gameObject.name,dicItemInfoValue, itemIndex) );

        //アイテム名と画像を更新
    	foreach (Transform child in childTransform){
            //アイテム名の更新
    		if(child.name == "ItemNameText"){
	    	    child.GetComponent<Text>().text = "× " + item.quantity;
		        PlayerDataStore.Instance.data.items[itemIndex].ObserveEveryValueChanged( _ => _.quantity)
		    	    .Subscribe (text => {
		    	    child.GetComponent<Text>().text = "× " + PlayerDataStore.Instance.data.items[itemIndex].quantity;
	        	    });
		    //画像の更新
    		}else if(child.name == "ItemImage"){
				child.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["ItemImage" + item.id], typeof(Sprite)) as Sprite;
		    //newマークの更新
    		}else if(child.name == "IconNewMarkImage"){
				if(!item.check){
					child.gameObject.SetActive(true);
					
					//UniRxで値を監視
					//アイテム詳細画面を表示したタイミングで非表示に
					PlayerDataStore.Instance.data.items[itemIndex].ObserveEveryValueChanged( _ => _.check)
						.Subscribe (flg => {
						if(PlayerDataStore.Instance.data.items[itemIndex].check){
							if(child != null){
								child.gameObject.SetActive(false);
							}
						}
					});

				}else{
					child.gameObject.SetActive(false);
				}
        	}
    	}
	}

	public void sortItemsData(string mode){
		//アイテムをならべかえる
		//アイテム情報のsortNumberを読み込んで決定する

		switch(mode){
			case "Materi":
				foreach (Transform obj in rootContent.transform){
					if(dicMateriListInfo_id.ContainsKey(obj.name)){
						obj.SetSiblingIndex(dicMateriListInfo_id[obj.name].sortNumber);
					}
				}
				break;

			case "Clothing":
				foreach (Transform obj in rootContent.transform){
					if(dicClothingListInfo_id.ContainsKey(obj.name)){
						obj.SetSiblingIndex(dicClothingListInfo_id[obj.name].sortNumber);
					}
				}
				break;

			case "KeyItem":
				foreach (Transform obj in rootContent.transform){
					if(dicKeyItemListInfo_id.ContainsKey(obj.name)){
						obj.SetSiblingIndex(dicKeyItemListInfo_id[obj.name].sortNumber);
					}
				}
				break;

			default:
				//画像のパスがなかった場合、
				//ここにデフォルトのパスをかく？
				break;
				}


	}
	
	public void GetItemDetailView(string ItemId, Dictionary<string, string> dicItemInfoValue, int index){
		//子要素を取得
        var childTransform = ItemDetailDialog.GetComponentsInChildren<Transform>();
		foreach (Transform child in childTransform){

			if(child.name == "DetailDialog"){
				var grandChildTransform = child.GetComponentsInChildren<Transform>();
				foreach (Transform grandChild in grandChildTransform){
					//クエスト名の更新
        		    if(grandChild.name == "TitleText"){
		        		grandChild.GetComponent<Text>().text = dicItemInfoValue["TitleText" + ItemId];

					//画像の更新
    				}else if(grandChild.name == "ItemImage"){

		    	    	grandChild.GetComponent<Image>().sprite = Resources.Load(dicItemInfoValue["ItemImage" + ItemId], typeof(Sprite)) as Sprite;
				    //所持数の更新
    				}else if(grandChild.name == "HowManyText"){
						//grandChild.GetComponent<Text>().text = dicQuestSubName[ItemId];
				    //説明の更新
    				}else if(grandChild.name == "ExplainText"){
						grandChild.GetComponent<Text>().text = "\n" + dicItemInfoValue["ExplainText" + ItemId];
				    //取得方法の更新
    				}else if(grandChild.name == "HowToGetText"){
						grandChild.GetComponent<Text>().text = dicItemInfoValue["HowToGetText" + ItemId];
					}
				}
    		}else if(child.name == "buttonClose"){
				child.GetComponent<Button>().onClick.AddListener( CloseItemDetail );

			}
		}
		ItemDetailDialogBG.SetActive(true);
		ItemDetailDialog.SetActive(true);
		
		if(!PlayerDataStore.Instance.data.items[index].check){
			PlayerDataStore.Instance.data.items[index].check = true;
		}
	
	}

	public void CloseItemDetail(){
		ItemDetailDialogBG.SetActive(false);
		ItemDetailDialog.SetActive(false);
	}
}
