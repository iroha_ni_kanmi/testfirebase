﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UniRx;
using System.Text.RegularExpressions;
using System.Linq;

public class ItemViewController : MonoBehaviour {
	
	[Header("ButtonSetting"), SerializeField]
	Button CloseButton01;
    public Button CloseButton02;

	public string SceneName;
	public GameObject DialogButton;
	public Slider CorrectItemPoint;
    private Animator anim;


    void Awake () {
        CloseButton01.onClick.AddListener(() => CloseAnim());
        CloseButton02.onClick.AddListener(() => CloseAnim());
        anim = GetComponent<Animator>();
        anim.SetTrigger("Open");
        DialogButton.GetComponent<Button>().onClick.AddListener( OnClickDialogButton);
	}
	
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		
	}
	
	void OnClickDialogButton(){
		DialogButton.SetActive(false);
        CloseAnim();
        LoadSceneController.LoadSceneObjects(SceneName);
		CorrectItemPoint.value = 0;
	}

    /// <summary>
    /// 閉じるアニメーション
    /// </summary>
    void CloseAnim()
    {
        anim.SetTrigger("Close");
    }

    /// <summary>
    /// AnimationEventで呼び出し
    /// </summary>
    /// <param name="SceneName"></param>
    void CloseScene(string SceneName)
    {
		StatusDataStore.Instance.data.viewMode = "MainView";		
        //SceneManager.UnloadSceneAsync(SceneName);
    }
}
