﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Image))]
public class DragController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Transform canvasTran;
    private GameObject draggingObject;
    public GameObject Viewport;

    void Awake()
    {
        //canvasTran = transform.parent;
        //Debug.Log(Viewport.gameObject.GetComponent<RectTransform>().rect);
    }   

    public void OnBeginDrag(PointerEventData pointerEventData)
    {
        CreateDragObject();
        draggingObject.transform.position = pointerEventData.position;
        //フェードインで出現させる
        CanvasFader.Begin (target:draggingObject, isFadeOut:false, duration:0.2f);

    }

    public void OnDrag(PointerEventData pointerEventData)
    {
        draggingObject.transform.position = pointerEventData.position;
    }

    public void OnEndDrag(PointerEventData pointerEventData)
    {
        gameObject.GetComponent<Image>().color = Vector4.one;
        CanvasFader.Begin (target:draggingObject, isFadeOut:true, duration:0.2f, ignoreTimeScale:true, onFinished:OnFinished);
    }

    // ドラッグオブジェクト作成
    public void CreateDragObject()
    {
    	//ドラッグするオブジェクトを生成
        draggingObject = new GameObject("Dragging Object");
        draggingObject.transform.SetParent(canvasTran);
        draggingObject.transform.SetAsLastSibling();
        draggingObject.transform.localScale = Vector3.one;

        // レイキャストがブロックされないように
        CanvasGroup canvasGroup = draggingObject.AddComponent<CanvasGroup>();
        canvasGroup.blocksRaycasts = false;

        Image draggingImage = draggingObject.AddComponent<Image>();
        Image sourceImage = GetComponent<Image>();

        draggingImage.sprite = sourceImage.sprite;
        draggingImage.preserveAspect = true;
        //draggingImage.rectTransform.sizeDelta = sourceImage.rectTransform.sizeDelta;
        draggingImage.rectTransform.sizeDelta = new Vector2(sourceImage.rectTransform.rect.width,sourceImage.rectTransform.rect.height);
        draggingImage.color = sourceImage.color;
        draggingImage.material = sourceImage.material;

        gameObject.GetComponent<Image>().color = Vector4.one * 0.6f;
    }
    
    private void OnFinished(){
        Destroy(draggingObject);        
    }
}
