﻿using System;
using System.Collections;
using ParallelWorld.DBModel;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace ParallelWorld.Controllers
{

    public class DBContoroller : MonoBehaviour
    {
        private string url = "https://dyhveoorm9.execute-api.ap-northeast-1.amazonaws.com/pre_alpha/players/";

        public InputField playerIdInput;
        public InputField playerNameInput;

        public Text playerName;

        public static PlayerData GetDBData()
        {
            PlayerData data = new PlayerData();

            try
            {
                //TODO:DBからデータを取得する
                var json = "{\"id\":\"42b9c9ab-969f-4d35-b52c-7c0fea913553\",\"name\":\"Test Player\",\"birthday\":\"19791201\",\"last_area\":\"0\",\"last_position\":[35.68139,139.76578],\"appearance\":{},\"item_in_hand\":0,\"items\":[{\"id\":\"e5486786-2dbe-46c3-9ff4-f046303b5638\",\"quantity\":24},{\"id\":\"13f56dcc-c573-4278-99fc-f80ec31ecd19\",\"quantity\":42},{\"id\":\"eea5ae55-287c-45b8-ac9e-56a27534333a\",\"quantity\":32},{\"id\":\"8da62628-8d8f-4c55-be59-19d180d8a607\",\"quantity\":42},{\"id\":\"7554eb2e-edcd-4bcb-b5bc-4e6146e4df55\",\"quantity\":12},{\"id\":\"128833e6-506c-4f71-b816-0fdde28b018a\",\"quantity\":81},{\"id\":\"45a13c3b-d5ca-4ffe-812b-02bde7cd9731\",\"quantity\":43},{\"id\":\"354fa0f1-9b9d-47a8-8daa-b87c38c36308\",\"quantity\":35},{\"id\":\"5a8e1c82-18cf-4e21-b576-4500fca68489\",\"quantity\":64},{\"id\":\"e4de8b56-7b83-4811-a476-4e5f98d9cf7c\",\"quantity\":28},{\"id\":\"addce0ab-0e48-43db-a571-6f1e470587d9\",\"quantity\":54},{\"id\":\"77a5d095-da99-4ba3-bbda-de72f76469f4\",\"quantity\":19},{\"id\":\"90ec0374-ba0f-49c8-9953-4455de1824db\",\"quantity\":32},{\"id\":\"3d727a56-ffca-4312-a429-66ff040db818\",\"quantity\":20},{\"id\":\"1d777bb5-27e3-495e-98d7-a8342a2920d5\",\"quantity\":10},{\"id\":\"669eda0e-cf71-4c49-a37f-465d6ff89434\",\"quantity\":40},{\"id\":\"4fb9fd76-7273-4ec3-86dc-dc8f41a94dca\",\"quantity\":86},{\"id\":\"9dacb770-ea7f-43d7-9cc8-9d7c570840a2\",\"quantity\":1},{\"id\":\"e80932de-08a5-451b-a36e-dfcc6a02d0f5\",\"quantity\":1},{\"id\":\"a4888af8-88ee-4c03-83f2-8546048e2356\",\"quantity\":1},{\"id\":\"dda9174e-7083-4c1d-bc4e-ba15b82b7e47\",\"quantity\":1},{\"id\":\"c74fbb00-f0ae-47e9-924a-b58144647924\",\"quantity\":53}],\"quests\":[{\"progress\":{\"meet_chara\":false,\"take_item\":true,\"create_doll\":false},\"id\":\"5bd42b59-7e8b-43d4-b33a-2e78d2c940f5\"},{\"progress\":{\"go_place\":false,\"take_item\":true},\"id\":\"11438bfb-8f02-4614-a4c0-8633752358f5\"},{\"progress\":{\"take_item\":true,\"create_doll\":false},\"id\":\"508fb1f8-039b-4886-8ee0-d44d129efa01\"},{\"progress\":{\"take_item\":true,\"create_doll\":true},\"id\":\"d8aa2148-3c17-4ed0-bf17-4448c1b05716\"},{\"progress\":{\"take_item\":true,\"create_doll\":true},\"id\":\"243c007f-4e24-4fde-9321-2a933fb021b6\"},{\"progress\":{\"take_item\":true,\"create_doll\":true},\"id\":\"9d3e5eee-16aa-4941-a3e3-36977044887a\"},{\"progress\":{\"take_item\":true,\"create_doll\":true},\"id\":\"182d7a3c-f0cc-4ead-84c6-1b70eefe13ed\"}],\"friends\":[],\"no_ad_id\":\"none\",\"settings\":{\"se_volume\":0,\"bgm_volume\":0},\"version\":\"0.4.0\",\"last_updated\":\"1532049815\"}";

                data = JsonUtility.FromJson<PlayerData>(json);

                //デバッグ
                string serialisedItemJson = JsonUtility.ToJson(data);
                Debug.Log("serialisedItemJson " + serialisedItemJson);
            }
            catch
            {
                //TODO:log
            }

            return data;
        }

//        void Start() {
//            StartCoroutine(GetPlayerData());
//        }

        IEnumerator GetPlayerData(string id) {
            string idUrl = url + id;

            UnityWebRequest request = UnityWebRequest.Get( idUrl );

            yield return request.SendWebRequest();

            if (request.isNetworkError) {
                Debug.Log( request.error );
            }
            else {
                if (request.responseCode == 200) {
                    Debug.Log( "Received data: " + request.downloadHandler.text );
                    PlayerData playerData = JsonUtility.FromJson<PlayerData>( request.downloadHandler.text );
                    Debug.Log( "Received player: \"" + playerData.name + "\"" );

                    string playerSummary = playerData.name + "\nId: " + playerData.id;

                    playerName.text = playerSummary;

                    Debug.Log( "Got new data: " + playerData.version );
                    long timestamp = long.Parse( playerData.last_updated );
                    DateTime epoch = new DateTime( 1970, 1, 1, 0, 0, 0, 0 );
                    DateTime dt = epoch.AddSeconds( timestamp );
                    Debug.Log( "Last updated: " + dt.ToString() );
                }
            }
        }

        public void GetPlayer() {
            StartCoroutine( GetPlayerData( playerIdInput.text ) );
        }

        public void CreatePlayer() {
            StartCoroutine( PostPlayerData( playerIdInput.text, playerNameInput.text ) );
        }

        public void UpdatePlayer() {
            StartCoroutine( UpdatePlayerData( playerIdInput.text, playerNameInput.text ) );
        }

        IEnumerator PostPlayerData(string id, string name)
        {
            string nameHolder = "_だれだれ";
            if (name != "") {
                nameHolder = "_" + name;
            }
            
            string testPlayer = "{ \"id\": \"" + id + "\", \"name\": \"" + nameHolder + "\", \"lastArea\": 1, \"lastPositionLat\": 35.68958759004372, \"lastPositionLon\": 139.70723390579224, \"appearance\": {}, \"itemInHand\": { \"id\": \"90ba0dd6-4522-4be2-999b-1acf5c951b6a\", \"title\": \"Needle\", \"genre\": 1, \"description\": \"Needs a thread...\", \"main_item\": true }, \"currentQuest\": 1, \"questProgress\": 50, \"completedQuest\": [], \"friends\": [], \"noAdId\": \"none\", \"version\": 1}";

            UnityWebRequest request = new UnityWebRequest( url, UnityWebRequest.kHttpVerbPOST );
            byte[] dataBytes = new System.Text.UTF8Encoding().GetBytes( testPlayer );

            request.uploadHandler = (UploadHandler) new UploadHandlerRaw( dataBytes );
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader( "Content-Type", "application/json" );

            yield return request.SendWebRequest();

            if (request.isNetworkError) {
                Debug.Log( request.error );
            }
            else {
                if (request.responseCode == 201) {
                    Debug.Log( "Received data: " + request.downloadHandler.text );

                    PlayerData playerData = JsonUtility.FromJson<PlayerData>( request.downloadHandler.text );

                    string playerSummary = playerData.name + "\nId: " + playerData.id;

                    playerName.text = playerSummary;
                }
                else {
                    Debug.Log( "Failed with response code: " + request.responseCode );
                }
            }
        }

        IEnumerator UpdatePlayerData(string id, string name)
        {
            string updateName = "{ \"name\": " + name + " }";

            UnityWebRequest request = new UnityWebRequest( url + id, UnityWebRequest.kHttpVerbPUT );
            byte[] dataBytes = new System.Text.UTF8Encoding().GetBytes( updateName );

            request.uploadHandler = (UploadHandler) new UploadHandlerRaw( dataBytes );
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader( "Content-Type", "application/json" );

            yield return request.SendWebRequest();

            if (request.isNetworkError) {
                Debug.Log( request.error );
            }
            else {
                if (request.responseCode == 200) {
                    Debug.Log( "Received data: " + request.downloadHandler.text );

                    PlayerData playerData = JsonUtility.FromJson<PlayerData>( request.downloadHandler.text );

                    string playerSummary = playerData.name + " (updated) \nId: " + playerData.id;

                    playerName.text = playerSummary;
                }
                else {
                    Debug.Log( "Failed with response code: " + request.responseCode );
                }
            }
        }
    }

}
