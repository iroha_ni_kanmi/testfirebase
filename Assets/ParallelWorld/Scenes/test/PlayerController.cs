﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public InputField playerIdInput;

	public void generateUUID() {
        System.Guid guid = System.Guid.NewGuid();
        Debug.Log( "Generated new UUID: " + guid );

        playerIdInput.text = guid.ToString();
    }

}
