<Q                         DIRECTIONAL    KAMAKURA_LOCALLIGHT_ON     KAMAKURA_SHADOWMOD_ON       |=  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 unity_OrthoParams;
uniform 	vec4 unity_CameraWorldClipPlanes[6];
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	float _EnableCubeColor;
uniform 	vec3 _CubeColor0;
uniform 	vec3 _CubeColor1;
uniform 	vec3 _CubeColor2;
uniform 	vec3 _CubeColor3;
uniform 	vec3 _CubeColor4;
uniform 	vec3 _CubeColor5;
uniform 	float _CubeColorUseLocalSpace;
uniform 	vec3 _CubeColorLocalSpaceMatrixRow0;
uniform 	vec3 _CubeColorLocalSpaceMatrixRow1;
uniform 	vec3 _CubeColorLocalSpaceMatrixRow2;
uniform 	vec3 _LocalLightVec;
uniform 	float _UsingRightMirroredMesh;
in  vec4 in_POSITION0;
in  vec2 in_TEXCOORD0;
in  vec4 in_NORMAL0;
in  vec4 in_TANGENT0;
in  vec4 in_COLOR0;
out vec4 vs_COLOR0;
out vec2 vs_TEXCOORD0;
out vec3 vs_TEXCOORD1;
out vec3 vs_TEXCOORD2;
out vec3 vs_TEXCOORD3;
out vec3 vs_TEXCOORD4;
out vec3 vs_TEXCOORD7;
out vec3 vs_TEXCOORD8;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec4 u_xlat1;
vec3 u_xlat2;
float u_xlat9;
bool u_xlatb9;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0.xyz = _WorldSpaceLightPos0.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceLightPos0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceLightPos0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[3].xyz * _WorldSpaceLightPos0.www + u_xlat0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    vs_TEXCOORD3.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[1].xyz * _LocalLightVec.yyy;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _LocalLightVec.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _LocalLightVec.zzz + u_xlat0.xyz;
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    vs_TEXCOORD8.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlatb0.x = 0.5<_EnableCubeColor;
    if(u_xlatb0.x){
        u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
        u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
        u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
        u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
        u_xlat9 = inversesqrt(u_xlat9);
        u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
        u_xlatb9 = 0.5<_CubeColorUseLocalSpace;
        if(u_xlatb9){
            u_xlat1.xyz = u_xlat0.yyy * _CubeColorLocalSpaceMatrixRow1.xyz;
            u_xlat1.xyz = u_xlat0.xxx * _CubeColorLocalSpaceMatrixRow0.xyz + u_xlat1.xyz;
            u_xlat1.xyz = u_xlat0.zzz * _CubeColorLocalSpaceMatrixRow2.xyz + u_xlat1.xyz;
            u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
            u_xlat9 = inversesqrt(u_xlat9);
            u_xlat0.xyz = vec3(u_xlat9) * u_xlat1.xyz;
        }
        u_xlat1.xyz = u_xlat0.xyz * u_xlat0.xyz;
        u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
        u_xlat9 = inversesqrt(u_xlat9);
        u_xlat1.xyz = vec3(u_xlat9) * u_xlat1.xyz;
        u_xlatb0.xyz = greaterThanEqual(u_xlat0.xyzx, vec4(0.0, 0.0, 0.0, 0.0)).xyz;
        u_xlat2.xyz = (u_xlatb0.x) ? _CubeColor1.xyz : _CubeColor3.xyz;
        u_xlat0.xyw = (u_xlatb0.y) ? _CubeColor0.xyz : _CubeColor5.xyz;
        u_xlat0.xyw = u_xlat0.xyw * u_xlat1.yyy;
        u_xlat0.xyw = u_xlat1.xxx * u_xlat2.xyz + u_xlat0.xyw;
        u_xlat1.xyw = (u_xlatb0.z) ? _CubeColor2.xyz : _CubeColor4.xyz;
        vs_TEXCOORD7.xyz = u_xlat1.zzz * u_xlat1.xyw + u_xlat0.xyw;
        vs_TEXCOORD7.xyz = clamp(vs_TEXCOORD7.xyz, 0.0, 1.0);
    } else {
        vs_TEXCOORD7.xyz = vec3(0.0, 0.0, 0.0);
    }
    u_xlatb0.x = 0.0<unity_OrthoParams.w;
    if(u_xlatb0.x){
        u_xlat0.xyz = (-unity_CameraWorldClipPlanes[4].yyy) * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
        u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * (-unity_CameraWorldClipPlanes[4].xxx) + u_xlat0.xyz;
        u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * (-unity_CameraWorldClipPlanes[4].zzz) + u_xlat0.xyz;
    } else {
        u_xlat1.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
        u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat1.xyz;
        u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat1.xyz;
        u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
        u_xlat0.xyz = u_xlat1.xyz + (-in_POSITION0.xyz);
    }
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    vs_TEXCOORD2.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    u_xlat0.xyz = in_NORMAL0.yzx * in_TANGENT0.zxy;
    u_xlat0.xyz = in_TANGENT0.yzx * in_NORMAL0.zxy + (-u_xlat0.xyz);
    u_xlat0.xyz = (-u_xlat0.xyz) * in_TANGENT0.www;
    u_xlatb9 = 0.5<_UsingRightMirroredMesh;
    vs_TEXCOORD4.xyz = (bool(u_xlatb9)) ? (-u_xlat0.xyz) : u_xlat0.xyz;
    vs_COLOR0 = in_COLOR0;
    vs_TEXCOORD1.xyz = in_NORMAL0.xyz;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _MainTex_ST;
uniform 	float _LightRampOffset;
uniform 	float _LightRampPresetsOffset;
uniform 	float _LightRampUseGVertexColor;
uniform 	float _FilterHue;
uniform 	float _FilterSaturation;
uniform 	float _FilterBrightness;
uniform 	float _FilterContrast;
uniform 	float _FilterContrastMidPoint;
uniform 	float _EnableFilter;
uniform 	vec4 _DiffuseColor;
uniform 	vec3 _LightColor0;
uniform 	vec4 _SpecularMap_ST;
uniform 	float _SpecularPower1;
uniform 	float _SpecularPower2;
uniform 	vec4 _SpecularColor1;
uniform 	vec4 _SpecularColor2;
uniform 	float _SpecularShiftIntensity;
uniform 	float _StrandTexIntensity;
uniform 	float _GlobalLightIntensity;
uniform 	vec4 _HairSpecShift_ST;
uniform 	vec4 _HairStrandTex_ST;
uniform 	float _PrimarySpecularIntensity;
uniform 	float _PrimarySpecularSmoothness;
uniform 	float _PrimarySpecularShadowAffection;
uniform 	float _PrimaryShift;
uniform 	float _SecondarySpecularIntensity;
uniform 	float _SecondarySpecularSmoothness;
uniform 	float _SecondarySpecularShadowAffection;
uniform 	float _SecondaryShift;
UNITY_LOCATION(0) uniform  sampler2D _LightRampTex;
UNITY_LOCATION(1) uniform  sampler2D _MainTex;
UNITY_LOCATION(2) uniform  sampler2D _HairSpecShift;
UNITY_LOCATION(3) uniform  sampler2D _SpecularMap;
UNITY_LOCATION(4) uniform  sampler2D _HairStrandTex;
in  vec4 vs_COLOR0;
in  vec2 vs_TEXCOORD0;
in  vec3 vs_TEXCOORD1;
in  vec3 vs_TEXCOORD2;
in  vec3 vs_TEXCOORD3;
in  vec3 vs_TEXCOORD4;
layout(location = 0) out vec4 SV_TARGET0;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat10_1;
vec3 u_xlat2;
vec2 u_xlat3;
vec4 u_xlat10_3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat10_5;
vec4 u_xlat6;
vec2 u_xlat7;
vec3 u_xlat8;
vec2 u_xlat9;
vec3 u_xlat10;
vec3 u_xlat12;
float u_xlat14;
float u_xlat16;
float u_xlat21;
float u_xlat22;
float u_xlat16_22;
bool u_xlatb22;
float u_xlat23;
void main()
{
    u_xlat0.x = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * vs_TEXCOORD4.xyz;
    u_xlat21 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * vs_TEXCOORD1.xyz;
    u_xlat21 = dot(vs_TEXCOORD2.xyz, vs_TEXCOORD2.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat22 = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat2.xyz = vec3(u_xlat22) * vs_TEXCOORD3.xyz;
    u_xlat22 = vs_COLOR0.y * 2.0 + -1.0;
    u_xlat23 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat23 = u_xlat23 * 0.5 + _LightRampOffset;
    u_xlat23 = u_xlat23 + 0.5;
    u_xlat3.x = _LightRampUseGVertexColor * u_xlat22 + u_xlat23;
    u_xlat3.y = _LightRampPresetsOffset;
    u_xlat10_3 = texture(_LightRampTex, u_xlat3.xy);
    u_xlat10.xyz = u_xlat10_3.xyz * _LightColor0.xyz;
    u_xlat10.xyz = u_xlat10.xyz * vec3(_GlobalLightIntensity);
    u_xlat4.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat4 = texture(_MainTex, u_xlat4.xy);
    u_xlatb22 = 0.5<_EnableFilter;
    if(u_xlatb22){
        u_xlatb22 = u_xlat4.y>=u_xlat4.z;
        u_xlat22 = u_xlatb22 ? 1.0 : float(0.0);
        u_xlat5.xy = u_xlat4.zy;
        u_xlat5.z = float(-1.0);
        u_xlat5.w = float(0.666666687);
        u_xlat6.xy = u_xlat4.yz + (-u_xlat5.xy);
        u_xlat6.z = float(1.0);
        u_xlat6.w = float(-1.0);
        u_xlat5 = vec4(u_xlat22) * u_xlat6 + u_xlat5;
        u_xlatb22 = u_xlat4.x>=u_xlat5.x;
        u_xlat22 = u_xlatb22 ? 1.0 : float(0.0);
        u_xlat6.xyz = u_xlat5.xyw;
        u_xlat6.w = u_xlat4.x;
        u_xlat5.xyw = u_xlat6.wyx;
        u_xlat5 = (-u_xlat6) + u_xlat5;
        u_xlat5 = vec4(u_xlat22) * u_xlat5 + u_xlat6;
        u_xlat22 = min(u_xlat5.y, u_xlat5.w);
        u_xlat22 = (-u_xlat22) + u_xlat5.x;
        u_xlat23 = (-u_xlat5.y) + u_xlat5.w;
        u_xlat12.x = u_xlat22 * 6.0 + 1.00000001e-10;
        u_xlat23 = u_xlat23 / u_xlat12.x;
        u_xlat23 = u_xlat23 + u_xlat5.z;
        u_xlat12.x = u_xlat5.x + 1.00000001e-10;
        u_xlat22 = u_xlat22 / u_xlat12.x;
        u_xlat23 = abs(u_xlat23) + _FilterHue;
        u_xlat22 = u_xlat22 * _FilterSaturation;
        u_xlat5.x = u_xlat5.x * _FilterBrightness;
        u_xlat12.xyz = vec3(u_xlat23) + vec3(1.0, 0.666666687, 0.333333343);
        u_xlat12.xyz = fract(u_xlat12.xyz);
        u_xlat12.xyz = u_xlat12.xyz * vec3(6.0, 6.0, 6.0) + vec3(-3.0, -3.0, -3.0);
        u_xlat12.xyz = abs(u_xlat12.xyz) + vec3(-1.0, -1.0, -1.0);
        u_xlat12.xyz = clamp(u_xlat12.xyz, 0.0, 1.0);
        u_xlat12.xyz = u_xlat12.xyz + vec3(-1.0, -1.0, -1.0);
        u_xlat12.xyz = vec3(u_xlat22) * u_xlat12.xyz + vec3(1.0, 1.0, 1.0);
        u_xlat5.xyz = u_xlat5.xxx * u_xlat12.xyz + (-vec3(vec3(_FilterContrastMidPoint, _FilterContrastMidPoint, _FilterContrastMidPoint)));
        u_xlat4.xyz = u_xlat5.xyz * vec3(vec3(_FilterContrast, _FilterContrast, _FilterContrast)) + vec3(vec3(_FilterContrastMidPoint, _FilterContrastMidPoint, _FilterContrastMidPoint));
        u_xlat4.xyz = clamp(u_xlat4.xyz, 0.0, 1.0);
    }
    u_xlat4.xyz = u_xlat4.xyz * _DiffuseColor.xyz;
    u_xlat5.xy = vs_TEXCOORD0.xy * _HairSpecShift_ST.xy + _HairSpecShift_ST.zw;
    u_xlat10_5 = texture(_HairSpecShift, u_xlat5.xy);
    u_xlat16_22 = u_xlat10_5.x + -0.5;
    u_xlat23 = u_xlat16_22 * _SpecularShiftIntensity + _PrimaryShift;
    u_xlat5.xyz = vec3(u_xlat23) * u_xlat1.xyz + u_xlat0.xyz;
    u_xlat23 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat23 = inversesqrt(u_xlat23);
    u_xlat5.xyz = vec3(u_xlat23) * u_xlat5.xyz;
    u_xlat22 = u_xlat16_22 * _SpecularShiftIntensity + _SecondaryShift;
    u_xlat0.xyz = vec3(u_xlat22) * u_xlat1.xyz + u_xlat0.xyz;
    u_xlat1.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    u_xlat1.xy = vs_TEXCOORD0.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
    u_xlat10_1 = texture(_SpecularMap, u_xlat1.xy);
    u_xlat8.xyz = vs_TEXCOORD2.xyz * vec3(u_xlat21) + u_xlat2.xyz;
    u_xlat21 = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat8.xyz = vec3(u_xlat21) * u_xlat8.xyz;
    u_xlat21 = dot(u_xlat5.xyz, u_xlat8.xyz);
    u_xlat2.x = (-u_xlat21) * u_xlat21 + 1.0;
    u_xlat2.x = sqrt(u_xlat2.x);
    u_xlat21 = u_xlat21 + 1.0;
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
    u_xlat9.x = u_xlat21 * -2.0 + 3.0;
    u_xlat21 = u_xlat21 * u_xlat21;
    u_xlat21 = u_xlat21 * u_xlat9.x;
    u_xlat2.x = log2(u_xlat2.x);
    u_xlat2.x = u_xlat2.x * _SpecularPower1;
    u_xlat2.x = exp2(u_xlat2.x);
    u_xlat9.xy = vec2(vec2(_PrimarySpecularSmoothness, _PrimarySpecularSmoothness)) * vec2(-0.5, 0.5) + vec2(0.5, 0.5);
    u_xlat16 = (-u_xlat9.x) + u_xlat9.y;
    u_xlat21 = u_xlat21 * u_xlat2.x + (-u_xlat9.x);
    u_xlat2.x = float(1.0) / u_xlat16;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = clamp(u_xlat21, 0.0, 1.0);
    u_xlat2.x = u_xlat21 * -2.0 + 3.0;
    u_xlat21 = u_xlat21 * u_xlat21;
    u_xlat21 = u_xlat21 * u_xlat2.x;
    u_xlat21 = u_xlat10_1.x * u_xlat21;
    u_xlat2.xyz = vec3(u_xlat21) * _SpecularColor1.xyz;
    u_xlat21 = _GlobalLightIntensity * u_xlat10_3.x + -1.0;
    u_xlat23 = _PrimarySpecularShadowAffection * u_xlat21 + 1.0;
    u_xlat2.xyz = vec3(u_xlat23) * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat8.xyz);
    u_xlat7.x = (-u_xlat0.x) * u_xlat0.x + 1.0;
    u_xlat7.x = sqrt(u_xlat7.x);
    u_xlat0.x = u_xlat0.x + 1.0;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat14 = u_xlat0.x * -2.0 + 3.0;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat14;
    u_xlat7.x = log2(u_xlat7.x);
    u_xlat7.x = u_xlat7.x * _SpecularPower2;
    u_xlat7.x = exp2(u_xlat7.x);
    u_xlat8.xy = vec2(vec2(_SecondarySpecularSmoothness, _SecondarySpecularSmoothness)) * vec2(-0.5, 0.5) + vec2(0.5, 0.5);
    u_xlat14 = (-u_xlat8.x) + u_xlat8.y;
    u_xlat0.x = u_xlat0.x * u_xlat7.x + (-u_xlat8.x);
    u_xlat7.x = float(1.0) / u_xlat14;
    u_xlat0.x = u_xlat7.x * u_xlat0.x;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat7.x = u_xlat0.x * -2.0 + 3.0;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat7.x;
    u_xlat7.xy = vs_TEXCOORD0.xy * _HairStrandTex_ST.xy + _HairStrandTex_ST.zw;
    u_xlat10_5 = texture(_HairStrandTex, u_xlat7.xy);
    u_xlat7.x = u_xlat10_5.x * _StrandTexIntensity;
    u_xlat14 = _SecondarySpecularShadowAffection * u_xlat21 + 1.0;
    u_xlat0.x = u_xlat10_1.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat7.x;
    u_xlat7.x = u_xlat14 * _SecondarySpecularIntensity;
    u_xlat0.x = u_xlat7.x * u_xlat0.x;
    u_xlat0.xyz = u_xlat0.xxx * _SpecularColor2.xyz;
    u_xlat0.xyz = u_xlat2.xyz * vec3(_PrimarySpecularIntensity) + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat10.xyz * u_xlat4.xyz + u_xlat0.xyz;
    SV_TARGET0.xyz = u_xlat4.www * u_xlat0.xyz;
    SV_TARGET0.w = u_xlat4.w * _DiffuseColor.w;
    return;
}

#endif
                                _LightRampTex                     _MainTex                _HairSpecShift                  _SpecularMap                _HairStrandTex               